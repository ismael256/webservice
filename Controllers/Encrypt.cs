﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Security.Cryptography.X509Certificates;

namespace PayDirectWebService.Controllers
{
    public class Encrypt
    {

        static void Main(string[] args)
        {
            System.Console.WriteLine("hello");
        }

        public string DecryptedData(string PlainStringToDencrypt, X509Certificate2 x509Cert_2)
        {
            RSACryptoServiceProvider privateKeyx = x509Cert_2.PrivateKey as RSACryptoServiceProvider;

            string str = PlainStringToDencrypt.Trim();
            //RSACryptoServiceProvider key = (RSACryptoServiceProvider)x509Cert_2.PublicKey.Key;

            byte[] bytesx = Convert.FromBase64String(str);
            byte[] decryptedBytes = privateKeyx.Decrypt(bytesx, false);

            return  Encoding.UTF8.GetString(decryptedBytes);
        }

        public string EncryptedData(string PlainStringToEncrypt, X509Certificate2 x509Cert_2)
        {
            string base64String;
            try
            {
                if (x509Cert_2 == null)
                {
                    throw new Exception("Certificate x509Cert_2 can not be null");
                }

                string str = PlainStringToEncrypt.Trim();
                byte[] bytes = Encoding.ASCII.GetBytes(str);

                RSACryptoServiceProvider key = (RSACryptoServiceProvider)x509Cert_2.PublicKey.Key;

                base64String = Convert.ToBase64String(key.Encrypt(bytes, false));
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return base64String;
        }

        public string Encrggypt(string plainText, X509Certificate2 cert)
        {
            RSACryptoServiceProvider publicKey = (RSACryptoServiceProvider)cert.PublicKey.Key;
            byte[] plainBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] encryptedBytes = publicKey.Encrypt(plainBytes, false);
            string encryptedText = encryptedBytes.ToString();
            return encryptedText;
        }

        public string Decrypt(string encryptedText, X509Certificate2 cert)
        {
            RSACryptoServiceProvider privateKey = (RSACryptoServiceProvider)cert.PrivateKey;
            byte[] encryptedBytes = Encoding.UTF8.GetBytes(encryptedText);
            byte[] decryptedBytes = privateKey.Decrypt(encryptedBytes, false);
            string decryptedText = decryptedBytes.ToString();
            return decryptedText;
        }
    }
}