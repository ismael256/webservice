﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using PayDirectWebService.Models.API.PaymentStatus;
using PayDirectWebService.Models.API;
using PayDirectWebService.Models.Dto;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Helpers;
/*
namespace PayDirectWebService.Controllers
{
    public class PaymentStatusController : ApiController
    {
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            string requestData = request.Content.ReadAsStringAsync().Result;
            LogAPIRequests logRequest = new LogAPIRequests();
            try
            {
                PaymentStatusRequest paymentStatusRequest = TextHelper.XmlDeserializeFromString<PaymentStatusRequest>(requestData);

                //TODO  uses strict ordering find way to make t flexible
                logRequest.ApiMethodstring = "PaymentStatusRequest";
                logRequest.OriginIP = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
                logRequest.RequestTime = DateTime.Now;
                logRequest.RequestMessage = requestData;

                ParameterHelper.ValidateApiRequestData(paymentStatusRequest);
                Payment paymt = paymentStatusRequest.Payments[0];   //0 becoz we are handling  only 1 payment item 4 now

                logRequest.Signature = paymentStatusRequest.Signature;
                logRequest.RequestId = paymt.PaymentId;

                APISecurity apiSecure = new APISecurity(paymentStatusRequest.ProductId,paymentStatusRequest.RouteId, paymt.PaymentId, paymt.PaymentLogId);

                logRequest.CallingInstitution = apiSecure.WebServiceAcquirerDetails.BankName;

                if (!PaymentOptionConfigs.CheckBankProductAccess(PaymentOptionConfigs.GetPaydirectBankId(apiSecure.WebServiceAcquirerDetails.BankCode),paymentStatusRequest.RouteId))
                    throw new SecurityViolationException("A security violation has been encountered, please ensure that you have the permission to perfom this action");

                if (!apiSecure.CheckPaymentStatusHashValue(paymentStatusRequest.Signature))
                    throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values ");

                PaymentNotificationResponse response = GetPaymentStatus(paymentStatusRequest, apiSecure.WebServiceAcquirerDetails);

                logRequest.ResponseCode = response.Payments[0].ResponseCode;  //only I reversal per request
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(response);
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (PDServiceException e)
            {
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.ExceptionMessage + ":::Code:::" + e.ResponseCode);
                int code;
                Int32.TryParse(e.ResponseCode, out code);
                logRequest.ResponseCode = code;
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(e.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.OK, e.FormatResponse());
            }
            catch (NullReferenceException e)
            {
                PDServiceException pd = new PDServiceException();
                pd.ExceptionMessage = e.Message;
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
                logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(DataFormatException.RESPONSE_CODE);
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatResponse());
            }
            catch (Exception e)
            {
                PDServiceException pd = new PDServiceException();
                pd.ExceptionMessage = e.Message;
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
                logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(SystemMalfunctionException.RESPONSE_CODE);
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatUnhandledResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatUnhandledResponse());
            }  
        }

        private PaymentNotificationResponse GetPaymentStatus(PaymentStatusRequest paymentStatusRequest, ServiceAcquirerDetails acquirerAttributes)
        {
             Payment paymt = paymentStatusRequest.Payments[0];   //0 becoz we are handling  only 1 payment item 4 now
             Dictionary<string, string> dic =  PaymentStatusDTO.RetrieveTransaction(paymt.PaymentLogId);

            PaymentNotificationResponse notificatnResponse = new PaymentNotificationResponse();
            notificatnResponse.RouteId = paymentStatusRequest.RouteId;

            PaymentResponse pytResponse = new PaymentResponse();
            pytResponse.Amount = Convert.ToDecimal(dic["Amount"]);
            pytResponse.PaymentLogId = paymt.PaymentLogId;

            Dictionary<string, string> dict = PaymentOptionConfigs.RetrievePaymentLogAttributes(paymt.PaymentLogId);

            pytResponse.PaymentReferenceNumber = dict["PaymentRefNumber"];
            pytResponse.ResponseCode = Convert.ToInt32(dic["ResponseCode"]);

            notificatnResponse.Payments = new List<PaymentResponse>();
            notificatnResponse.Payments.Add(pytResponse);

            return notificatnResponse;
        }
    }
} */