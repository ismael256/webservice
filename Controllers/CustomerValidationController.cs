﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net.Http.Headers;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API;
using PayDirectWebService.Models.API.CustomerValidation;
using PayDirectWebService.Models.Dto;
using System.Runtime.Serialization;
using PayDirectWebService.Models.Dao; /*

namespace PayDirectWebService.Controllers
{
    public class CustomerValidationController : ApiController
    {
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            string requestData = request.Content.ReadAsStringAsync().Result;
            LogAPIRequests logRequest = new LogAPIRequests();
            try
            {
                CustomerInformationRequest custReqestInfo = TextHelper.XmlDeserializeFromString<CustomerInformationRequest>(requestData);

                logRequest.ApiMethodstring = "CustomerInformationRequest";
                logRequest.OriginIP = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
                logRequest.RequestTime = DateTime.Now;
                logRequest.RequestMessage = requestData;

                ParameterHelper.ValidateApiRequestData(custReqestInfo);

                logRequest.Signature = custReqestInfo.Signature;
                logRequest.RequestId = 0;

                APISecurity apiSecure = new APISecurity(custReqestInfo.ProductId, custReqestInfo.RouteId, custReqestInfo.CustReference);

                logRequest.CallingInstitution = apiSecure.WebServiceAcquirerDetails.BankName;

                if (!PaymentOptionConfigs.CheckBankProductAccess(PaymentOptionConfigs.GetPaydirectBankId(apiSecure.WebServiceAcquirerDetails.BankCode), custReqestInfo.RouteId))
                    throw new SecurityViolationException("A security violation has been encountered, please ensure that you have the permission to perfom this action");

                if (!apiSecure.CheckCustomerRequestHashValue(custReqestInfo.Signature))
                    throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values ");

                RetrieveCustomer rt = new RetrieveCustomer(custReqestInfo);
                CustomerInformationResponse resp = new CustomerInformationResponse();

                List<Customer> cust = new List<Customer>();
                cust.Add(rt.CustomerDetails);
                resp.Customers = cust;

                logRequest.ResponseCode = resp.Customers[0].ResponseCode;  //only I reversal per request
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(resp);
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.OK, resp);;
            }
            catch (PDServiceException e)
            {
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.ExceptionMessage + ":::Code:::" + e.ResponseCode);
                int code;
                Int32.TryParse(e.ResponseCode, out code);
                logRequest.ResponseCode = code;
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(e.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.OK, e.FormatResponse());
            }
            catch (NullReferenceException e)
            {
                PDServiceException pd = new PDServiceException();
                pd.ExceptionMessage = e.Message;
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
                logRequest.ResponseCode = 3;//DEFUNCTY Convert.ToInt32(DataFormatException.RESPONSE_CODE);
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatResponse());
            }
            catch (Exception e)
            {
                PDServiceException pd = new PDServiceException();
                pd.ExceptionMessage = e.Message;
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
                logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(SystemMalfunctionException.RESPONSE_CODE);
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatUnhandledResponse());
            }
        }

        public HttpResponseMessage Get()
        {
            CustomerInformationRequest custRequest = new CustomerInformationRequest();
            custRequest.CustReference = "12345";
            custRequest.RouteId = 11;
            custRequest.Signature = "CB148D7726D94B6F775B2E205551A19DB44625820F466FC191163AB5C8DF00D1";
            custRequest.ProductId = "12345";

            ParameterHelper.ValidateApiRequestData(custRequest);

            APISecurity apiSecure = new APISecurity(custRequest.ProductId, custRequest.RouteId, custRequest.CustReference);

            if (!apiSecure.CheckCustomerRequestHashValue(custRequest.Signature))
                throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values ");

            RetrieveCustomer rt = new RetrieveCustomer(custRequest);
            CustomerInformationResponse resp = new CustomerInformationResponse();

            List<Customer> cust = new List<Customer>();
            cust.Add(rt.CustomerDetails);
            resp.Customers = cust;

            HttpResponseMessage rut = Request.CreateResponse(HttpStatusCode.OK, resp);
            return rut;


            //RetrieveCustomer rt = new RetrieveCustomer(custRequest);

            //CustomerInformationResponse resp = new CustomerInformationResponse();

            //List<Customer> cust = new List<Customer>();
            //cust.Add(rt.CustomerDetails);

            //resp.Customers = cust;



            //HttpResponseMessage rut = Request.CreateResponse(HttpStatusCode.OK, custRequest);
            //return rut;
        }
    }
}
*/