﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Net.Http.Headers;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API;
using PayDirectWebService.Models.API.PaymentReversal;
using PayDirectWebService.Models.API.PaymentReversal.Response;
using PayDirectWebService.Models.Dto;
/*
namespace PayDirectWebService.Controllers
{
    public class PaymentReversalController : ApiController
    {
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            string requestData = request.Content.ReadAsStringAsync().Result;
            LogAPIRequests logRequest = new LogAPIRequests();
            try
            {
                PaymentReversalRequest paymentReversalRequest = TextHelper.XmlDeserializeFromString<PaymentReversalRequest>(requestData);

                logRequest.ApiMethodstring = "PaymentReversal";
                logRequest.OriginIP = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
                logRequest.RequestTime = DateTime.Now;
                logRequest.RequestMessage = requestData;

                ParameterHelper.ValidateApiRequestData(paymentReversalRequest);

                Reversal reversal = paymentReversalRequest.Reversals[0];//0 becoz we are handling  only 1 payment item 4 now
                logRequest.Signature = paymentReversalRequest.Signature;
                logRequest.RequestId = reversal.ReversalId;

                APISecurity apiSecure = new APISecurity(paymentReversalRequest.ProductId, paymentReversalRequest.RouteId, reversal.Amount, reversal.ReversalId, reversal.PaymentLogId);

                logRequest.CallingInstitution = apiSecure.WebServiceAcquirerDetails.BankName;

                //unless otherwise, reversal requests should onlye ever be made once  
                if (LogAPIRequests.RequestExists(paymentReversalRequest.Signature))
                    throw new InvalidRequestException("This is a duplicate post, please crosscheck your request and try again");

                if (!PaymentOptionConfigs.CheckBankProductAccess(PaymentOptionConfigs.GetPaydirectBankId(apiSecure.WebServiceAcquirerDetails.BankCode), paymentReversalRequest.RouteId))
                    throw new SecurityViolationException("A security violation has been encountered, please ensure that you have the permission to perfom this action");

                if (!apiSecure.CheckPaymentReversalHashValue(paymentReversalRequest.Signature))
                    throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values ");

                PaymentReversalResponse response = MakeReversal(paymentReversalRequest, apiSecure.WebServiceAcquirerDetails);

                logRequest.ResponseCode = response.Reversals[0].ResponseCode;  //only I reversal per request
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(response);
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (PDServiceException e)
            {
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.ExceptionMessage + ":::Code:::" + e.ResponseCode);
                int code;
                Int32.TryParse(e.ResponseCode,out code);
                logRequest.ResponseCode = code;
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(e.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.OK, e.FormatResponse());
            }
            catch (NullReferenceException e)
            {
                PDServiceException pd = new PDServiceException();
                pd.ExceptionMessage = e.Message;
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
                logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(DataFormatException.RESPONSE_CODE);
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatResponse());
            }
            catch (Exception e)
            {
                PDServiceException pd = new PDServiceException();
                pd.ExceptionMessage = e.Message;
                PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
                logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(SystemMalfunctionException.RESPONSE_CODE);
                logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
                logRequest.LogIncomingTransaction();

                return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatUnhandledResponse());
            }  
        }

       
      /*  public HttpResponseMessage  GET()
        {
            PaymentReversalRequest paymentReversalRequest  = new PaymentReversalRequest();

            paymentReversalRequest.Signature = "0FED2336661A6EF241BD657B4D024632A7990A2AB5FFB59CDAF4F88F78A0F48F";
            paymentReversalRequest.ProductId = "12345";
            paymentReversalRequest.RouteId = 36;
            paymentReversalRequest.Reversals = new List<Reversal>();

            paymentReversalRequest.Reversals = new List<Reversal>();
             Reversal rt = new Reversal();
            rt.Amount = 0;
            rt.PaymentLogId = 1062;
            rt.Reason = "Wrong amount was sent";
            rt.ReversalId = 12;
            rt.TerminalId = "23";
            paymentReversalRequest.Reversals.Add(rt);
            PaymentReversalResponse response = MakeReversal(paymentReversalRequest);

            return Request.CreateResponse(HttpStatusCode.OK, response);

        } */
/*
        private PaymentReversalResponse MakeReversal(PaymentReversalRequest paymentReversalRequest,ServiceAcquirerDetails acquirerAttributes)
        {
             Reversal reversal = paymentReversalRequest.Reversals[0];//0 becoz we are handling  only 1 payment item 4 now
             ReversalDetails reversalDet = new ReversalDetails(paymentReversalRequest.RouteId, reversal.PaymentLogId, reversal.ReversalId, reversal.TerminalId, reversal.Amount, reversal.Reason);

             if (!reversalDet.ProductDetails.UsesPaymentLock)
                 throw new RestrictedFunctionException("This product has not been configured to allow reversals");

            if(reversalDet.ReversalState != 0)
                throw new InvalidRequestException("A reversal has already been carried out for this transaction");

             if (reversalDet.ProductDetails.ReversalType == 0) //0 is for Zeroize
                 reversalDet.Zeroize();
             else
             {
                 if (reversalDet.ProductDetails.ReversalType == 1)
                     reversalDet.ReverseAsNewTransaction();
                 else
                     throw new FunctionNotSupportedException("This product has not been configured with a reversal type that hasn't been implemented yet");
             }

            PaymentReversalResponse response = new PaymentReversalResponse();
            response.RouteId = paymentReversalRequest.RouteId;

            ReversalResponse reversalResponse = new ReversalResponse();
            reversalResponse.PaymentLogId = reversal.PaymentLogId;
            reversalResponse.ResponseCode = 0;

            response.Reversals = new List<ReversalResponse>();
            response.Reversals.Add(reversalResponse);
            return response;
        }
    }
} */