﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Net.Http.Headers;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotification;
using PayDirectWebService.Models.API;
using PayDirectWebService.Models.Dto;

using PayDirectWebService.Models.Dao;
/*
namespace PayDirectWebService.Controllers
{
    public class PaymentNotificationController : ApiController
    {

       public HttpResponseMessage Post(HttpRequestMessage request)
       {
           string requestData = request.Content.ReadAsStringAsync().Result;
           LogAPIRequests logRequest = new LogAPIRequests();
           try
           {
               PaymentNotificationRequest paymentNotificationRequest = TextHelper.XmlDeserializeFromString<PaymentNotificationRequest>(requestData);

               //TODO  uses strict ordering find way to make t flexible
               logRequest.ApiMethodstring = "paymentNotificationRequest";
               logRequest.OriginIP = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
               logRequest.RequestTime = DateTime.Now;
               logRequest.RequestMessage = requestData;

               ParameterHelper.ValidateApiRequestData(paymentNotificationRequest);

               Payment paymt = paymentNotificationRequest.Payments[0];   //0 becoz we are handling  only 1 payment item 4 now

               logRequest.Signature = paymentNotificationRequest.Signature;
               logRequest.RequestId = paymt.PaymentId;

               APISecurity apiSecure = new APISecurity(paymentNotificationRequest.ProductId, paymentNotificationRequest.RouteId, paymt.Amount, paymt.PaymentDateTime, paymt.PaymentId, paymt.CustReference);

               logRequest.CallingInstitution = apiSecure.WebServiceAcquirerDetails.BankName;

               if (!PaymentOptionConfigs.CheckBankProductAccess(PaymentOptionConfigs.GetPaydirectBankId(apiSecure.WebServiceAcquirerDetails.BankCode), paymentNotificationRequest.RouteId))
                   throw new SecurityViolationException("A security violation has been encountered, please ensure that you have the permission to perfom this action");

               if (LogAPIRequests.RequestExists(paymentNotificationRequest.Signature))
                   throw new InvalidRequestException("This is a duplicate post, please crosscheck your request and try again");

               if (!apiSecure.CheckPaymentRequestHashValue(paymentNotificationRequest.Signature))
                   throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values ");

               try
               {
                   paymentNotificationRequest.Payments[0].PaymentDate = TextHelper.FromUnixTime(paymentNotificationRequest.Payments[0].PaymentDateTime);
                   paymentNotificationRequest.Payments[0].CustReference = apiSecure.DecryptedCustomerRef;
               }
               catch (Exception e) { throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values"); }

               PaymentNotificationResponse response = MakePayment(paymentNotificationRequest, apiSecure.WebServiceAcquirerDetails);

               logRequest.ResponseCode = response.Payments[0].ResponseCode;  //only I reversal per request
               logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(response); 
               logRequest.LogIncomingTransaction();

               return Request.CreateResponse(HttpStatusCode.OK, response);
           }
           catch (PDServiceException e)
           {
               PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.ExceptionMessage + ":::Code:::" + e.ResponseCode);
               int code;
               Int32.TryParse(e.ResponseCode, out code);
               logRequest.ResponseCode = code;
               logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(e.FormatResponse());
               logRequest.LogIncomingTransaction();

               return Request.CreateResponse(HttpStatusCode.OK, e.FormatResponse());
           }
           catch (NullReferenceException e)
           {
               PDServiceException pd = new PDServiceException();
               pd.ExceptionMessage = e.Message;
               PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
               logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(DataFormatException.RESPONSE_CODE);
               logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
               logRequest.LogIncomingTransaction();

               return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatResponse());
           }
           catch (Exception e)
           {
               PDServiceException pd = new PDServiceException();
               pd.ExceptionMessage = e.Message;
               PayDirectWebService.Helpers.ErrorLog.LogError(e, "Message:::" + e.Message);
               logRequest.ResponseCode = 3;//DEFUNCT Convert.ToInt32(SystemMalfunctionException.RESPONSE_CODE);
               logRequest.ResponseMessage = TextHelper.XmlSerializeToObject(pd.FormatResponse());
               logRequest.LogIncomingTransaction();

               return Request.CreateResponse(HttpStatusCode.InternalServerError, pd.FormatUnhandledResponse());
           }  
       }

       public HttpResponseMessage Get()
       {
           Payment req = new Payment();
           req.PaymentId = 1;
           req.CustReference = "085064000095116036251217087241140092244122129190";
           req.CustomerName = "Kedyr";
           req.CustomerAddress = "Kololo";
           req.CustomerPhoneNumber = 256774427564;
           req.Amount = 25000;
           req.PaymentMethod = "Cash";
           req.TerminalId = "45";
           req.PaymentDate = Convert.ToDateTime("2013-06-06 12:34");
           req.TellerLogin = "Florence";
           req.TellerNames = "Nakibuuka";
           req.DepositSlipNumber = "34";
           req.BankCode = "BOU";
           req.BranchCode = "001";
           req.CollectionsAccount = "1100020000123";
           req.PaymentCurrencyCode = "008";
           req.PaymentDateTime = 1357042320;


           PaymentItem pt = new PaymentItem();
           pt.ItemAmount = 25000;
           pt.ItemCode = 2;


           List<PaymentItem> paymtItm = new List<PaymentItem>();
           paymtItm.Add(pt);

           req.PaymentItems = paymtItm;

           PaymentNotificationRequest pnot = new PaymentNotificationRequest();
           pnot.RouteId = 11;
           pnot.Signature = "041073005245148139030106011201162120249157043171082173064207093153055015025223237204148117048166";
           pnot.ProductId = "12345";

           pnot.Payments = new List<Payment>();
           pnot.Payments.Add(req);

           ServiceAcquirerDetails acquirerAttributes = new ServiceAcquirerDetails();
           //APISecurity sec = new APISecurity("12345", pnot.RouteId,req.Amount,req.PaymentDateTime,req.PaymentId);

          // MakePayment(pnot, acquirerAttributes);
           
           /*
           TransactionDetails dobj = new TransactionDetails( pnot, sec.WebServiceAcquirerDetails);

           PaymentTransactionLog rt = new PaymentTransactionLog();
           TransactionDetailsModel trxndetail = dobj.TransactiondetailsModel;

           rt.InsertTransactionLog(trxndetail);

           PaymentItemLogDetails payDet = new PaymentItemLogDetails();

           PaymentItemDetails payItmDetails = new PaymentItemDetails(rt.payment_log_id, dobj.ProductDetails.UsesClassicFeeRegime, paymtItm, req.Amount);
          

           for (int c = 0; c < payItmDetails.PaymentItemModelDetails.Count; c++)
           {
               PaymentItemDetailsModel itemDetModel = payItmDetails.PaymentItemModelDetails[c];
               payDet.InsertPaymentItemLogDetails(itemDetModel);

               decimal isw_trxnFee = (decimal)itemDetModel.isw_trans_fee_pld;
               decimal bankCollction_trxnFee = (decimal)itemDetModel.bank_coll_fee_pld;

               ItemAmountAdjustment adjustmentAmt = new ItemAmountAdjustment(payDet.AdjustmentId, itemDetModel.paylog_details_amount, isw_trxnFee, bankCollction_trxnFee);
               ItemAmountAdjustmentModel adjustmtAmt = adjustmentAmt.FillItemAmountAdjustmentModel();

               PaymentItemAmountAdjustment.InsertItemAmountAdjustment(adjustmtAmt);
           }

           PaymentNotificationResponse notificatnResponse = new PaymentNotificationResponse();
           notificatnResponse.RouteId = pnot.RouteId;

           PaymentResponse pytResponse = new PaymentResponse();
           pytResponse.Amount = trxndetail.approved_amount;
           pytResponse.PaymentLogId = rt.payment_log_id;
           pytResponse.PaymentReferenceNumber = trxndetail.payment_ref_num + rt.pay_index;
           pytResponse.ResponseCode = Convert.ToInt32(trxndetail.response_code);

           notificatnResponse.Payments = new List<PaymentResponse>();
           notificatnResponse.Payments.Add(pytResponse);


           //return dobj.settlement_date.ToString(); */
           
       /*    HttpResponseMessage rut = Request.CreateResponse(HttpStatusCode.OK, pnot);
           return rut; 
       } 

       private PaymentNotificationResponse MakePayment(PaymentNotificationRequest paymentNotificationRequest, ServiceAcquirerDetails acquirerAttributes)
       {
               TransactionDetails trxnDetail = new TransactionDetails( paymentNotificationRequest, acquirerAttributes);

               TransactionDetailsModel trxndetail = trxnDetail.TransactiondetailsModel;

               PaymentTransactionLog trxnlog = new PaymentTransactionLog();
               try
               {
                   trxnlog.InsertTransactionLog(trxndetail);

                   Payment paymt = paymentNotificationRequest.Payments[0];   //0 becoz we are handling  only 1 payment item 4 now

                   List<PaymentItem> paymtItm = paymt.PaymentItems;
                   //insert payment item details tbl_adjustment table, log_details

                   PaymentItemDetails payItmDetails = new PaymentItemDetails(trxnlog.payment_log_id, trxnDetail.ProductDetails.UsesClassicFeeRegime, paymtItm, paymt.Amount);
                   PaymentItemLogDetails payDet = new PaymentItemLogDetails();

                   for (int c = 0; c < payItmDetails.PaymentItemModelDetails.Count; c++)
                   {
                       try
                       {
                            PaymentItemDetailsModel itemDetModel = payItmDetails.PaymentItemModelDetails[c];
                            payDet.InsertPaymentItemLogDetails(itemDetModel);
                      
                           decimal isw_trxnFee = (decimal)itemDetModel.isw_trans_fee_pld;
                           decimal bankCollction_trxnFee = (decimal)itemDetModel.bank_coll_fee_pld;

                           ItemAmountAdjustment adjustmentAmt = new ItemAmountAdjustment(payDet.AdjustmentId, itemDetModel.paylog_details_amount, isw_trxnFee, bankCollction_trxnFee);
                           ItemAmountAdjustmentModel adjustmtAmt = adjustmentAmt.FillItemAmountAdjustmentModel();

                           PaymentItemAmountAdjustment.InsertItemAmountAdjustment(adjustmtAmt);
                       }
                       catch (PaymentException e)
                       {
                           //log back trxn insert and maybe paymentItemdetails and adjustment
                           LogBackInsertions.DeleteTransactionDetails(trxnlog.payment_log_id, trxnlog.trans_id, trxnlog.pl_trans_id);
                           LogBackInsertions.DeleteLogAdjustmentRecord(payDet.AdjustmentId);
                           LogBackInsertions.DeleteItemDetailsLog(payDet.AdjustmentId);

                           throw e.CaughtException;
                       }
                   }
               }
               catch (PaymentException e)
               {
                   //log back trxn insert and maybe paymentItemLogdetails
                   LogBackInsertions.DeleteTransactionDetails(trxnlog.payment_log_id, trxnlog.trans_id, trxnlog.pl_trans_id);

                   throw e.CaughtException;
               }

               PaymentNotificationResponse notificatnResponse = new PaymentNotificationResponse();
               notificatnResponse.RouteId = paymentNotificationRequest.RouteId;

               PaymentResponse pytResponse = new PaymentResponse();
               pytResponse.Amount = trxndetail.approved_amount;
               pytResponse.PaymentLogId = trxnlog.payment_log_id;
               pytResponse.PaymentReferenceNumber = trxndetail.payment_ref_num + trxnlog.pay_index;
               pytResponse.ResponseCode = Convert.ToInt32(trxndetail.response_code);

               notificatnResponse.Payments = new List<PaymentResponse>();
               notificatnResponse.Payments.Add(pytResponse);

               return notificatnResponse;
       }
    }
} */