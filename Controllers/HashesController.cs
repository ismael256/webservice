﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PayDirectWebService.Helpers;
using System.Security.Cryptography;

namespace PayDirectWebService.Controllers
{
    public class HashesController : Controller
    {

        public string Get128ByteString()
        {
            return Helpers.AES4all.generate128Key();
        }

        public string test()
        {
            return SHA512Helper.HashValue("hhhhhdddddd");
        }

        public string CreateCustomerInfoRequestHash(string privateKey, string productCode, string custRef)
        {
            privateKey = privateKey.Trim();
            string raw = privateKey + productCode.Trim() + custRef.Trim();
            return SHA512Helper.HashValue(raw);
        }

        public string EncryptCustomerRef(string privateKey, string custRef)
        {
            string raw = custRef.Trim();
            privateKey = privateKey.Trim();
            return Helpers.TrippleDES.Encrypt(custRef, privateKey);
        }

        // /hashes/CreatePaymentHashValue?privateKey=54321&productCode=11&amount=5000&unixtime=1385458012&paymentId=56&customerRef=12345
        public string CreatePaymentHashValue(string privateKey, string productCode, string amount, string unixtime, string paymentId,string customerRef)
        {
            privateKey = privateKey.Trim();
            productCode = productCode.Trim();
            amount = amount.Trim();
            unixtime = unixtime.Trim();
            paymentId = paymentId.Trim();
            customerRef = customerRef.Trim();

            string raw = privateKey + productCode + amount + unixtime + paymentId + customerRef;
            return SHA512Helper.HashValue(raw);
        }

        public string CreatePaymentsReversalHashValue(string privateKey, string productCode, string amount, string paymentId, string paymentLogId)//private key + productcode + amount +  paymentId + paymentLogId
        {
            privateKey = privateKey.Trim();
            productCode = productCode.Trim();
            amount = amount.Trim();
            paymentId = paymentId.Trim();
            paymentLogId = paymentLogId.Trim();

            string raw = privateKey + productCode + amount + paymentId + paymentLogId;
            return SHA512Helper.HashValue(raw);
        }

        public string CreatePaymentsStatusHashValue(string privateKey, string productCode, string paymentId, string paymentLogId)
        {
            privateKey = privateKey.Trim();
            productCode = productCode.Trim();
            paymentId = paymentId.Trim();
            paymentLogId = paymentLogId.Trim();

            string raw = privateKey + productCode +  paymentId + paymentLogId;
            return SHA512Helper.HashValue(raw);
        }
    }
}
