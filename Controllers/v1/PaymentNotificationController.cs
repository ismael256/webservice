﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Net.Http.Headers;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotification.dto;
using PayDirectWebService.Models.API.PaymentNotification.util;
using PayDirectWebService.Models.API.PaymentNotification.dao;
using PayDirectWebService.Models.API;
using PayDirectWebService.Models.API.Security.util;
using PayDirectWebService.Models.API.Security.dao;
using PayDirectWebService.Models.API.Security.dto;
using PayDirectWebService.Models.Dto;

using PayDirectWebService.Models.Dao;

namespace PayDirectWebService.Controllers
{
    public class PaymentNotificationController : ApiController
    {

        public HttpResponseMessage Post(HttpRequestMessage requestMsg)
       {
           try
           {

               PaymentNotificationRequest paymentNotificationRequest = TextHelper.ExtractIncomingHttpRequestMessageObject<PaymentNotificationRequest>(requestMsg);
               ValidateRequestDataFormat(paymentNotificationRequest);


               bool showErrorDescription = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SecurityErrorHelperMessages"]);

               IEnumerable<string> signature = Request.Headers.GetValues("signature");
               IEnumerable<string> clientId = Request.Headers.GetValues("Authorization");
               IEnumerable<string> nonce = Request.Headers.GetValues("nonce");
               IEnumerable<string> timestamp = Request.Headers.GetValues("timestamp");
               long time = TextHelper.ConvertStringToLong(timestamp.First(), "Timestamp header");

               MerchantApiServiceprovider apiServiceProvider = SecurityDao.GetClientAuthAttributes(clientId.First());
               //FromDownstreamRequestMessageSecure msgSecure = new FromDownstreamRequestMessageSecure(apiServiceProvider.PrivateKey, signature.First(), nonce.First(), time);
               //msgSecure.ValidatePaymentNotificationSignature(paymentNotificationRequest);

           

               //chec for terminal-id, 
               //check that service provider has access to product

            //   if (!PaymentOptionConfigs.CheckBankProductAccess(PaymentOptionConfigs.GetPaydirectBankId(apiServiceProvider.BankCode), paymentNotificationRequest.RouteId))
              //     throw new SecurityViolationException("A security violation has been encountered, please ensure that you have the permission to perfom this action");
              
               PaymentNotificationResponse response = MakePayment.doPayment(paymentNotificationRequest, apiServiceProvider);
               return Request.CreateResponse(HttpStatusCode.OK, response);
           }
           catch (PaymentException TrsfExpt)
           {
               PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleTransferException(TrsfExpt);
               return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
           }
           catch (PaymentNotificationException SExptn)
           {
               PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleServiceException(SExptn);
               return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
           }
           catch (Exception Exptn)
           {
               PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleUnhandledException(Exptn);
               return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
           } 
       }

        private void ValidateRequestDataFormat(PaymentNotificationRequest paymentNotificationRequest)
        {
            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        throw new DataFormatException("Wrongly formatted input values :: " + error.ErrorMessage);
                    }
                }
            }
        }

       public HttpResponseMessage Get()
       {
           return Request.CreateResponse(HttpStatusCode.OK, "ddd");
           /*Payment req = new Payment();
           req.PaymentId = 2237;
           req.CustReference = "12345";
           req.CustomerName = "Kedyr";
           req.CustomerAddress = "Kololo";
           req.CustomerPhoneNumber = 256774427564;
           req.Amount = 25000;
           req.PaymentMethod = "Cash";
           req.TerminalId = "45";
           req.PaymentDate = Convert.ToDateTime("2015-12-03 12:34");
           req.TellerLogin = "Florence";
           req.TellerNames = "Nakibuuka";
           req.DepositSlipNumber = "34";
           req.BankCode = "BOU";
           req.BranchCode = "001";
           req.CollectionsAccount = "1100020000123";

           DateTime time = TimeHelper.ConvertToDateTime("2015-12-03 12:34");
           req.PaymentCurrencyCode = "008";

           req.PaymentDateTime = TimeHelper.ToUnixTime(time);


           PaymentItem pt = new PaymentItem();
           pt.ItemAmount = 25000;
           pt.ItemCode = 2;


           List<PaymentItem> paymtItm = new List<PaymentItem>();
           paymtItm.Add(pt);

           req.PaymentItems = paymtItm;

           PaymentNotificationRequest pnot = new PaymentNotificationRequest();
           pnot.RouteId = 11;
           pnot.Signature = "041073005245148139030106011201162120249157043171082173064207093153055015025223237204148117048166";
           pnot.ProductId = "12345";

           pnot.Payments = new List<Payment>();
           pnot.Payments.Add(req);

           MerchantApiServiceprovider apiServiceProvider = SecurityDao.GetClientAuthAttributes("12345", false);

           pnot.Payments[0].PaymentDate = TextHelper.FromUnixTime(pnot.Payments[0].PaymentDateTime);

           PaymentNotificationResponse response = MakePayment.doPayment(pnot, apiServiceProvider);
           return Request.CreateResponse(HttpStatusCode.OK, response); */
       } 



    }
} 