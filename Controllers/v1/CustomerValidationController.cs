﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Net.Http.Headers;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.API.Security.util;
using PayDirectWebService.Models.API.Security.dao;
using PayDirectWebService.Models.API.Security.dto;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using PayDirectWebService.Models.API.CustomerValidation.util;

namespace PayDirectWebService.Controllers.v1
{
    public class CustomerValidationController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(HttpRequestMessage requestMsg)
        {
           try
           {
                CustomerInformationRequest custReqestInfo = TextHelper.ExtractIncomingHttpRequestMessageObject<CustomerInformationRequest>(requestMsg);
                ValidateRequestDataFormat(custReqestInfo);

                bool showErrorDescription = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SecurityErrorHelperMessages"]);

                IEnumerable<string> signature = Request.Headers.GetValues("signature");
                IEnumerable<string> clientId = Request.Headers.GetValues("Authorization");
                IEnumerable<string> nonce = Request.Headers.GetValues("nonce");
                IEnumerable<string> timestamp = Request.Headers.GetValues("timestamp");
                long time = TextHelper.ConvertStringToLong(timestamp.First(), "Timestamp header ");
                MerchantApiServiceprovider apiServiceProvider = SecurityDao.GetClientAuthAttributes(clientId.First());
                //FromDownstreamRequestMessageSecure msgSecure = new FromDownstreamRequestMessageSecure(apiServiceProvider.PrivateKey, signature.First(), nonce.First(), time);
                //msgSecure.ValidateCustomerNumberRequest(custReqestInfo);

                CustomerInformationResponse response = new CustomerInformationResponse();

                List<Customer> cust = RetrieveCustomer.BatchUploadCustomerRetrieval(custReqestInfo.Customers, custReqestInfo.ProductId);

                response.Customers = cust;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (PaymentException TrsfExpt)
            {
                PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleTransferException(TrsfExpt);
                return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
            }
            catch (PaymentNotificationException SExptn)
            {
                PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleServiceException(SExptn);
                return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
            }
            catch (Exception Exptn)
            {
                PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleUnhandledException(Exptn);
                return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
            } 
        }


        private void ValidateRequestDataFormat(CustomerInformationRequest custReqestInfo)
        {
            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        throw new DataFormatException("Wrongly formatted input values :: " + error.ErrorMessage);
                    }
                }
            }
        }

        [HttpGet]
        public HttpResponseMessage fff()
        {

            DateTime time = PayDirectWebService.Helpers.TimeHelper.FromUnixTime(1452071318);
            //DateTimeOffset offset =  PayDirectWebService.Helpers.TimeHelper.GmtToEastAfrica(time);
            return Request.CreateResponse(HttpStatusCode.OK, time);

           /* CustomerInformationRequest rt = new CustomerInformationRequest();
            rt.PaymentItemCode = "123";
            rt.ProductId = 11;
            rt.RouteId = "jj";
            rt.Signature = "777";
            rt.TransmissionDate = 1234567;


            CustNumber rtt = new CustNumber();
            rtt.CustReference = "12345";

            CustNumber rttt = new CustNumber();
            rtt.CustReference = "1677yh45";

            List<CustNumber> customers = new List<CustNumber>();
            customers.Add(rtt);
            customers.Add(rttt);

            rt.Customers = customers;

           /* rt.Amount = 0;
            rt.CustReference = "12345";
            rt.ProductId = 25;
            rt.TransmissionDate = 1234566;
            *
            return rt;*/
        }
    }
}