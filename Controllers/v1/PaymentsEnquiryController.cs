﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto;
using PayDirectWebService.Models.API.PaymentNotification.dto;
using PayDirectWebService.Models.API.Security.util;
using PayDirectWebService.Models.API.Security.dao;
using PayDirectWebService.Models.API.Security.dto;
using PayDirectWebService.Models.API.PaymentNotificationEnquiry.utils;

namespace PayDirectWebService.Controllers.v1
{
    public class PaymentsEnquiryController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(HttpRequestMessage requestMsg)
        {
           try
            {
                 PaymentNotificationEnquiryRequest paymentsEnquiry = TextHelper.ExtractIncomingHttpRequestMessageObject<PaymentNotificationEnquiryRequest>(requestMsg);
                 ValidateRequestDataFormat(paymentsEnquiry);

                 bool showErrorDescription = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SecurityErrorHelperMessages"]);
               
                 IEnumerable<string> signature = Request.Headers.GetValues("signature");
                 IEnumerable<string> clientId = Request.Headers.GetValues("Authorization");
                 IEnumerable<string> nonce = Request.Headers.GetValues("nonce");
                 long timestamp = TimeHelper.ToUnixTime( DateTime.Now);
                 MerchantApiServiceprovider apiServiceProvider = SecurityDao.GetClientAuthAttributes(clientId.First());
                 FromDownstreamRequestMessageSecure msgSecure = new FromDownstreamRequestMessageSecure(apiServiceProvider.PrivateKey, signature.First(), nonce.First(), timestamp);
                 msgSecure.ValidatePaymentNotificationEnquiryRequest(paymentsEnquiry);

                 PaymentNotificationEnquiryResponse response = new PaymentNotificationEnquiryResponse();
                 RetrievePayments retrievedPayments = new RetrievePayments(paymentsEnquiry);
                 response.Payments = retrievedPayments.GetAllPaymentsInRange();
                 response.ResponseCode =  0;
                 response.ResponseMessage = "Successfull";

                 return Request.CreateResponse(HttpStatusCode.OK, response);
            }
           catch (PaymentException TrsfExpt)
           {
               PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleTransferException(TrsfExpt);
               return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
           }
           catch (PaymentNotificationException SExptn)
           {
               PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleServiceException(SExptn);
               return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
           }
           catch (Exception Exptn)
           {
               PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleUnhandledException(Exptn);
               return Request.CreateResponse(HttpStatusCode.OK, exceptionResponse);
           } 
             
        }

        private void ValidateRequestDataFormat(PaymentNotificationEnquiryRequest paymentsEnquiry)
        {
            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        throw new DataFormatException("Wrongly formatted input values :: " + error.ErrorMessage);
                    }
                }
            }
        }
    }
}