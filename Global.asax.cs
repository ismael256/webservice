﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using PayDirectWebService.Models;

namespace PayDirectWebService
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
                AreaRegistration.RegisterAllAreas();

                GlobalConfiguration.Configuration.Filters.Add(new PayDirectWebService.Models.UnhandldExceptionFilter());
                //GlobalConfiguration.Configuration.MessageHandlers.Add(new PayDirectWebService.Models.API.Utils.ApiMessageHandler());

                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);

                ConfigureApi(GlobalConfiguration.Configuration);

                PayDirectWebService.Models.Utils.PayDirectApplciationData pt = new PayDirectWebService.Models.Utils.PayDirectApplciationData();
                pt.PaymentChannels = PayDirectWebService.Models.Utils.AutoLoad.loadPaymentChannels();
                pt.PayDirectBanks = PayDirectWebService.Models.Utils.AutoLoad.GetPayDirectBanks();
              //  pt.WebServiceAcquirerDetails = PayDirectWebService.Models.Utils.AutoLoad.GetWebServiceAcquirerDetails();
                pt.PaymentTypes = PayDirectWebService.Models.Utils.AutoLoad.GetPaymentMethods();

                Application["PaydirectData"] = pt; 

        }

        void ConfigureApi(HttpConfiguration config)
        {
            // Remove the JSON formatter
            config.Formatters.Remove(config.Formatters.JsonFormatter);

            // Remove the XML formatter
            //config.Formatters.Remove(config.Formatters.XmlFormatter);
        }

        private void Configure(HttpConfiguration httpConfiguration)
        {

            httpConfiguration.Filters.Add(new ElmahErrorAttribute());
            httpConfiguration.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        protected void Application_Error()
        {
        }
    }
}