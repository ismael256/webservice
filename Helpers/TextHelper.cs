﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using PayDirectWebService.Models.Exceptions;
using System.Text.RegularExpressions;
using System.Xml;
using PayDirectWebService.Helpers;

namespace PayDirectWebService.Helpers
{
    public static class TextHelper
    {
        

        public static long ConvertStringToLong(string variable,string variableDescription)
        {
            try { return Convert.ToInt64(variable); }
            catch (Exception ex) { throw new DataFormatException(variableDescription + " should be an integer"); }
        }

        public static long ConvertStringToInt(string variable, string variableDescription)
        {
            try { return Convert.ToInt32(variable); }
            catch (Exception ex) { throw new DataFormatException(variableDescription + " should be an integer"); }
        }

        public static DateTime ConvertToDateTime(string time)
        {
            DateTime tm;
            if (DateTime.TryParse(time, out tm))
                return tm;
            else
                throw new DataFormatException("Invalid time conversion! " + time);
        }

        public static string XmlSerializeToObject<T>(this T objectInstance)
        {
            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(objectInstance.GetType());

                var sb = new StringBuilder();
                using (TextWriter writer = new StringWriter(sb))
                {
                    xmlSerializer.Serialize(writer, objectInstance);
                }
                return sb.ToString();
            }
            catch (Exception e)
            {
                throw new DataFormatException("Wrongly formatted request message, please correct your request and try again"); //NB the request should be in desceding order
            }
        }

        public static T XmlDeserializeFromString<T>(string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static T ExtractIncomingHttpRequestMessageObject<T>(HttpRequestMessage request)
        {
            string requestData = "";
            try
            {
                requestData = request.Content.ReadAsStringAsync().Result;
            }// ReadAsAsync<string>() and ReadAsAsyncString() bring about different results depending on the client
            catch (Exception e)
            {
                requestData = request.Content.ReadAsAsync<string>().Result;
            }
            ErrorLog.LogError(new Exception(), requestData);
            return TextHelper.XmlDeserializeFromString<T>(requestData);
        }


        public static object XmlDeserializeFromString(string objectData, Type type)
        {
            try
            {
                var serializer = new XmlSerializer(type);
                object result;

                using (TextReader reader = new StringReader(objectData))
                {
                    result = serializer.Deserialize(reader);
                }

                return result;
            }
            catch (Exception e)
            {
                throw new DataFormatException("Wrongly formatted request message, please correct your request and try again"); //NB the request should be in desceding order
            }
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> items)
        {
            return items == null || !items.Any();
        }

    }
}