﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using System.Security.Cryptography;

namespace PayDirectWebService.Helpers
{
    public class SHA512Helper
    {
        public static string HashValue(string str)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                byte[] keyByte = encoding.GetBytes(str);
                byte[] msgByte  = shaM.ComputeHash(keyByte);

                return ByteToString(msgByte);
            }
        }

        private static string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }
    }
}