﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Threading;
using PayDirectWebService.Models.Exceptions;


namespace PayDirectWebService.Helpers
{
   /* public class HttpClientHelper
    {
        HttpClient _client;

        public HttpClientHelper(string baseAddress)
        {
            WebRequestHandler handler = new WebRequestHandler()
            {
                AllowAutoRedirect = false,
                UseProxy = false
            };
            _client = new HttpClient(handler);

            _client.BaseAddress = new Uri(baseAddress);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
        }

        public HttpResponseMessage Post(string postUrl, Object objct)
        {
            return _client.PostAsJsonAsync(postUrl, objct).Result;
        }

        public HttpResponseMessage Get(string postUrl)
        {
            return _client.GetAsync(postUrl).Result;
        */

        public class HttpClientHelper
       {
            public static Task<HttpResponseMessage> Post(string baseAddress, string postUrl, Object objct,Dictionary<string,string> headers)
            {
                WebRequestHandler handler = new WebRequestHandler()
                {
                    AllowAutoRedirect = false,
                    UseProxy = false
                };

                using (HttpClient _client = new HttpClient(handler))
                {
                    _client.BaseAddress = new Uri(baseAddress);
                    _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                    _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(headers["Authorization"]);
                    _client.DefaultRequestHeaders.Add("timestamp", headers["timestamp"]);
                    _client.DefaultRequestHeaders.Add("signature", headers["signature"]);
                    _client.DefaultRequestHeaders.Add("nonce", headers["nonce"]);

                    Task<HttpResponseMessage> response = _client.PostAsXmlAsync(postUrl, objct); 

                   if (!response.Result.IsSuccessStatusCode)  //if response contains an error code
                        throw new NoResponseFromRemoteServerException("The remote server returned with an HTTP Error " +  response.Result.StatusCode); 
                    return response;
                }
             }

                public static Task<HttpResponseMessage> CreateResponse(string content, System.Text.Encoding encoding,string mediaType)
                {
                    StringContent ct = new StringContent(content, encoding, mediaType);
                    var result = new HttpResponseMessage();
                    result.Content = ct;

                    var t = Task.Factory.StartNew(() => result);

                    return t;
                }
       
    }
}