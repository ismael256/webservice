﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using System.Security.Cryptography;

namespace PayDirectWebService.Helpers
{
    public class HashHelper
    {
        static readonly char[] AvailableCharacters = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
          };

        public static string HashValue(string str)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                byte[] keyByte = encoding.GetBytes(str);
                byte[] msgByte = shaM.ComputeHash(keyByte);

                return ByteToString(msgByte);
            }
        }

        public static string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }

        /// <summary>
        /// Generates a random string
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        internal static string GenerateIdentifier(int length)
        {
            char[] identifier = new char[length];
            byte[] randomData = new byte[length];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomData);
            }

            for (int idx = 0; idx < identifier.Length; idx++)
            {
                int pos = randomData[idx] % AvailableCharacters.Length;
                identifier[idx] = AvailableCharacters[pos];
            }

            return new string(identifier);
        }

        public static string GenerateClientId()
        {
            string hash = HashHelper.GenerateIdentifier(256);
            return HashValue(hash);
        }

        public static string GenerateSecretKey()
        {
            return HashValue(HashHelper.GenerateIdentifier(256)) + HashValue(HashHelper.GenerateIdentifier(256));
        }
    }
}