﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Helpers
{
    public class ParameterHelper
    {
        private static string ValidateRequestMessageContent(Object objtoValidate)
        {
            string errorMessages = "";

            var validationContext = new ValidationContext(objtoValidate, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(objtoValidate, validationContext, validationResults);

            foreach (var validationResult in validationResults) //only return the first eror message
            {
                errorMessages = validationResult.ErrorMessage;
                break;
            }

            return errorMessages;
        }

        public static void ValidateApiRequestData(Object objtoValidate)
        {
            string errorsInMessg = "";
            try
            {
                errorsInMessg = ParameterHelper.ValidateRequestMessageContent(objtoValidate);
                if (errorsInMessg.Length > 2)
                    throw new DataFormatException(errorsInMessg);
            }
            catch (Exception e)
            {
                throw new DataFormatException("Wrongly formatted input values" + errorsInMessg);
            }
        }
    }
}