﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace PayDirectWebService.Helpers
{
    public class ErrorLog
    {
        /// <summary>
        /// Log error to Elmah
        /// </summary>
        public static void LogError(Exception ex, string contextualMessage) //todo add status code
        {
            try
            {
                    var annotatedException = new Exception(contextualMessage, ex);
                    Elmah.ErrorSignal.FromCurrentContext().Raise(annotatedException, HttpContext.Current);
            }
            catch (Exception)
            {
                // uh oh! just keep going
            }
        }
    }
}