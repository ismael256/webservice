﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PayDirectWebService.Helpers
{
    //http://blog.950buy.com/article/php-and-net-common-encryption-and-decryption-function-class-use-of-3des-encryption-and-decryption/
    public class Crypto3DES
    {
        public Crypto3DES()
        {

        }

        private System.Text.Encoding encoding;

        public string Key { get; set; }

        public System.Text.Encoding Encoding
        {
            get
            {
                if (encoding == null)
                {
                    encoding = System.Text.Encoding.UTF8;
                }
                return encoding;
            }

            set
            {
                encoding = value;
            }
        }


        public string Encrypt3DES(string strString)
        {
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();

            DES.Key = Encoding.ASCII.GetBytes(this.Key);
            DES.Mode = CipherMode.CBC;
            DES.Padding = PaddingMode.Zeros;

            ICryptoTransform DESEncrypt = DES.CreateEncryptor();

            byte[] Buffer = encoding.GetBytes(strString);

            return Convert.ToBase64String(DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }


        public string Decrypt3DES(string strString)
        {
            DESCryptoServiceProvider DES = new DESCryptoServiceProvider();

            DES.Key = Encoding.UTF8.GetBytes(this.Key);
            DES.Mode = CipherMode.ECB;
            DES.Padding = PaddingMode.Zeros;
            ICryptoTransform DESDecrypt = DES.CreateDecryptor();

            byte[] Buffer = Convert.FromBase64String(strString);
            return UTF8Encoding.UTF8.GetString(DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

    }
}