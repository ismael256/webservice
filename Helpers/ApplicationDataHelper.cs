﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Utils;

namespace PayDirectWebService.Helpers
{
    public class ApplicationDataHelper
    {
        public static PayDirectApplciationData GetPayDirectConfigApplicationData()
        {
            return (PayDirectApplciationData)System.Web.HttpContext.Current.Application["PaydirectData"];
        }
    }
} 