﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Helpers
{
    public class TimeHelper
    {
        public static string FormatTime(string time)
        {
            DateTime tm;
            if (DateTime.TryParse(time, out tm))
                return tm.ToString("yyyy-MM-dd HH:mm");
            else
                throw new DataFormatException("Invalid time conversion! " + time);
        }


        public static DateTime ConvertToDateTime(string time)
        {
            DateTime tm;
            if (DateTime.TryParse(time, out tm))
                return tm;
            else
                throw new DataFormatException("Invalid time conversion! " + time);
        }

        public static DateTime FromUnixTime(long unixTime)
        {
            try
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return  GmtToEastAfrica(epoch.AddSeconds(unixTime));
            }
            catch (Exception e)
            {
                throw new DataFormatException("Invalid datetime value ");
            }
        }

        public static DateTime GmtToEastAfrica(DateTime dateTime)
        {

            TimeZoneInfo eastafrica = TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime,eastafrica); 
        }

        public static long ToUnixTime(DateTime time)
        {
            try
            {
                TimeSpan span = (time - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
                //total seconds (which is a UNIX timestamp)
                return (long)span.TotalSeconds;
            }
            catch (Exception e)
            {
                throw new DataFormatException("Invalid datetime value ");
            }
        }
    }
}