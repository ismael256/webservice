﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using PayDirectWebService.Models.Exceptions;

//http://code.google.com/p/aes-encryption-samples/downloads/list
namespace PayDirectWebService.Helpers
{
	public class AES4all
	{
        private static RijndaelManaged getAESCBCCipher(byte[] keyBytes, byte[] IVBytes, PaddingMode padding)
        {
            RijndaelManaged cipher = new RijndaelManaged();
            cipher.KeySize = 128;
            cipher.BlockSize = 128;
            cipher.Mode = CipherMode.CBC;
            cipher.Padding = padding;
            cipher.IV = IVBytes;
            cipher.Key = keyBytes;
            return cipher;
        }

        private static RijndaelManaged getAESECBCipher(byte[] keyBytes, PaddingMode padding)
        {
            RijndaelManaged cipher = new RijndaelManaged();
            cipher.KeySize = 128;
            cipher.BlockSize = 128;
            cipher.Mode = CipherMode.ECB;
            cipher.Padding = padding;
            cipher.Key = keyBytes;
            return cipher;
        }

        private static byte[] encrypt(RijndaelManaged cipher, byte[] toEncrypt)
        {
            //Get an encryptor.
            ICryptoTransform encryptor = cipher.CreateEncryptor();
            //Encrypt the data.
            MemoryStream msEncrypt = new MemoryStream();
            CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
            //Write all data to the crypto stream and flush it.
            csEncrypt.Write(toEncrypt, 0, toEncrypt.Length);
            csEncrypt.FlushFinalBlock();

            //Get encrypted array of bytes.
            return msEncrypt.ToArray();
        }

        private static byte[] decrypt(RijndaelManaged cipher, byte[] encrypted)
        {
            //Get an encryptor.
            ICryptoTransform decryptor = cipher.CreateDecryptor();
            //Now decrypt the previously encrypted message using the decryptor
            // obtained in the above step.
            MemoryStream msDecrypt = new MemoryStream(encrypted);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
            byte[] fromEncrypt = new byte[encrypted.Length];
            //Read the data out of the crypto stream.
            //csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
            csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
            return fromEncrypt;
        }

        public static byte[] demo1encrypt(byte[] keyBytes, byte[] ivBytes, PaddingMode padding, byte[] messageBytes)
        {
            RijndaelManaged cipher = getAESCBCCipher(keyBytes, ivBytes, padding);
            return encrypt(cipher, messageBytes);
        }

        public static byte[] demo1decrypt(byte[] keyBytes, byte[] ivBytes, PaddingMode padding, byte[] encryptedMessageBytes)
        {
            RijndaelManaged decipher = getAESCBCCipher(keyBytes, ivBytes, padding);
            return decrypt(decipher, encryptedMessageBytes);
        }

        public static byte[] demo2encrypt(byte[] keyBytes, PaddingMode padding, byte[] messageBytes)
        {
            RijndaelManaged cipher = getAESECBCipher(keyBytes, padding);
            return encrypt(cipher, messageBytes);
        }

        public static byte[] demo2decrypt(byte[] keyBytes, PaddingMode padding, byte[] encryptedMessageBytes)
        {
            RijndaelManaged decipher = getAESECBCipher(keyBytes, padding);
            return decrypt(decipher, encryptedMessageBytes);
        }

        static void Main(string[] args)
        {
            ASCIIEncoding textConverter = new ASCIIEncoding();

            String sDemoMesage = "This is a demo message from C#!";
            byte[] demoMesageBytes = textConverter.GetBytes(sDemoMesage);
            //shared secret
            byte[] demoKeyBytes = new byte[] {  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
            // Initialization Vector - usually a random data, stored along with the shared secret,
            // or transmitted along with a message.
            // Not all the ciphers require IV - we use IV in this particular sample
            byte[] demoIVBytes = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                                            0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};

            PaddingMode padding = PaddingMode.ISO10126;
            /**/
            Console.WriteLine("Demo Key (base64): " + System.Convert.ToBase64String(demoKeyBytes));
            Console.WriteLine("Demo IV  (base64): " + System.Convert.ToBase64String(demoIVBytes));

            byte[] demo1EncryptedBytes = demo1encrypt(demoKeyBytes, demoIVBytes, padding, demoMesageBytes);
            Console.WriteLine("Demo1 encrypted (base64): " + System.Convert.ToBase64String(demo1EncryptedBytes));
            byte[] demo1DecryptedBytes = demo1decrypt(demoKeyBytes, demoIVBytes, padding, demo1EncryptedBytes);
            Console.WriteLine("Demo1 decrypted message : " + textConverter.GetString(demo1DecryptedBytes));

            byte[] demo2EncryptedBytes = demo2encrypt(demoKeyBytes, padding, demoMesageBytes);
            Console.WriteLine("Demo2 encrypted (base64): " + System.Convert.ToBase64String(demo2EncryptedBytes));
            byte[] demo2DecryptedBytes = demo2decrypt(demoKeyBytes, padding, demo2EncryptedBytes);
            Console.WriteLine("Demo2 decrypted message : " + textConverter.GetString(demo2DecryptedBytes));
            Console.ReadLine();
        }

        public static string encryptString(string key, string msg)
        {
            try
            {
                ASCIIEncoding textConverter = new ASCIIEncoding();

                byte[] messageBytes = textConverter.GetBytes(msg);
                byte[] keyBytes = textConverter.GetBytes(key);

                PaddingMode padding = PaddingMode.ISO10126;

                RijndaelManaged cipher = getAESECBCipher(keyBytes, padding);
                byte[] encrypted = encrypt(cipher, messageBytes);
                return GetString(encrypted);
            }
            catch (Exception ex) { throw new DataFormatException("Error while resolving security configurations, ensure that you are using the right enryption configs"); } //maybe key is not 128
        }

        public static string decryptString(string key, string cipher)
        {
            try
            {

                byte[] encryptedMessageBytes = Encoding.ASCII.GetBytes(cipher);
                byte[] keyBytes = Encoding.ASCII.GetBytes(key);

                PaddingMode padding = PaddingMode.ISO10126;

                RijndaelManaged decipher = getAESECBCipher(keyBytes, padding);
                byte[] decpherd = decrypt(decipher, encryptedMessageBytes);
                return Convert.ToBase64String(decpherd);
            }
            catch (Exception ex) { throw new DataFormatException("Error while resolving security configurations, ensure that you are using the right enryption configs"); } //maybe key is not 128
        }

        public static string generate128Key()
        {
                var numberOfBits = 128;
                var keyBytes = new byte[numberOfBits / 8];

                new RNGCryptoServiceProvider().GetBytes(keyBytes);
                var rijndaelManagedCipher = new RijndaelManaged();

                rijndaelManagedCipher.BlockSize = 128;
                rijndaelManagedCipher.Key = keyBytes;

                return System.Convert.ToBase64String(rijndaelManagedCipher.Key);
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

	}
}