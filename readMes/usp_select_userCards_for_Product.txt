﻿USE [uat_paydirect_core]
GO
/****** Object:  StoredProcedure [dbo].[use_select_userCards_for_Product]    Script Date: 10/04/2013 10:12:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kedyr>
-- Create date: <3/10/2013>
-- Description:	<Used with the Paydirect Webservice,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_select_userCards_for_Product]
	@userLoginName varchar(50),
	@product_id int
AS
BEGIN
	SELECT     
		Cards.*, CardAccounts.*
	FROM         
		Cards INNER JOIN
		CardAccounts (nolock) ON Cards.cardID = CardAccounts.cardAccountCardID INNER JOIN
                Products (nolock) ON CardAccounts.cardAccountProductID = Products.productID INNER JOIN
			    Users (nolock) ON Cards.cardUserID = Users.userID		
		WHERE     (dbo.Users.userLoginName = @userLoginName) AND (dbo.CardAccounts.cardAccountProductID = @product_id)
		
END
