﻿USE [uat_paydirect_ent]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_convertCommaListToTable]    Script Date: 12/07/2015 17:42:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[ufn_convertCommaListToTable] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (id int IDENTITY(1,1) NOT NULL, list_item varchar(100) NOT NULL)
AS
BEGIN
	IF @list IS NOT NULL
	BEGIN
		DECLARE @pos        int,
				@nextpos    int,
				@valuelen   int

		SELECT @pos = 0, @nextpos = 1

		WHILE @nextpos > 0
		BEGIN
			SELECT @nextpos = charindex(',', @list, @pos + 1)
			SELECT @valuelen = CASE WHEN @nextpos > 0
								  THEN @nextpos
								  ELSE len(@list) + 1
							 END - @pos - 1
			INSERT @tbl (list_item)
				VALUES (ltrim(rtrim(convert(varchar(100), substring(@list, @pos + 1, @valuelen)))))
			SELECT @pos = @nextpos
		END
	END
	RETURN
END
