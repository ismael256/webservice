﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[usp_payment_item_reversal]    Script Date: 12/11/2013 19:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_payment_item_reversal]
(
	@payment_detail_id bigint,
	@payment_log_id bigint,	
	@comment varchar(255) = NULL,
	@reason varchar(50) = NULL,
	@retVal int output
)
AS

DECLARE @ref varchar(50)
DECLARE @reversal_status int
DECLARE @previous_payment_log_id bigint
DECLARE @product_id bigint 

select @reversal_status = d.reversal_status
from tbl_payments_log p inner join tbl_payments_log_details d
on p.payment_log_id = d.paylog_details_paylog_id
inner join tbl_payments_log_adjustments a on d.paylog_details_id  = a.adjustment_id
where  d.paylog_details_id = @payment_detail_id

if (@reversal_status = 1) -- reversed
BEGIN
	SET @retVal = -2000
	Return
END
ELSE IF (@reversal_status = 2) -- Repost 
BEGIN
	SET @retVal = -3000
	Return
END
ELSE
BEGIN
	BEGIN TRAN
		
	-- export_status of 0 (not pulled), item_type of 2 (reversed) so that enterprise reporting
	-- can pull the initial payment_log	
	UPDATE tbl_payments_log_details SET reversal_status = 1, [export_status] = 0, item_type = 2 
	WHERE paylog_details_id = @payment_detail_id
	
	INSERT INTO [tbl_payments_log_details]
	([paylog_details_paylog_id],[paylog_details_amount],[paylog_details_comments]
	,[payment_type_id],[payment_type_code],[payment_type_name],[lead_bank_id],[lead_bank_code]
	,[lead_bank_name],[lead_bank_cbn_code],[cbn_account_name],[cbn_account_number],[ref_payment_type_id]
	,[ref_payment_type_code],[ref_payment_type_name],[iso_bank_id],[iso_bank_name],[iso_bank_code]
	,[iso_bank_cbn_code],[total_fee],[isw_trans_fee_pld],[bank_coll_fee_pld],[lead_bank_fee_pld]
	,[iso_fee_pld],[customer_borne_fee],[item_type],[export_status],[reversal_status])
	SELECT
	@payment_log_id, -1 * paylog_details_amount, @comment
	,[payment_type_id],[payment_type_code],[payment_type_name],[lead_bank_id],[lead_bank_code]
	,[lead_bank_name],[lead_bank_cbn_code],[cbn_account_name],[cbn_account_number],[ref_payment_type_id]
	,[ref_payment_type_code],[ref_payment_type_name],[iso_bank_id],[iso_bank_name],[iso_bank_code]
	,[iso_bank_cbn_code],-1 * [total_fee],-1 * [isw_trans_fee_pld],-1 * [bank_coll_fee_pld],-1 * [lead_bank_fee_pld]
	,-1 * [iso_fee_pld],-1 * [customer_borne_fee], 0, 1, 2 --set reversal status to repost, export_status to 1 - already pulled, item type of 0 - standard
	FROM [tbl_payments_log_details] d
	join tbl_payments_log_adjustments a on d.paylog_details_id  = a.adjustment_id
	where d.paylog_details_id = @payment_detail_id
	
	IF (@@Error <> 0 )
	BEGIN
		SET @retVal = -4003
		ROLLBACK TRANSACTION
		RETURN
	END
	
	DECLARE @paylog_details_id bigint
	SET @paylog_details_id = @@Identity
	
	
	INSERT INTO [tbl_payments_log_adjustments]
	([adjustment_id],[adjustment_amount],[adjustment_comments],[adjustment_reason]
	,[tellers_adjustment_amount],[adjustment_type],[isw_coll_fee],[bank_coll_fee],[is_pending])
	SELECT
	@paylog_details_id, -1 * [adjustment_amount],@comment,@reason
	,[tellers_adjustment_amount],[adjustment_type],[isw_coll_fee],[bank_coll_fee],[is_pending]
	FROM [tbl_payments_log_adjustments]
	WHERE [adjustment_id] = @payment_detail_id
	
	IF (@@Error <> 0 )
	BEGIN
		SET @retVal = -4004
		ROLLBACK TRANSACTION
		RETURN
	END
	
	
		
	COMMIT
	SET @retVal = 0
END



