﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[usp_payments_log_adjustment_UPDATE]    Script Date: 12/11/2013 17:40:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_payments_log_adjustment_UPDATE]
	@adjustment_id bigint,
	@adjustment_amount money,
	@adjustment_comments varchar(255) = '',
	@adjustment_reason varchar(50) = '-',
	@tellers_adjustment_amount money = 0,
	@adjustment_type varchar(30) = '',
	@isw_coll_fee money = 0,
	@bank_coll_fee money,
	@is_pending bit = 0
AS

UPDATE [dbo].[tbl_payments_log_adjustments] SET
	[adjustment_amount] = @adjustment_amount,
	[adjustment_comments] = @adjustment_comments,
	[adjustment_reason] = @adjustment_reason,
	[tellers_adjustment_amount] = @tellers_adjustment_amount,
	[adjustment_type] = @adjustment_type,
	[isw_coll_fee] = @isw_coll_fee,
	[bank_coll_fee] = @bank_coll_fee,
	[is_pending] = @is_pending
WHERE
	[adjustment_id] = @adjustment_id

--endregion

