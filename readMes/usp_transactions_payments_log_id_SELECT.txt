﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[usp_transactions_payments_log_id_SELECT]    Script Date: 12/17/2013 17:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*---------------------------------------------------------------------------*\
 *																			 
 * InterSwitch Ltd.															 
 * Nigeria																	 
 * 			
 *
 * File Name:		usp_transactions_payments_log_id_SELECT.sql												  
 * Version Number:	1										 
 *
 * 
 *
 * Copyright (C) 2005                           							 
 * This software may not be copied or distributed in any form without the	 
 * written permission of InterSwitch Ltd.									 
 *																			 
\*---------------------------------------------------------------------------*/

ALTER       PROCEDURE [dbo].[usp_transactions_payments_log_id_SELECT]
	@payment_log_id 	bigint
AS
BEGIN
	SELECT     
		tbl_transactions.*
	FROM         
		dbo.tbl_transactions INNER JOIN
               	dbo.tbl_payments_log_transactions ON 
		dbo.tbl_transactions.trans_id = dbo.tbl_payments_log_transactions.pl_trans_trans_id
	WHERE 
		pl_trans_paylog_id = @payment_log_id
END
