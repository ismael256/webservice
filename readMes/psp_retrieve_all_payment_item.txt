﻿USE [uat_paydirect_ent]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[psp_retrieve_all_payment_item]
		@item_college_id = 23

SELECT	'Return Value' = @return_value

GO
