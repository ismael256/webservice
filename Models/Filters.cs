﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Web.Http;
using System.Web;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models
{

    public class UnHandlesExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is Exception)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(context.Exception));
                context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }

    public class ElmahErrorAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
    {

        public override void OnException(System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext)
        {

            if (actionExecutedContext.Exception != null)
                Elmah.ErrorSignal.FromCurrentContext().Raise(actionExecutedContext.Exception);

            base.OnException(actionExecutedContext);
        }
    }

    public class ElmahHandledErrorLoggerFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnException(actionExecutedContext);

            Elmah.ErrorSignal.FromCurrentContext().Raise(actionExecutedContext.Exception);
        }
    }

    public class UnhandldExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent("An unhandled exception has been encountered"),
                ReasonPhrase = "An unhandled exception has been encountered, Please review your request and if the error persists please conatct system administrator "
            };


            context.Response = msg;
        }
    }
}