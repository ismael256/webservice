﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace PayDirectWebService.Models
{
    public class PayDirectConfig :  ConfigurationSection
    {
        private static PayDirectConfig value = ConfigurationManager.GetSection("StaticPayDirectValueConfig") as PayDirectConfig;

        public static PayDirectConfig Value
        {
            get { return value; }
        }

        [ConfigurationProperty("realTimeDataUploadCustomerValidationValue")]
        public int RealTimeDataUploadCustomerValidationValue
        {
            get { return (int)this["realTimeDataUploadCustomerValidationValue"]; }
            set { this["realTimeDataUploadCustomerValidationValue"] = value; }
        }

        [ConfigurationProperty("batchDataUploadCustomerValidationValue")]
        public int BatchDataUploadCustomerValidationValue
        {
            get { return (int)this["batchDataUploadCustomerValidationValue"]; }
            set { this["batchDataUploadCustomerValidationValue"] = value; }
        }
    }
}