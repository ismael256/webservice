﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.Exceptions
{
    public class PaymentException : Exception
    {
        public short ResponseCode { get; set; }
        public string ResponseMessage { get; set; }


        public enum ExceptionLevel
        {
            TRANSACTION_LOG_INSERTION, PAYMENT_ITEM_DETAIL_INSERTION,PAYMENT_ITEM_ADJUSTMENT,PAYMENT_REVERSAL,API_ACCESS
        }

        public Exception CaughtException;
        public ExceptionLevel ThrownAtLevel;

        public PaymentException(Exception e,ExceptionLevel level)
        {
            CaughtException = e;
            ThrownAtLevel = level;
        }

        public PaymentException() {}
    }
}