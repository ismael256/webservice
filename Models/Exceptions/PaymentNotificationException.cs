﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using PayDirectWebService.Helpers;

namespace PayDirectWebService.Models.Exceptions
{
    public class PaymentNotificationException : Exception
    {
        /// <summary>
        /// the point at which the exception occured a
        /// </summary>
         public enum ExceptionOccuredLevel
        {
             TRANSACTION_LOG_INSERTION, 
             PAYMENT_ITEM_DETAIL_INSERTION,
             PAYMENT_ITEM_ADJUSTMENT,
             PAYMENT_REVERSAL,
             API_ACCESS,
             UNHANDLED_LEVEL
        }

         /// <summary>
         /// maps the level at which the excption occured to the expected level ie client to upstream
         /// </summary>
         public  static string OccuredLevelMapping(ExceptionOccuredLevel level)
         {
             switch (level)
             {
                 case ExceptionOccuredLevel.TRANSACTION_LOG_INSERTION:
                     return "TRANSACTION_LOG_INSERTION";
                 case ExceptionOccuredLevel.PAYMENT_ITEM_DETAIL_INSERTION:
                     return "PAYMENT_ITEM_DETAIL_INSERTION";
                 case ExceptionOccuredLevel.PAYMENT_ITEM_ADJUSTMENT:
                     return "PAYMENT_ITEM_ADJUSTMENT";
                 case ExceptionOccuredLevel.API_ACCESS:
                     return "API_ACCESS";
                 case ExceptionOccuredLevel.PAYMENT_REVERSAL:
                     return "PAYMENT_REVERSAL";
                 default:
                     return "UNIDENTIFIED_LEVEL";
             }
         }

         public PaymentException CaughtException;
        public ExceptionOccuredLevel ThrownAtLevel;

        public PaymentNotificationException(PaymentException e, ExceptionOccuredLevel level)
        {
            CaughtException = e;
            ThrownAtLevel = level;
        }

        public PaymentNotificationExceptionResp FormatResponse()
        {
            PaymentNotificationExceptionResp response = new PaymentNotificationExceptionResp();
            response.ResponseCode = this.CaughtException.ResponseCode;
            response.ResponseMessage =  this.CaughtException.ResponseMessage;
            return response;
        }

        public static PaymentNotificationExceptionResp HandleUnhandledException(Exception Exptn)
        {
            PaymentNotificationException SExptn = new PaymentNotificationException(new InternalServerException(Exptn.Message), PaymentNotificationException.ExceptionOccuredLevel.UNHANDLED_LEVEL);
            return HandleServiceException(SExptn);
        }

        public static PaymentNotificationExceptionResp HandleServiceException(PaymentNotificationException SExptn)
        {
            PaymentNotificationExceptionResp extionResponse = SExptn.FormatResponse();

            /* trxnExce = new Transaction(extionResponse, transactionNo); TODO OPEN IT up when refeactoring payment
            try 
            {
                if (trxnExce.TransactionNo != 0)
                    trxnExce.LogTransferExceptionResponse();
                else
                    throw new Exception("Transaction Cannot be logged beacuse the TransactionNo is 0. Usually caused when a fatal exception occurs");
            }
            catch (PaymentNotificationException exc) { ErrorLog.LogError(exc, exc.Message + " ::: " + exc.CaughtException.Message + " :::: " + exc.CaughtException.ResponseMessage + " LEVEL " + PaymentNotificationException.OccuredLevelMapping(PaymentNotificationException.ExceptionOccuredLevel.UNHANDLED_LEVEL)); }
            catch (Exception exc) { ErrorLog.LogError(exc, exc.Message + " Exception While Logging Transaction " + " LEVEL " + PaymentNotificationException.OccuredLevelMapping(PaymentNotificationException.ExceptionOccuredLevel.UNHANDLED_LEVEL)); } */
           
            string exptionMsg = TextHelper.XmlSerializeToObject<PaymentNotificationExceptionResp>(extionResponse);
            ErrorLog.LogError(SExptn, 
                SExptn.CaughtException.ResponseMessage +
                ":: LEVEL " + PaymentNotificationException.OccuredLevelMapping(PaymentNotificationException.ExceptionOccuredLevel.UNHANDLED_LEVEL) +
                ":: Exception Message " + exptionMsg + 
                " :: Source " + SExptn.Source + 
                " :: Message  " + SExptn.Message +
                " :: Stacktrace " + SExptn.StackTrace + 
                " :: Inner exception trace  " + SExptn.CaughtException.StackTrace + 
                " ::Base exception trace " + SExptn.GetBaseException().StackTrace);
            return extionResponse;
        }

        public static PaymentNotificationExceptionResp HandleTransferException(PaymentException TrsfExpt)
        {
            PaymentNotificationException SExptn = new PaymentNotificationException(TrsfExpt, PaymentNotificationException.ExceptionOccuredLevel.UNHANDLED_LEVEL);
            return HandleServiceException(SExptn);
        }

    }
}