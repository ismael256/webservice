﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Http;
using System.Net.Http;

namespace PayDirectWebService.Models.Exceptions
{
    public class PDServiceException : Exception
    {
        public string ResponseCode { get; set; }
        public string ExceptionMessage { get; set; }

        public static string TRANSACTIONSTATUS_FAILURE = "failure";
        public static string TRANSACTIONSTATUS_DENIED = "denied";
        public static string OTHERERROR_CODE = "13";

        public PDServiceException() { }

        public PDServiceException(string code, string message)
        {
            this.ResponseCode = code;
            this.ExceptionMessage = message;
        }

        public HttpError FormatResponse()
        {
            HttpError err = new HttpError();

            err["ResponseCode"] = this.ResponseCode;
            err["ExceptionMessage"] = this.ExceptionMessage;

            return err;  
        }

        public HttpError FormatUnhandledResponse()
        {
            HttpError err = new HttpError();

            err["ResponseCode"] = OTHERERROR_CODE;
            err["ExceptionMessage"] = this.ExceptionMessage;

            return err;  
        }
    }
}