﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.Exceptions
{
    [Serializable()]
    [DataContract(Namespace = "")]
    public class PaymentNotificationExceptionResp
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(4, ErrorMessage = "The {0} must be at least {2} characters int.", MinimumLength = 4)]
        public short ResponseCode { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(200, ErrorMessage = "The {0} must be at least {2} characters int.", MinimumLength = 0)]
        public string ResponseMessage { get; set; }
    }
}