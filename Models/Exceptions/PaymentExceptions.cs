﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.Exceptions
{
    public class InternalServerException : PaymentException
    {
        public const short APIResponseCode = 96;
        public InternalServerException(string messages)
        {
            this.ResponseCode = 96;
            this.ResponseMessage = messages;
        }
    }

    public class InvalidMerchantReferenceException : PaymentException
    {
        public InvalidMerchantReferenceException(string messages)
        {
            this.ResponseCode = 4;
            this.ResponseMessage = messages;
        }
    }

    public class RestrictedFunctionException : PaymentException
    {
        public RestrictedFunctionException(string messages)
        {
            this.ResponseCode = 17;
            this.ResponseMessage = messages;
        }
    }

    public class FunctionNotSupportedException : PaymentException
    {
        public FunctionNotSupportedException(string messages)
        {
            this.ResponseCode = 15;
            this.ResponseMessage = messages;
        }
    }

    public class NoResponseFromRemoteServerException : PaymentException
    {
        public NoResponseFromRemoteServerException(string messages)
        {
            this.ResponseCode = 91;
            this.ResponseMessage = messages;
        }
    }

    public class SecurityViolationException : PaymentException
    {
        public SecurityViolationException(string messages)
        {
            this.ResponseCode = 63;
            this.ResponseMessage = messages;
        }
    }

    public class InvalidCustomerReferenceException : PaymentException
    {
        public static int RESPONSE_CODE = 53;
        public InvalidCustomerReferenceException(string messages)
        {
            this.ResponseCode = 53;
            this.ResponseMessage = messages;
        }
    }

    public class InvalidRequestException : PaymentException
    {
        public InvalidRequestException(string messages)
        {
            this.ResponseCode = 13;
            this.ResponseMessage = messages;
        }
    }

    public class DataFormatException : PaymentException
    {
        public DataFormatException(string messages)
        {
            this.ResponseCode = 12;
            this.ResponseMessage = messages;
        }
    }

    public class IncorrectApplicationConfigurationException : PaymentException
    {
        public IncorrectApplicationConfigurationException(string messages)
        {
            this.ResponseCode = 16;
            this.ResponseMessage = messages;
        }
    }

    public class SuccessfullResponse : PaymentException
    {
        public static int RESPONSE_CODE = 0;
        public static string RESPONSE_MESSAGE = "Successfull";
    }

    public class SuccessfullWithSomeExceptionsResponse : PaymentException
    {
        public static int RESPONSE_CODE = 4004;
        public static string RESPONSE_MESSAGE = "Successfull with exceptions";
    }
}