﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;/*
using PayDirectWebService.Models.Dao.PayDirect_ENT;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentReversal;
using PayDirectWebService.Models.API.PaymentNotification;
using PayDirectWebService.Helpers;
using System.Data;
using System.Data.SqlClient;

namespace PayDirectWebService.Models.Dto
{
    public class PaymentReversals
    {
        public static void Zeroroize(ReversalDetailsModel reversalModel)
        {
            try
            {
                 using (var dbEnt = new PayDirectEntEntities())
                {
                    dbEnt.usp_payments_log_adjustment_UPDATE(
                         reversalModel.adjustment_id, reversalModel.adjustment_amount, reversalModel.adjustment_comments, reversalModel.adjustment_reason, reversalModel.tellers_adjustment_amount, reversalModel.adjustment_type, reversalModel.isw_coll_fee, reversalModel.bank_coll_fee, reversalModel.is_pending);
                } 
            }
            catch (Exception err)
            {
                throw new PaymentException(new InternalServerException("A database Exception was thrown when implementing the reversal"), PaymentException.ExceptionLevel.PAYMENT_REVERSAL);
            }
        }

        public static void ReverseAsNewTransaction(ReversalDetailsModel reversalModel)
        {
            try
            { 
                PaymentItemReversal(reversalModel);
                PaymentReversal(reversalModel);
            }
            catch (Exception err)
            {
                throw new PaymentException(new InternalServerException("A database Exception was thrown when implementing the reversal"), PaymentException.ExceptionLevel.PAYMENT_REVERSAL);
            }
        }

        private static void PaymentItemReversal(ReversalDetailsModel reversalModel)
        {
            SqlParameter retValParameter = new SqlParameter
            {
                ParameterName = "retVal",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = SqlDbType.Int
            };
            SqlParameter payment_detail_idParameter = new SqlParameter
            {
                ParameterName = "payment_detail_id",
                Value = reversalModel.adjustment_id,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.BigInt
            };
            SqlParameter payment_log_idParameter = new SqlParameter
            {
                ParameterName = "payment_log_id",
                Value = reversalModel.payment_log_id,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.BigInt
            };
            SqlParameter commentParameter = new SqlParameter
            {
                ParameterName = "comment",
                Value = reversalModel.adjustment_comments,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                Size = 255
            };
            SqlParameter reasonParameter = new SqlParameter
            {
                ParameterName = "reason",
                Value = reversalModel.adjustment_reason,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                Size = 50
            };
             string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectEntEntities_ADO"].ConnectionString;
             using (SqlConnection connection = new SqlConnection(connstring))
             {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "usp_payment_item_reversal";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(payment_detail_idParameter);
                    command.Parameters.Add(payment_log_idParameter);
                    command.Parameters.Add(commentParameter);
                    command.Parameters.Add(reasonParameter);
                    command.Parameters.Add(retValParameter);

                    connection.Open();
                    command.ExecuteNonQuery();

                    connection.Close();
             }
        }

        private static void PaymentReversal(ReversalDetailsModel reversalModel)
        {
            SqlParameter retValParameter = new SqlParameter
            {
                ParameterName = "retVal",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = SqlDbType.Int
            };
            SqlParameter reversed_payment_log_idParameter = new SqlParameter
            {
                ParameterName = "reversed_payment_log_id",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = SqlDbType.BigInt
            };
            SqlParameter payment_log_idParameter = new SqlParameter
            {
                ParameterName = "payment_log_id",
                Value = reversalModel.payment_log_id,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.BigInt
            };
            SqlParameter reversal_dateParameter = new SqlParameter
            {
                ParameterName = "reversal_date",
                Value = DateTime.Now,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.DateTime
            };
            SqlParameter payment_ref_numParameter = new SqlParameter
            {
                ParameterName = "payment_ref_num",
                Value = reversalModel.payment_ref_number,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                 Size = 50
            };
             SqlParameter is_chq_paymentParameter = new SqlParameter
            {
                ParameterName = "is_chq_payment",
                Value = reversalModel.is_cheque_payment,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Bit,
                 Size = 50
            };

             string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectEntEntities_ADO"].ConnectionString;
             using (SqlConnection connection = new SqlConnection(connstring))
             {
                 SqlCommand command = new SqlCommand();
                 command.Connection = connection;
                 command.CommandText = "usp_payment_reversal";
                 command.CommandType = CommandType.StoredProcedure;

                 command.Parameters.Add(retValParameter);
                 command.Parameters.Add(payment_log_idParameter);
                 command.Parameters.Add(reversed_payment_log_idParameter);
                 command.Parameters.Add(reversal_dateParameter);
                 command.Parameters.Add(payment_ref_numParameter);
                 command.Parameters.Add(is_chq_paymentParameter);

                 connection.Open();
                 command.ExecuteNonQuery();

                 connection.Close();
             }
        }
    }
} */