﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Core.Objects;
using PayDirectWebService.Helpers;/*
using PayDirectWebService.Models.Dao.PayDirect_Core;
using PayDirectWebService.Models.Dao.PayDirect_ENT;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotification;

namespace PayDirectWebService.Models.Dto
{
    public class PaymentOptionConfigs
    {
        public int ChannelId { get; set; }
        public string ChannelName { get; set; }
        public int CardId { get; set; }
        public long CardPan { get; set; }
        public int AccountId { get; set; }
        public String AccountNumber { get; set; }
        public String ReceiptNumber { get; set; }
        public String PaymentRefNumber { get; set; }

        /// <summary>
        /// in v.1.0.0 only bank branch is accepted
        /// </summary>
        /// <returns>it is channelId while string is channel name</returns>
        public void RetrievePaymentChannel()
        {
            IList<PaymentChannel> channels = ApplicationDataHelper.GetPayDirectConfigApplicationData().PaymentChannels;
            
            for(int c=0;c<channels.Count;c++)
            {
                if (channels[c].channelName.Contains("Bank"))
                {
                   this.ChannelId = channels[c].channelID;
                   this.ChannelName = channels[c].channelName;
                   return;
                }
            }

            throw new IncorrectApplicationConfigurationException("Failure to resolve Payment-Channel");
        }



        private void RetrieveTellerCardData(string userName,int productId,string institutionCode)
        {
            using (var dbCore = new PayDirectCoreEntities())
            {
                IList<SP_usp_select_userCards_based_on_Product_Result> cardDetails = dbCore.SP_usp_select_userCards_based_on_Product(userName, productId, institutionCode).ToList<SP_usp_select_userCards_based_on_Product_Result>();
                if (cardDetails.Count == 0)
                    throw new IncorrectApplicationConfigurationException("No Existing Till account");

                SP_usp_select_userCards_based_on_Product_Result tillAcct = cardDetails[0];
                this.CardId = tillAcct.cardID;
                this.CardPan = Convert.ToInt64(tillAcct.cardPAN);
                this.AccountId = tillAcct.cardAccountID;
                this.AccountNumber = tillAcct.cardAccountNumber;
            }
        }

        public static string  GenerateReceiptNumber()
        {
            using (var dbEnt = new PayDirectEntEntities())
            {
                ObjectParameter parameter = new ObjectParameter("receiptNo", "VARCHAR(10)");
                dbEnt.usp_generate_receipt_number(parameter);

                //this.ReceiptNumber = parameter.Value.ToString();
                return parameter.Value.ToString();
            }
        }
        //todo don't restrict teller to payment channekl

        public void GeneratePaymentRefNumber(string bankCode,string productCode,DateTime paymentDate)
        {
            string defaultRef = bankCode.Trim() + "|BRH|" + productCode.Trim() + "|" + paymentDate.ToString("dd-MM-yyyy") + "|";
            string receiptNo = GenerateReceiptNumber();

            this.PaymentRefNumber = defaultRef;
            this.ReceiptNumber = GenerateReceiptNumber();
        }

        public static BankBranchDetails RetrieveBankBranchDetail(string branchCode, int bankId)
        {
            using (var dbCore = new PayDirectCoreEntities())
            {
                var branchDetail = (from bbk in dbCore.Branches
                                    where bbk.branchCode == branchCode && bbk.branchBankID == bankId
                                    select bbk).FirstOrDefault();

                if (branchDetail == null)
                    throw new FormatException("Used Bank Branch Code or Bank code are wrong, please correct and try again, if this error persists, please contact the PayDirect Administrator");

                BankBranchDetails obj = new BankBranchDetails();
                obj.BranchAddress = branchDetail.branchAddress;
                obj.BranchBankID = branchDetail.branchBankID;
                obj.BranchCode = branchDetail.branchCode;
                obj.BranchID = branchDetail.branchID;
                obj.BranchName = branchDetail.branchName;

                return obj;
            }
        }

        public static long RetrieveAdjustmentId(long paymentLogid)
        {
            using (var dbEnt = new PayDirectEntEntities())
            {
                var paylog_details_id = (from bbk in dbEnt.tbl_payments_log_details
                                         where bbk.paylog_details_paylog_id == paymentLogid
                                         select bbk.paylog_details_id).First();

                try
                {
                    long id = Convert.ToInt64(paylog_details_id);
                    return id;
                }
                catch (Exception e) { throw new IncorrectApplicationConfigurationException("The paymentLogId specified in the request is wrong"); }
            }
        }

        public static Dictionary<string, string> RetrievePaymentLogAttributes(long paymentLogid)
        {
            using (var dbEnt = new PayDirectEntEntities())
            {
                var paylog_details = (from bbk in dbEnt.tbl_payments_log
                                         where bbk.payment_log_id == paymentLogid
                                         select bbk).First();

                Dictionary<string, string> dic = new Dictionary<string, string>();
                if (paylog_details != null)
                {
                    dic.Add("PaymentRefNumber", paylog_details.payment_ref_num);
                    dic.Add("ReversalStatus", paylog_details.reversal_status.ToString());
                    return dic;
                }
                else
                    throw new IncorrectApplicationConfigurationException("The paymentLogId specified in the request is wrong");
            }
        }

        public static  Products GetProductDetails(int productId)
        {
            return Products.GetProduct(productId);
        }

        public static bool CheckBankProductAccess(int bankId, int productCode)
        {
            using (var dbCore = new PayDirectCoreEntities())
            {
                return dbCore.BanksProducts.Any(a => a.bpProductID == productCode && a.bpBankID == bankId);
            }
        }

        public static int GetPaydirectBankId(string bankcode)
        {
            IList<PayDirectWebService.Models.Dao.PayDirect_Core.Bank> banks =  ApplicationDataHelper.GetPayDirectConfigApplicationData().PayDirectBanks;

            for (int c = 0; c < banks.Count; c++)
            {
                if (banks[c].bankCode == bankcode)
                    return banks[c].bankID;
            }
            throw new IncorrectApplicationConfigurationException("Wrong bankCode has either been configured for this institution or a wrong bank code/public key has been sent as part of the request");
        }
    }
} */