﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Helpers;/*
using PayDirectWebService.Models.Dao.Service;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models.Dto
{
    public class ServiceAcquirerDetails
    {
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public int TillAccountId { get; set; }
        public int TillCardId { get; set; }
        public string TillCardPAN { get; set; }
        public string TillAccountNumber { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }

        public static ServiceAcquirerDetails RetrieveServiceAcquire(string publicKey)
        {
            ServiceAcquirerDetails obj = new ServiceAcquirerDetails();

            PayDirectApplciationData pt = ApplicationDataHelper.GetPayDirectConfigApplicationData();

            IList<tbl_acquirerDetails> acquirerDetails =  pt.WebServiceAcquirerDetails;
            foreach (var aquierer in acquirerDetails)
            {
                if (aquierer.publicKey == publicKey)
                {
                    obj.BankName = aquierer.bankName;
                    obj.BankCode = aquierer.bankCode;
                    obj.TillAccountId = aquierer.tillAccountId;
                    obj.TillCardId = aquierer.tillCardId;
                    obj.TillCardPAN = aquierer.tillCardPAN;
                    obj.TillAccountNumber = aquierer.tillAccountNumber;
                    obj.PublicKey = aquierer.publicKey;
                    obj.PrivateKey = aquierer.privateKey;
                    return obj;
                }
            }

            throw new SecurityViolationException("A security violation has been encountered, please ensure that you are sending correct values");
        }
    }
} */