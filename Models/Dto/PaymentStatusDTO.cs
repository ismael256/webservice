﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;/*
using PayDirectWebService.Models.Dao.PayDirect_Core;
using PayDirectWebService.Models.Dao.PayDirect_ENT;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models.Dto
{
    public class PaymentStatusDTO
    {
        public static Dictionary<string, string> RetrieveTransaction(long paymentLogId)
        {
            using (var dbEnt = new PayDirectEntEntities())
            {
                IList<tbl_transactions> trnasctions = dbEnt.SP_usp_transactions_payments_log_id_SELECT(paymentLogId).ToList<tbl_transactions>();
                
                if(trnasctions.Count > 1)
                    throw new IncorrectApplicationConfigurationException("Provided PaymentLogId appears to be wrong please contact the PayDirect administrator to rectify this issue");

                tbl_transactions trxn = trnasctions[0];

                int responseCode = Convert.ToInt32( trxn.final_response_code);
                decimal amount = Convert.ToDecimal(trxn.final_approved_amount);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("ResponseCode", trxn.final_response_code);
                dic.Add("Amount", trxn.final_approved_amount.ToString());

                return dic;
            }
        }
    }
} */