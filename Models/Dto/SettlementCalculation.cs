﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Helpers;

namespace PayDirectWebService.Models
{
    public class SettlementCalculation
    {
        public enum SettlementFrequencyTypes
        {
            DailySettlement,
            WeeklySettlement,
            MonthlySettlement
        };


        private double SettleInNextDays { get; set; }
        private DayOfWeek SettleOnSetWeekDay { get; set; }
        private int SettleDayOfMonth { get; set; }
        private int CashSettlementAddDays { get; set; }
        private int SettlementFrequency { get; set; }
        private DateTime PaymentDate { get; set; }
        /// <summary>
        /// the number of days that have to be added to the settlement date to get the remitance day
        /// </summary>
        private int RemittanceAddDays { get; set; }
        public DateTime SettlementDate { get; set; }
        public DateTime RemittanceDate { get; set; }

        public SettlementCalculation(int settlementFrequency, int cashSettlementAddDays, DateTime paymentDate, int remittanceAddDays)
        {
            SettlementFrequency = settlementFrequency;
            SettlementFrequencyTypes settleFrequency = this.ResolveSettlementFrequency();

            PaymentDate = paymentDate;
            RemittanceAddDays = remittanceAddDays;

            if (settleFrequency == SettlementFrequencyTypes.DailySettlement)
            {
                SettleInNextDays = cashSettlementAddDays;
                this.CalculateSettlementDateForDailyFreq();
            }

            if (settleFrequency == SettlementFrequencyTypes.WeeklySettlement)
            {
                SettleOnSetWeekDay = this.CashSettlementAddDaysWeeklySettlemet();
                this.CalculateSettlementdateForWeeklyFreq();
            }

            if (settleFrequency == SettlementFrequencyTypes.MonthlySettlement)
            {
                SettleDayOfMonth = cashSettlementAddDays;
                this.CalculateSettlementdateForDayInMonth();
            }
        }

        private void CalculateSettlementDateForDailyFreq()
        {
            DateTime settleDay =  PaymentDate.AddDays(SettleInNextDays);
            SettlementDate =  RoundDownToMidnight(settleDay);
            RemittanceDate = SettlementDate.AddDays(RemittanceAddDays);
        }

        private static DateTime RoundDownToMidnight(DateTime time)
        {
            //use 1minute after midnight as the settlement time
            String settle_date = time.ToString("yyyy-MM-dd ") + "00:02";
            return TextHelper.ConvertToDateTime(TimeHelper.FormatTime(settle_date));
        }

        private void CalculateSettlementdateForWeeklyFreq()
        {
            DateTime settleDay = GetNextDateForDay(PaymentDate, SettleOnSetWeekDay);
            SettlementDate = RoundDownToMidnight(settleDay);
            RemittanceDate = SettlementDate.AddDays(RemittanceAddDays);
        }

        private void CalculateSettlementdateForDayInMonth()
        {
            int paymentDay = Convert.ToInt32(PaymentDate.ToString("dd"));
            DateTime settlementDate = PaymentDate;

            //check if day already passed
            if (paymentDay >= SettleDayOfMonth)
            {
                settlementDate = settlementDate.AddMonths(1);
                settlementDate = new DateTime(settlementDate.Year, settlementDate.Month, SettleDayOfMonth, settlementDate.Hour, settlementDate.Hour, settlementDate.Second);
            }
            else
            {
                settlementDate = new DateTime(settlementDate.Year, settlementDate.Month, SettleDayOfMonth, settlementDate.Hour, settlementDate.Hour, settlementDate.Second);
            }

            SettlementDate = RoundDownToMidnight(settlementDate);
            RemittanceDate = SettlementDate.AddDays(RemittanceAddDays);
        }



        #region hepers

        /// <summary>
        /// Finds the next date whose day of the week equals the specified day of the week.
        /// </summary>
        /// <param name="startDate">
        ///		The date to begin the search.
        /// </param>
        /// <param name="desiredDay">
        ///		The desired day of the week whose date will be returneed.
        /// </param>
        /// <returns>
        ///		The returned date occurs on the given date's week.
        ///		If the given day occurs before given date, the date for the
        ///		following week's desired day is returned.
        /// </returns>
        private static DateTime GetNextDateForDay(DateTime startDate, DayOfWeek desiredDay)
        {
            // Given a date and day of week,
            // find the next date whose day of the week equals the specified day of the week.
            return startDate.AddDays(DaysToAdd(startDate.DayOfWeek, desiredDay));
        }

        /// <summary>
        /// Calculates the number of days to add to the given day of
        /// the week in order to return the next occurrence of the
        /// desired day of the week.
        /// </summary>
        /// <param name="current">
        ///		The starting day of the week.
        /// </param>
        /// <param name="desired">
        ///		The desired day of the week.
        /// </param>
        /// <returns>
        ///		The number of days to add to <var>current</var> day of week
        ///		in order to achieve the next <var>desired</var> day of week.
        /// </returns>
        private static int DaysToAdd(DayOfWeek current, DayOfWeek desired)
        {
            // f( c, d ) = g( c, d ) mod 7, g( c, d ) > 7
            //           = g( c, d ), g( c, d ) < = 7
            //   where 0 <= c < 7 and 0 <= d < 7

            int c = (int)current;
            int d = (int)desired;
            int n = (7 - c + d);

            return (n > 7) ? n % 7 : n;
        }

        private SettlementCalculation.SettlementFrequencyTypes ResolveSettlementFrequency()
        {
            SettlementCalculation.SettlementFrequencyTypes freq;
            switch (SettlementFrequency)
            {
                case 0:
                    freq = SettlementCalculation.SettlementFrequencyTypes.DailySettlement;
                    break;
                case 1:
                    freq = SettlementCalculation.SettlementFrequencyTypes.WeeklySettlement;
                    break;
                case 2:
                    freq = SettlementCalculation.SettlementFrequencyTypes.MonthlySettlement;
                    break;
                default:
                    freq = SettlementCalculation.SettlementFrequencyTypes.DailySettlement;
                    break;
            }

            return freq;
        }

        private DayOfWeek CashSettlementAddDaysWeeklySettlemet()
        {
            switch (CashSettlementAddDays)
            {
                case 0:
                    return DayOfWeek.Sunday;
                case 1:
                    return DayOfWeek.Monday;
                case 2:
                    return DayOfWeek.Tuesday;
                case 3:
                    return DayOfWeek.Wednesday;
                case 4:
                    return DayOfWeek.Thursday;
                case 5:
                    return DayOfWeek.Friday;
                case 6:
                    return DayOfWeek.Saturday;
            }

            return DayOfWeek.Monday; //defaults to Monday
        }
        #endregion
    }

    
}