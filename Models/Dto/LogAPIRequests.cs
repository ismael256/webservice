﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PayDirectWebService.Models.Exceptions;/*
using PayDirectWebService.Models.Dao.Service;
using System.Data.SqlClient;

namespace PayDirectWebService.Models.Dto
{
    public class LogAPIRequests
    {
        public string CallingInstitution { get; set; }
        public string ApiMethodstring { get; set; }
        public string RequestMessage { get; set; }
        public string Signature { get; set; }
        public int ResponseCode { get; set; }
        public string OriginIP { get; set; }
        public string ResponseMessage { get; set; }
        public DateTime RequestTime { get; set; }
        public long RequestId { get; set; }

        public LogAPIRequests() { }

        public LogAPIRequests(string institution,string serviceMethod,DateTime reqTime,string reqMsg,string hashValue,long reqId,int responseCode,string responseMsg)
        {
            CallingInstitution = institution;
            ApiMethodstring = serviceMethod;
            RequestMessage = reqMsg;
            Signature = hashValue;
            ResponseCode = responseCode;
            ResponseMessage = responseMsg;
            RequestTime = reqTime;
            RequestId = reqId;
        }


        public void LogIncomingTransaction()
        {
            SqlParameter institutionParameter = new SqlParameter
            {
                ParameterName = "institution",
                Value = CallingInstitution,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                Size = 50
            };
            SqlParameter service_methodParameter = new SqlParameter
            {
                ParameterName = "service_method",
                Value = ApiMethodstring,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                Size = 50
            };
            SqlParameter request_timeParameter = new SqlParameter
            {
                ParameterName = "request_time",
                Value = RequestTime,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.DateTime
            };
            SqlParameter request_messageParameter = new SqlParameter
            {
                ParameterName = "request_message",
                Value = RequestMessage,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Text
            };
            SqlParameter hash_valueParameter = new SqlParameter
            {
                ParameterName = "hash_value",
                Value = Signature,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                Size = 256
            };
            SqlParameter request_idParameter = new SqlParameter
            {
                ParameterName = "request_id",
                Value = RequestId,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.BigInt
            };
            SqlParameter response_codeParameter = new SqlParameter
            {
                ParameterName = "response_code",
                Value = ResponseCode,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Int
            };
            SqlParameter originating_IPParameter = new SqlParameter
            {
                ParameterName = "originating_IP",
                Value = OriginIP,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.VarChar,
                Size = 20
            };
            SqlParameter response_messageParameter = new SqlParameter
            {
                ParameterName = "response_message",
                Value = ResponseMessage,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Text
            };

            try
            {
                string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectWebService_ADO"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connstring))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "psp_log_webserviceTrace";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(institutionParameter);
                    command.Parameters.Add(service_methodParameter);
                    command.Parameters.Add(request_messageParameter);
                    command.Parameters.Add(request_timeParameter);
                    command.Parameters.Add(hash_valueParameter);
                    command.Parameters.Add(request_idParameter);
                    command.Parameters.Add(response_codeParameter);
                    command.Parameters.Add(response_messageParameter);
                    command.Parameters.Add(originating_IPParameter);

                    connection.Open();
                    command.ExecuteNonQuery();

                    connection.Close();
                }
            }
            catch (Exception err)
            {
                PayDirectWebService.Helpers.ErrorLog.LogError(err, "Message:::" + err.Message);
            }

        }

        public static bool RequestExists(string hash)
        {
            using (var db = new PayDirectWebServiceEntities())
            {
                return db.WebServiceTraces.Any(trce => trce.Signature == hash); //  .tbl_ActivationFiles.Any(insts => insts.batchFileId == batchFileId);
            }
        }

        public static void LogApiUsage(string request,DateTime time,string response)
        {
             SqlParameter timestampParameter = new SqlParameter
            {
                ParameterName = "timestamp",
                Value = time,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.DateTime
            };
            SqlParameter requestParameter = new SqlParameter
            {
                ParameterName = "request",
                Value = request,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Text
            };
             SqlParameter responseParameter = new SqlParameter
            {
                ParameterName = "response",
                Value = response,
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Text
            };
             try
             { 
                 string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectWebService_ADO"].ConnectionString;
                 using (SqlConnection connection = new SqlConnection(connstring))
                 {
                     SqlCommand command = new SqlCommand();
                     command.Connection = connection;
                     command.CommandText = "psp_logApiRequests_Responses";
                     command.CommandType = CommandType.StoredProcedure;

                     command.Parameters.Add(timestampParameter);
                     command.Parameters.Add(requestParameter);
                     command.Parameters.Add(responseParameter);

                     connection.Open();
                     command.ExecuteNonQuery();

                     connection.Close();
                 }
             }
             catch (Exception err)
             {
                 PayDirectWebService.Helpers.ErrorLog.LogError(err, "Message:::" + err.Message);
             } 
        }
    }
} */