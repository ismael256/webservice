﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;

namespace PayDirectWebService.Models.Utils
{
    public class PayDirectApplciationData
    {
        public IList<PaymentChannel> PaymentChannels { get; set; }
        public IList<Bank> PayDirectBanks { get; set; }
        public IList<PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities.tbl_payment_type> PaymentTypes { get; set; }
    }
} 