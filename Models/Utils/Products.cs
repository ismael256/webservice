﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models;
using PayDirectWebService.Models.Dao;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models.Utils
{
    public class Products
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public bool AllowCustomerSearch { get; set; }
        public bool UsesThirdParty { get; set; }
        public string DefaultThirdPartyCode { get; set; }
        public bool CaptureIdentity { get; set; }
        public bool ManualCaptureOnly { get; set; }
        public bool AllowMultiplePaymentItem { get; set; }
        public bool UsesUpload { get; set; }
        public bool UsesPreconfiguredFees { get; set; }
        public bool UsesPaymentLock { get; set; }
        public int PaymentLock { get; set; }
        public bool UsesServices { get; set; }
        public bool UsesLeadBank { get; set; }
        public int InstitutionId { get; set; }
        public int CashSettlementAddDays { get; set; }
        public int SettlementFrequency { get; set; }
        public int ReversalType { get; set; }
        public int RemittanceAddDays { get; set; }
        public string RouteId { get; set; }
        public int NotificationType { get; set; }
        public int DataUploadType { get; set; }
        public bool UsesRemittance_bank { get; set; }
        public bool MandatoryCompositeCustomer_Nr { get; set; }
        public bool UsesClassicFeeRegime { get; set; }
        public string ServiceUrl { get; set; }
        public string ServiceUsername { get; set; }
        public string ServicePassword { get; set; }
        public bool UsesAltPayment_ref { get; set; }
        public bool SecureCustRef { get; set; }
        public bool HasItemCategory { get; set; }
        public string ItemCategorySingularName { get; set; }
        public string ItemCategoryPluralName { get; set; }
        public bool EnableNotification { get; set; }

        public static Products GetProduct(int productId)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                Products prdt = new Products();

                var product = (from crd in dbEnt.tbl_colleges
                               where crd.college_id == productId
                               select crd).FirstOrDefault();

                if (product == null)
                    throw new InvalidMerchantReferenceException("Invalid RouteId/ProductId " + productId);

                prdt.ProductId = product.college_id;
                prdt.ProductName = product.college_name;
                prdt.ProductCode = product.college_code;
                prdt.AllowCustomerSearch = product.allow_customer_search;
                prdt.UsesThirdParty = product.uses_third_party;
                prdt.DefaultThirdPartyCode = product.default_third_party_code;
                prdt.CaptureIdentity = product.capture_identity;
                prdt.ManualCaptureOnly = product.manual_capture_only;
                prdt.AllowMultiplePaymentItem = product.allow_multiple_payment_item;
                prdt.UsesUpload = product.uses_upload;
                prdt.UsesPaymentLock = product.uses_payment_lock;
                prdt.PaymentLock = Convert.ToInt32(product.payment_lock);
                prdt.ReversalType = product.reversal_type;
                prdt.UsesServices = product.uses_services;
                prdt.UsesLeadBank = Convert.ToBoolean(product.uses_lead_bank);
                prdt.InstitutionId = Convert.ToInt32(product.institution_id);
                prdt.CashSettlementAddDays = product.cash_settlement_add_days;
                prdt.RemittanceAddDays = product.remittance_add_days;
                prdt.RouteId = product.route_id;
                prdt.NotificationType = product.notification_type;
                prdt.DataUploadType = product.data_upload_type;
                prdt.UsesRemittance_bank = product.uses_remittance_bank;
                prdt.MandatoryCompositeCustomer_Nr = product.mandatory_composite_customer_nr;
                prdt.SettlementFrequency = product.settlement_frequency;
                prdt.UsesClassicFeeRegime = product.uses_classic_fee_regime;
                prdt.ServiceUrl = product.service_url;
                prdt.ServiceUsername = product.service_username;
                prdt.ServicePassword = product.service_password;
                prdt.UsesAltPayment_ref = product.uses_alt_payment_ref;
                prdt.SecureCustRef = product.secure_cust_ref;
                prdt.HasItemCategory = product.has_item_category;
                prdt.ItemCategorySingularName = product.item_category_singular_name;
                prdt.ItemCategoryPluralName = product.item_category_plural_name;
                prdt.EnableNotification = product.enable_notification;
                prdt.UsesPreconfiguredFees = product.uses_preconfigured_fees;

                return prdt;
            }
        }
    }
}