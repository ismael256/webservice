﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;
using PayDirectWebService.Models.Dao.Service;

namespace PayDirectWebService.Models.Utils
{
    /// <summary>
    /// loads the mostly used configurations
    /// </summary>
    public class AutoLoad
    {

        public static IList<PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities.PaymentChannel> loadPaymentChannels()
        { 
            using (var dbCore = new PaydirectCoreEntities())
            {
                return dbCore.SP_get_all_payment_channels().ToList<PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities.PaymentChannel>();
            }
        }

        public static IList<PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities.Bank> GetPayDirectBanks()
        {
            using (var dbCore = new PaydirectCoreEntities())
            {
                return dbCore.SP_psp_rpt_retrieve_banks().ToList<PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities.Bank>();
            }
        }

        /*public static IList<tbl_acquirerDetails> GetWebServiceAcquirerDetails()
        {
            using (var dbService = new PayDirectWebServiceEntities())
            {
                return  dbService.SP_psp_retrieve_acquirerDetails().ToList<tbl_acquirerDetails>();
            }
        }*/

        public static IList<PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities.tbl_payment_type> GetPaymentMethods()
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                return dbEnt.SP_usp_payment_types_SELECT().ToList<PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities.tbl_payment_type>();
            }
        }
    }
} 