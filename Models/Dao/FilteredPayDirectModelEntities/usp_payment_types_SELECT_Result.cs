//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities
{
    using System;
    
    public partial class usp_payment_types_SELECT_Result
    {
        public int payment_type_id { get; set; }
        public string payment_type_code { get; set; }
        public string payment_type_name { get; set; }
        public Nullable<int> status { get; set; }
    }
}
