//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_InterswitchStaff_SELECT_USERID_Result
    {
        public long interswitchStaffID { get; set; }
        public Nullable<int> roleID { get; set; }
        public string roleName { get; set; }
        public Nullable<long> userID { get; set; }
        public string userLastName { get; set; }
        public string userLoginName { get; set; }
        public string userPassword { get; set; }
        public Nullable<bool> userDisableAccess { get; set; }
        public Nullable<int> userRoleID { get; set; }
        public string userOtherNames { get; set; }
        public Nullable<int> userInstitutionID { get; set; }
        public Nullable<bool> userIsActivated { get; set; }
        public string emailAddress { get; set; }
        public Nullable<int> BusinessUnitId { get; set; }
        public Nullable<int> FailedPasswordAttemptCount { get; set; }
        public Nullable<System.DateTime> FailedPasswordAttemptDate { get; set; }
        public Nullable<bool> userIsLockedOut { get; set; }
        public Nullable<int> userLockOutDuration { get; set; }
    }
}
