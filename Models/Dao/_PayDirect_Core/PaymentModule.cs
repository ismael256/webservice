//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentModule
    {
        public PaymentModule()
        {
            this.PaymentTypes = new HashSet<PaymentType>();
        }
    
        public int paymentModuleID { get; set; }
        public int paymentModuleProductID { get; set; }
        public string paymentModuleName { get; set; }
        public string paymentModuleImage { get; set; }
        public string paymentModulePayPageURL { get; set; }
        public int paymentModuleSortOrder { get; set; }
        public int paymentModuleChannelID { get; set; }
        public string last_updated_by { get; set; }
        public Nullable<System.DateTime> last_updated_on { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public string product_name { get; set; }
    
        public virtual PaymentChannel PaymentChannel { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<PaymentType> PaymentTypes { get; set; }
    }
}
