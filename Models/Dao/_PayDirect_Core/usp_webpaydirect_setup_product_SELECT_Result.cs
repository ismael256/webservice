//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_webpaydirect_setup_product_SELECT_Result
    {
        public int productID { get; set; }
        public string productName { get; set; }
        public string productImage { get; set; }
        public string productDescription { get; set; }
        public string productReportsURL { get; set; }
        public int productSortOrder { get; set; }
        public string productShortCode { get; set; }
        public string productCode { get; set; }
        public string vd_interface_url { get; set; }
        public string cheques_monitoring_url { get; set; }
        public string product_mgt_menu_url { get; set; }
        public Nullable<int> product_parent_id { get; set; }
        public Nullable<bool> channels_product { get; set; }
        public Nullable<bool> branch_product { get; set; }
        public string channels_reports_url { get; set; }
        public Nullable<int> categoryId { get; set; }
        public string last_updated_by { get; set; }
        public Nullable<System.DateTime> last_updated_on { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public int webpaydirect_setup_id { get; set; }
        public string site_host_name { get; set; }
        public bool auto_redirect { get; set; }
        public string payment_page_text { get; set; }
        public string feedback_page_text { get; set; }
        public string site_logo { get; set; }
        public string background_color { get; set; }
        public bool authenticate_iin { get; set; }
        public bool authenticate_ref { get; set; }
        public string font_color { get; set; }
        public string web_service_url { get; set; }
        public string merchant_id { get; set; }
        public string merchant_name { get; set; }
        public string terminal_id { get; set; }
        public string merchant_location { get; set; }
        public Nullable<int> is_multi_acquirer_item { get; set; }
        public Nullable<bool> uses_ssl { get; set; }
        public string mac_key { get; set; }
        public Nullable<bool> is_enable_macing { get; set; }
        public Nullable<bool> is_post_response { get; set; }
        public Nullable<bool> send_postilion_code_in_response { get; set; }
        public string payment_processor { get; set; }
        public Nullable<bool> SupportsSecure3D { get; set; }
        public Nullable<short> Secure3dProcessorId { get; set; }
        public Nullable<bool> EnableLoyaltySpend { get; set; }
    }
}
