//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_tqf_provider_SELECT_BY_Id_Result
    {
        public int provider_id { get; set; }
        public string provider_code { get; set; }
        public string provider_name { get; set; }
        public int provider_bank_id { get; set; }
    }
}
