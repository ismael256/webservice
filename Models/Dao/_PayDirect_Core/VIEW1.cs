//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class VIEW1
    {
        public int remittanceAccountID { get; set; }
        public string remittanceAccountNumber { get; set; }
        public string remittanceAccountName { get; set; }
        public int remittanceCardID { get; set; }
        public string remittanceAccountType { get; set; }
        public int remittanceAccountProductID { get; set; }
        public int remittancePayChannelID { get; set; }
        public int Expr1 { get; set; }
        public string Expr2 { get; set; }
        public string Expr3 { get; set; }
        public long Expr4 { get; set; }
    }
}
