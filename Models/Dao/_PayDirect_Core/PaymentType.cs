//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentType
    {
        public PaymentType()
        {
            this.PayTypesPayMethods = new HashSet<PayTypesPayMethod>();
        }
    
        public int paymentTypeID { get; set; }
        public string paymentTypeCode { get; set; }
        public string paymentTypeName { get; set; }
        public int paymentTypeModuleID { get; set; }
    
        public virtual PaymentModule PaymentModule { get; set; }
        public virtual ICollection<PayTypesPayMethod> PayTypesPayMethods { get; set; }
    }
}
