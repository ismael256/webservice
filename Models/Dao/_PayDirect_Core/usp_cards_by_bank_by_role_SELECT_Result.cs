//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_cards_by_bank_by_role_SELECT_Result
    {
        public int cardID { get; set; }
        public string cardPAN { get; set; }
        public string cardSequenceNumber { get; set; }
        public string cardExpiryDay { get; set; }
        public string cardExpiryMonth { get; set; }
        public string cardExpiryYear { get; set; }
        public string cardCVV2 { get; set; }
        public long cardUserID { get; set; }
        public bool isCardHotlisted { get; set; }
        public Nullable<System.DateTime> cardPINAge { get; set; }
        public Nullable<System.DateTime> cardCurrentPINAge { get; set; }
        public long userID { get; set; }
        public string userLastName { get; set; }
        public string userLoginName { get; set; }
        public string userPassword { get; set; }
        public bool userDisableAccess { get; set; }
        public int userRoleID { get; set; }
        public string userOtherNames { get; set; }
        public int userInstitutionID { get; set; }
        public bool userIsActivated { get; set; }
        public string emailAddress { get; set; }
        public Nullable<int> BusinessUnitId { get; set; }
        public Nullable<int> FailedPasswordAttemptCount { get; set; }
        public Nullable<System.DateTime> FailedPasswordAttemptDate { get; set; }
        public Nullable<bool> userIsLockedOut { get; set; }
        public Nullable<int> userLockOutDuration { get; set; }
        public int bankID { get; set; }
        public long bankStaffID { get; set; }
        public int bankStaffBranchID { get; set; }
        public string bankStaffCode { get; set; }
        public int roleID { get; set; }
    }
}
