//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class fullbranch
    {
        public long userid { get; set; }
        public string userLoginName { get; set; }
        public string sortcode_ { get; set; }
        public string AccountNumber { get; set; }
        public int branchid { get; set; }
    }
}
