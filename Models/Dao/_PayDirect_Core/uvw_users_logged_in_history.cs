//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class uvw_users_logged_in_history
    {
        public long loginHistoryUserID { get; set; }
        public System.DateTime loginDateTime { get; set; }
        public Nullable<System.DateTime> logoutDateTime { get; set; }
    }
}
