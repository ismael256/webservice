//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class VIEW3
    {
        public string userLastName { get; set; }
        public string cardPAN { get; set; }
        public int userInstitutionID { get; set; }
        public string emailAddress { get; set; }
    }
}
