//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_LSWCBanks_SELECT_ALL_Result
    {
        public int lswcBankID { get; set; }
        public string lswcBankCode { get; set; }
        public Nullable<int> bankID { get; set; }
        public string bankCode { get; set; }
        public Nullable<bool> isBankOnSwitch { get; set; }
        public string bankName { get; set; }
        public Nullable<bool> isBankSuspended { get; set; }
        public string cbnBankCode { get; set; }
        public string bankIIN { get; set; }
        public Nullable<bool> isPAYDirectBank { get; set; }
        public string last_updated_by { get; set; }
        public Nullable<System.DateTime> last_updated_on { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> institutionID { get; set; }
        public string institutionCode { get; set; }
        public string instututionDescription { get; set; }
        public Nullable<int> instInstitutionTypeID { get; set; }
        public Nullable<int> instInstitutionTableID { get; set; }
        public Nullable<int> institutionParentId { get; set; }
        public Nullable<bool> isPaydirectInstitution { get; set; }
    }
}
