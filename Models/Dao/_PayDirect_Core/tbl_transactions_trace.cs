//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_transactions_trace
    {
        public long trans_id { get; set; }
        public Nullable<System.DateTime> trans_date_time { get; set; }
        public Nullable<System.DateTime> request_date_time { get; set; }
        public Nullable<System.DateTime> response_date_time { get; set; }
        public string response_code { get; set; }
        public Nullable<decimal> trans_amount { get; set; }
        public Nullable<decimal> approved_amount { get; set; }
        public string currency_code { get; set; }
        public Nullable<decimal> surcharge { get; set; }
        public string surcharge_currency { get; set; }
        public string from_account_type { get; set; }
        public string to_account_type { get; set; }
        public string from_account_number { get; set; }
        public string to_account_number { get; set; }
        public string trans_type { get; set; }
        public string terminal_id { get; set; }
        public string retrieval_ref_no { get; set; }
        public string card_pan { get; set; }
        public string card_seq_number { get; set; }
        public string card_exp_day { get; set; }
        public string card_exp_month { get; set; }
        public string card_exp_year { get; set; }
        public string card_acceptor_id { get; set; }
        public string card_acceptor_name_location { get; set; }
        public string acquiring_institution_id { get; set; }
        public string recieving_institution_id { get; set; }
        public string entity_type { get; set; }
        public string pin_data { get; set; }
        public string trans_error { get; set; }
        public string extended_data { get; set; }
    }
}
