//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_tqf_State_SELECT_BY_Id_Result
    {
        public int stateID { get; set; }
        public string stateCode { get; set; }
        public string stateName { get; set; }
        public Nullable<int> hasCBNBranch { get; set; }
        public int countryID { get; set; }
    }
}
