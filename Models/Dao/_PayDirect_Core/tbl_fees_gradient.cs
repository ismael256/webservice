//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_fees_gradient
    {
        public int fees_gradient_id { get; set; }
        public int product_id { get; set; }
        public int step { get; set; }
        public string gradient_period { get; set; }
        public decimal lower_range { get; set; }
        public decimal upper_range { get; set; }
        public decimal gradient_fees { get; set; }
        public string inst_type { get; set; }
        public decimal gradient_fees_percent { get; set; }
        public Nullable<long> lower_trans_count { get; set; }
        public Nullable<long> upper_trans_count { get; set; }
        public Nullable<int> channel_id { get; set; }
    
        public virtual PaymentChannel PaymentChannel { get; set; }
        public virtual Product Product { get; set; }
    }
}
