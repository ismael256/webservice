//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_audit_tables
    {
        public string tbl_name { get; set; }
        public string action_type { get; set; }
        public string db_action { get; set; }
        public string audit_object_name { get; set; }
        public string summary_template { get; set; }
        public string action_object_id_table { get; set; }
        public string action_object_id_columns { get; set; }
        public string action_object_id_join_columns { get; set; }
        public string column_names { get; set; }
        public string old_new_relationships { get; set; }
    }
}
