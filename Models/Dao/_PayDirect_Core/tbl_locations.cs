//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_locations
    {
        public int location_id { get; set; }
        public string location_code { get; set; }
        public string location_name { get; set; }
        public Nullable<int> location_parent { get; set; }
        public Nullable<int> institution_id { get; set; }
        public Nullable<bool> status { get; set; }
        public Nullable<int> state_id { get; set; }
        public Nullable<int> location_level { get; set; }
    }
}
