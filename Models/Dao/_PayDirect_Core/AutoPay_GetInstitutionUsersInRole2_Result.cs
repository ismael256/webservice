//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class AutoPay_GetInstitutionUsersInRole2_Result
    {
        public long UserId { get; set; }
        public string userLastname { get; set; }
        public string userLoginName { get; set; }
        public string userPassword { get; set; }
        public bool UserdisableAccess { get; set; }
        public int userRoleId { get; set; }
        public string userOthernames { get; set; }
        public int userInstitutionId { get; set; }
        public bool userIsActivated { get; set; }
        public string emailAddress { get; set; }
        public string FullName { get; set; }
    }
}
