//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PayDirectWebService.Models.Dao._PayDirect_Core
{
    using System;
    
    public partial class usp_select_bank_product_2_Result
    {
        public int bankProductID { get; set; }
        public int bpBankID { get; set; }
        public int bpProductID { get; set; }
        public bool bankProductStatus { get; set; }
        public Nullable<System.DateTime> goLiveDate { get; set; }
        public string last_updated_by { get; set; }
        public Nullable<System.DateTime> last_updated_on { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
    }
}
