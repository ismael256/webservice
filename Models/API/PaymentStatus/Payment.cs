﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentStatus
{
    [DataContract(Namespace = "")]
    public class Payment
    {
        [DataMember(IsRequired = true)]
        public long PaymentLogId { get; set; }

        [DataMember(IsRequired = true)]
        public long PaymentId { get; set; }
    }
}