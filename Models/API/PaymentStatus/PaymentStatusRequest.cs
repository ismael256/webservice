﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentStatus
{
    [DataContract(Namespace = "")]
    public class PaymentStatusRequest
    {
        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public int ProductCode { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string HashValue { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string InstKey { get; set; }

        [DataMember(IsRequired = true)]
        public List<Payment> Payments { get; set; }
    }
}