﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace PayDirectWebService.Models.API.Utils
{
    public static class DataRecordExtensions
    {
            public static bool HasColumn(this IDataRecord dr, string columnName)
            {
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                        return true;
                }
                return false;
            }

            public static List<string> ExtractColumnsIntoList(this IDataRecord dr)
            {
                var listOfColumns = new List<string>();
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    listOfColumns.Add(dr.GetName(i));
                }
                return listOfColumns;
            }

            public static string SafeDataReaderGetString(this SqlDataReader reader, string fieldname)
            {
                int colIndex = reader.GetOrdinal(fieldname);
                if (!reader.IsDBNull(colIndex))
                    return reader[fieldname].ToString();
                return string.Empty;
            }

            //http://sqlblog.com/blogs/adam_machanic/archive/2006/07/12/sqldatareader-performance-tips.aspx
            public static string SafeDataReaderGetString(this SqlDataReader reader, string fieldname, int colIndex)
            {
                if (!reader.IsDBNull(colIndex))
                    return reader[fieldname].ToString();
                return string.Empty;
            }

            public static int GetFieldIndexString(this SqlDataReader reader, string fieldname)
            {
                return reader.GetOrdinal(fieldname);
            }
        }

}