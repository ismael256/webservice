﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Runtime.Serialization;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.Utils.dao;
using PayDirectWebService.Models.API.Utils.dto;

namespace PayDirectWebService.Models.API.Utils
{
    [DataContract]
    public class ApiResponse : ApiUsage
    {



        public ApiResponse(HttpResponseMessage response, ApiAccessLog accessLog)
        {
            if (response != null)
            {
                //accessLog.ResponseMessage = response.Content.ReadAsStringAsync().Result;
                //LogApiAccess.LogOutGoingRequests(accessLog);
            }
            else
            {
                throw new InvalidRequestException("Empty response Message");
            }
        }

        //to encryption method will be here
    }
} 