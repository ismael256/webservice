﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.Utils.dto
{
    public class ApiAccessLog
    {
        public long TransactionId { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string RequestMessage { get; set; }
        public string ResponseMessage { get; set; }
        public string SourceIpAddress { get; set; }
        public string Signature { get; set; }
        public string Nonce { get; set; }

    }
}