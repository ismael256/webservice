﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http.Headers;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.Utils.dto
{
    [DataContract]
    [KnownType(typeof(ApiResponse))]
    [KnownType(typeof(ApiRequest))]
    public class ApiUsage
    {
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string ClientId { get; set; }
        [DataMember]
        public string Nonce { get; set; }
        [DataMember]
        public string Timestamp { get; set; }
        [DataMember]
        public string Signature { get; set; }
        [DataMember]
        public string SourceIpaddress { get; set; }
    }
}