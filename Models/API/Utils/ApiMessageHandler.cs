﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using PayDirectWebService.Models.API.Utils.dto;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Helpers;

namespace PayDirectWebService.Models.API.Utils
{
    public class ApiMessageHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            ApiAccessLog accessLog = new ApiAccessLog();
           // try
           // {
                var apiRequest = new ApiRequest(request);
                accessLog = apiRequest.AccessLog;
          /*  }
            catch (PaymentException TrsfExpt)
            {
                PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleTransferException(TrsfExpt);
                return ExceptionResponse(exceptionResponse);
            }
            catch (PaymentNotificationException SExptn)
            {
                PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleServiceException(SExptn);
                return ExceptionResponse(exceptionResponse);
            }
            catch (Exception Exptn)
            {
                PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleUnhandledException(Exptn);
                return ExceptionResponse(exceptionResponse);
            }*/


            return base.SendAsync(request, cancellationToken).ContinueWith(
                task =>
                {
                  //  try
                  //  {
                        var apiResponse = new PayDirectWebService.Models.API.Utils.ApiResponse(task.Result, accessLog);
                   /* }
                    catch (PaymentException TrsfExpt)
                    {
                        PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleTransferException(TrsfExpt);
                        return ExceptionResponse(exceptionResponse).Result;
                    }
                    catch (PaymentNotificationException SExptn)
                    {
                        PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleServiceException(SExptn);
                        return ExceptionResponse(exceptionResponse).Result;
                    }
                    catch (Exception Exptn)
                    {
                        PaymentNotificationExceptionResp exceptionResponse = PaymentNotificationException.HandleUnhandledException(Exptn);
                        return ExceptionResponse(exceptionResponse).Result;
                    }*/

                    return task.Result;
                }
            );
        }

        public static Task<HttpResponseMessage> ExceptionResponse(PaymentNotificationExceptionResp exceptionResponse)
        {
            string response = TextHelper.XmlSerializeToObject<PaymentNotificationExceptionResp>(exceptionResponse);
            return HttpClientHelper.CreateResponse(response, System.Text.Encoding.UTF8, "application/xml");
        }
    }
}