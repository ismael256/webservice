﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Runtime.Serialization;
using PayDirectWebService.Models.API.Utils.dto;
using PayDirectWebService.Models.API.Utils.dao;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.Security.util; 

namespace PayDirectWebService.Models.API.Utils
{
    [DataContract]
    public class ApiRequest : PayDirectWebService.Models.API.Utils.dto.ApiUsage
    {
        PayDirectWebService.Models.API.Utils.dto.ApiUsage apiRequest;
        public ApiAccessLog AccessLog;
        public ApiRequest(HttpRequestMessage request)
        {
            if (request != null)
            {
                IEnumerable<string> headers;
                apiRequest = new PayDirectWebService.Models.API.Utils.dto.ApiUsage();
                apiRequest.Content = request.Content.ReadAsAsync<string>().Result;// TODO this line returns an empty string
                apiRequest.SourceIpaddress = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "";
                bool showErrorDescription = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SecurityErrorHelperMessages"]);

               /* bool time = request.Headers.TryGetValues("timestamp", out headers);
                if (!time)
                    throw new PaymentNotificationException(new SecurityViolationException("Incorrect Security parameters - " + ((showErrorDescription) ? "The timestamp header is missing" : "")), PaymentNotificationException.ExceptionOccuredLevel.API_ACCESS);
                apiRequest.Timestamp = headers.First(); */

                bool clientId = request.Headers.TryGetValues("Authorization", out headers);
                if (!clientId)
                    throw new SecurityViolationException("Incorrect Security parameters - " + ((showErrorDescription) ? "The Authorization header is missing" : ""));
                apiRequest.ClientId = headers.First();

                bool signature = request.Headers.TryGetValues("signature", out headers);
                if (!signature)
                    throw new SecurityViolationException("Incorrect Security parameters - " + ((showErrorDescription) ? "The signature header is missing" : ""));
                apiRequest.Signature = headers.First();

                bool nonce = request.Headers.TryGetValues("nonce", out headers);
                if (!nonce)
                    throw new SecurityViolationException("Incorrect Security parameters - " + ((showErrorDescription) ? "The nonce header is missing" : ""));
                apiRequest.Nonce = headers.First();

                ApiAccessLog acclog = new ApiAccessLog();
                acclog.SourceIpAddress = apiRequest.SourceIpaddress;
                acclog.RequestMessage = apiRequest.Content;
                acclog.Timestamp = DateTime.Now;
                acclog.Nonce = apiRequest.Nonce;
                acclog.Signature = apiRequest.Signature;

                //TransactionSecure secure = new TransactionSecure(apiRequest);
                 acclog.TransactionId = LogApiAccess.LogIncomingRequests(acclog);
            }
            else
            {
                throw new InvalidRequestException("Empty Request Message");
            }
        }

    }
} 