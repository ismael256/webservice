﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.API.Utils.dto; 
using PayDirectWebService.Models.Dao.Service;

namespace PayDirectWebService.Models.API.Utils.dao
{
    public class LogApiAccess
    {

        public static long LogIncomingRequests(ApiAccessLog request)
        {
            using (var dbContext = new PayDirectWebServiceEntities())
            {
                var trxn = new tbl_accessLogs();
                trxn.request = request.RequestMessage;
                trxn.timestamp = request.Timestamp;
                trxn.sourceIpAddress = request.SourceIpAddress;
                trxn.nonce = request.Nonce;
                trxn.signature = request.Signature;

                dbContext.tbl_accessLogs.Add(trxn);
                dbContext.SaveChanges();

                return trxn.id;
            }
        }

        public static void LogOutGoingRequests(ApiAccessLog response)
        {
            using (var dbContext = new PayDirectWebServiceEntities())
            {
                var dbNode = (from trxn in dbContext.tbl_accessLogs
                              where trxn.id == response.TransactionId
                              select trxn).First();

                dbNode.response = response.ResponseMessage;
                dbNode.timestamp = response.Timestamp;
                dbNode.sourceIpAddress = response.SourceIpAddress;
                dbNode.request = response.RequestMessage;

                dbContext.SaveChanges();
            }
        }
    }
} 