﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    [DataContract( Namespace = "")]
    public class PaymentNotificationRequest
    {
        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string RouteId { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string Signature { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public long TransmissionDate { get; set; }


        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public int  ProductId { get; set; }

        [DataMember(IsRequired = true)]
        public List<Payment> Payments { get; set; }


    }
}