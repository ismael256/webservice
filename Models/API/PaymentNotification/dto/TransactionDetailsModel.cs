﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    public class TransactionDetailsModel
    {
        public int channel_id { get; set; }
        public string channel_name { get; set; }
        public string coll_acct_num { get; set; }
        public System.DateTime payment_log_date { get; set; }
        public string payment_ref_num { get; set; }
        public string cust_number { get; set; }
        public string old_cust_number { get; set; }
        public string product_unique_field { get; set; }
        public string cust_name { get; set; }
        public string cust_address { get; set; }
        public string cust_phone { get; set; }
        public string cust_email { get; set; }
        public string cust_dob { get; set; }
        public string cust_location_code { get; set; }
        public string cust_location_name { get; set; }
        public string cust_sub_location_code { get; set; }
        public string cust_sub_location_name { get; set; }
        public Nullable<int> card_id { get; set; }
        public string card_pan { get; set; }
        public Nullable<int> account_id { get; set; }
        public string account_number { get; set; }
        public string user_login_name { get; set; }
        public string user_last_name { get; set; }
        public string user_other_names { get; set; }
        public string role_name { get; set; }
        public Nullable<long> user_db_id { get; set; }
        public string user_staff_code { get; set; }
        public int bank_id { get; set; }
        public string bank_name { get; set; }
        public string cbn_bank_code { get; set; }
        public string bank_code { get; set; }
        public Nullable<int> branch_id { get; set; }
        public string branch_name { get; set; }
        public string branch_code { get; set; }
        public string deposit_slip_no { get; set; }
        public int payment_method_id { get; set; }
        public string payment_method_name { get; set; }
        public Nullable<int> state_id { get; set; }
        public string state_name { get; set; }
        public Nullable<decimal> isw_trans_fee_percent { get; set; }
        public Nullable<decimal> bank_coll_fee_percent { get; set; }
        public string reciept_number { get; set; }
        public string comments { get; set; }
        public string misc_bank_code { get; set; }
        public string misc_branch_code { get; set; }
        public string payment_currency { get; set; }
        public Nullable<decimal> isw_trans_fee { get; set; }
        public Nullable<decimal> lead_bank_fees_percent { get; set; }
        public Nullable<bool> is_double_posting { get; set; }
        public Nullable<System.DateTime> settlement_date { get; set; }
        public Nullable<System.DateTime> remittance_date { get; set; }
        public string student_number { get; set; }
        public string student_name { get; set; }
        public string student_college_code { get; set; }
        public string student_college_name { get; set; }
        public int student_college_id { get; set; }
        public string student_department { get; set; }
        public string student_faculty { get; set; }
        public string student_level { get; set; }
        public string college_session { get; set; }
        public string semester { get; set; }
        public Nullable<int> product_id { get; set; }
        public string product_code { get; set; }
        public string product_name { get; set; }
        public Nullable<decimal> fees_amount { get; set; }
        public decimal iso_fees_percent { get; set; }
        public bool notification_status { get; set; }
        public Nullable<int> notification_retries { get; set; }
        public Nullable<decimal> isw_trans_fees { get; set; }
        public Nullable<decimal> coll_bank_fees { get; set; }
        public Nullable<decimal> lead_bank_fees { get; set; }
        public Nullable<decimal> iso_fees { get; set; }
        public string transaction_ref_num { get; set; }
        public string transaction_status { get; set; }
        public string transaction_code { get; set; }
        public int payment_notification_status { get; set; }
        public int remittance_status { get; set; }
        public Nullable<bool> push_status { get; set; }
        public string surcharge_deposit_slip_no { get; set; }
        public string additional_info { get; set; }
        public bool use_ent_receipt_no { get; set; }
        public Nullable<System.DateTime> reversal_lock_start_date { get; set; }
        public int email_notification_status { get; set; }
        public int sms_notification_status { get; set; }
        public int ftp_notification_status { get; set; }
        public int payment_notification_retries { get; set; }
        public Nullable<System.DateTime> payment_notification_date { get; set; }
        public Nullable<System.DateTime> payment_notification_response_date { get; set; }
        public int reversal_status { get; set; }
        public Nullable<int> export_status { get; set; }
        public Nullable<int> export_type { get; set; }
        public Nullable<decimal> bank_coll_fee { get; set; }
        public int service_notification_status { get; set; }
        public Nullable<bool> settlement_processed { get; set; }
        public bool uses_percentage { get; set; }
        public Nullable<System.DateTime> settlement_processed_date { get; set; }
        public int loadcard_notification_status { get; set; }
        private int SettlementFrequencyType { get; set; }
        public decimal trans_amount { get; set; }
        public decimal approved_amount { get; set; }
        public decimal surcharge { get; set; }
        public string from_account_type { get; set; }
        public string from_account_number { get; set; }
        public string to_account_number { get; set; }
        public string trans_type { get; set; }
        public string terminal_id { get; set; }
        public string retrieval_ref_no { get; set; }
        public string to_account_type { get; set; }
        public string card_seq_number { get; set; }
        public string card_exp_day { get; set; }
        public string card_exp_month { get; set; }
        public string card_exp_year { get; set; }
        public bool data_push_status { get; set; }
        public bool alt_ref_flag { get; set; }
        public int alt_ref_length { get; set; }
        public bool is_fee { get; set; }
        public string coll_acct_name { get; set; }
        public string response_code { get; set; }
        public string narration { get; set; }
        public bool is_chq_payment { get; set; }
        public string chq_number { get; set; }
        public Nullable<DateTime> chq_value_date { get; set; }
        public int chq_bank_id { get; set; }
        public string chq_bank_name { get; set; }
        public bool is_own_bank_chq { get; set; }
    }
}