﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
     [DataContract(Namespace = "")]
    public class Payment
    {

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public long PaymentLogId { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(51, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string CustReference { get; set; }


        [DataType(DataType.Text)]
        [StringLength(51, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string AltCustReference { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(101, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string CustomerName { get; set; }

        [DataMember(IsRequired = false)]
        [DataAnnotationsExtensions.Integer]
        public Nullable<long> CustomerPhoneNumber { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Numeric]
        public decimal Amount { get; set; }


        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string DepositSlipNumber { get; set; }


        [DataMember(IsRequired = true)]
        public List<PaymentItem> PaymentItems { get; set; }

    }

}