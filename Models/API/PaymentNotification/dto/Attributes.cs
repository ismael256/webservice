﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
     [DataContract(Namespace = "")]
    public class Attributes
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string Value { get; set; }
    }
}