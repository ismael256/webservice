﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    public class ItemAmountAdjustmentModel
    {
        public long adjustment_id { get; set; }
        public decimal adjustment_amount { get; set; }
        public string adjustment_comments { get; set; }
        public string adjustment_reason { get; set; }
        public decimal tellers_adjustment_amount { get; set; }
        public string adjustment_type { get; set; }
        public decimal isw_coll_fee { get; set; }
        public decimal bank_coll_fee { get; set; }
        public bool is_pending { get; set; }
    }
}