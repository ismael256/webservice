﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    public class BankDetails
    {
        public string BankCode { get; set; }
        public int BankId { get; set; }
        public string CBNBankCode { get; set; }
        public string BankName { get; set; }
        public string BankIIN { get; set; }
    }
}