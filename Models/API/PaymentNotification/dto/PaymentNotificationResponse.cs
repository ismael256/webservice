﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    [DataContract(Namespace = "")]
    public class PaymentNotificationResponse
    {
        [DataMember]
        public List<PaymentResponse> Payments { get; set; }
    }
}