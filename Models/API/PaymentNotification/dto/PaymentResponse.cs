﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    [DataContract(Namespace = "")]
    public class PaymentResponse
    {
         [DataMember]
         public long PaymentLogId { get; set; }

         [DataMember]
         public string PaymentReferenceNumber { get; set; }

         [DataMember]
         public int ResponseCode { get; set; }

         [DataMember]
         public decimal Amount { get; set; }
    }
}