﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    public class TransactionPaymentDetails
    {
        public string PaymentTypeCode { get; set; }
        public string PaymentTypeName { get; set; }
        public string LeadBankId { get; set; }
        public decimal TotalFee { get; set; }
        public decimal SwitchFees { get; set; }
        public decimal CollectionBankFees { get; set; }
        public decimal LeadBankFees { get; set; }
    }
}