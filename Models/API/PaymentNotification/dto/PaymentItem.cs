﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
     [DataContract(Namespace = "")]
    public class PaymentItem
    {
        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Numeric]
        public int ItemCode {get;set;}

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Numeric]
        public decimal ItemAmount { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string ItemSignature { get; set; }
    }
}