﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.dto
{
    public class PaymentItemDetailsModel
    {
        public long paylog_details_id { get; set; }
        public long paylog_details_paylog_id { get; set; }
        public decimal paylog_details_amount { get; set; }
        public string paylog_details_comments { get; set; }
        public int payment_type_id { get; set; }
        public string payment_type_code { get; set; }
        public string payment_type_name { get; set; }
        public int lead_bank_id { get; set; }
        public string lead_bank_code { get; set; }
        public string lead_bank_name { get; set; }
        public string lead_bank_cbn_code { get; set; }
        public string cbn_account_name { get; set; }
        public string cbn_account_number { get; set; }
        public Nullable<int> ref_payment_type_id { get; set; }
        public string ref_payment_type_code { get; set; }
        public string ref_payment_type_name { get; set; }
        public Nullable<int> iso_bank_id { get; set; }
        public string iso_bank_name { get; set; }
        public string iso_bank_code { get; set; }
        public string iso_bank_cbn_code { get; set; }
        public Nullable<decimal> total_fee { get; set; }
        public Nullable<decimal> isw_trans_fee_pld { get; set; }
        public Nullable<decimal> bank_coll_fee_pld { get; set; }
        public Nullable<decimal> lead_bank_fee_pld { get; set; }
        public decimal iso_fee_pld { get; set; }
        public Nullable<decimal> customer_borne_fee { get; set; }
        public Nullable<int> export_status { get; set; }
        public Nullable<int> item_type { get; set; }
        public int reversal_status { get; set; }
        public long payment_item_category_id { get; set; }
        public string payment_item_category_name { get; set; }
        public bool has_additional_data { get; set; }
        public string merchandize_pin { get; set; }
        public string merchandize_serial_no { get; set; }
        public string merchandize_customer_msg { get; set; }
        public string third_party_account_number { get; set; }
        public Nullable<decimal> vas_provider_fee { get; set; }
        public Nullable<int> vas_provider_bank_id { get; set; }
        public string vas_provider_bank_name { get; set; }
        public string vas_provider_account_no { get; set; }
        public Nullable<decimal> terminal_owner_fee { get; set; }
        public Nullable<int> terminal_owner_bank_id { get; set; }
        public string terminal_owner_bank_name { get; set; }
        public string terminal_owner_account_no { get; set; }
        public string iso_bank_account_no { get; set; }
        public bool fee_uses_percentage { get; set; }
    }
}