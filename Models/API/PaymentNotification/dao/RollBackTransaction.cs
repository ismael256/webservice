﻿using System;
using System.Collections.Generic;
using System.Linq;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.dao
{
    public class RollBackTransaction
    {
        public static void DeleteTransactionDetails(long payment_log_id,long trans_id,long pl_trans_id)
        {
            if (PaymentLogIdExists(payment_log_id))
                DeletePaymentLog(payment_log_id);

            if(TransactionLogIdExists(trans_id))
                DeleteTransactionLog(trans_id);

            if (TransactionPaymentLogLinkRecordExists(pl_trans_id))
                DeletePaymentTransactionLogLinkRecord(pl_trans_id);
        }

        private static bool PaymentLogIdExists(long payment_log_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                return dbEnt.tbl_payments_log.Any(log => log.payment_log_id == payment_log_id);
            }
        }

        private static void DeletePaymentLog(long payment_log_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                //first delete the file
                var logid = (from log in dbEnt.tbl_payments_log
                              where log.payment_log_id == payment_log_id
                             select log).SingleOrDefault();

                if (logid != null)
                {
                    dbEnt.tbl_payments_log.Remove(logid);
                    dbEnt.SaveChanges();
                }
            }
        }

        private static bool TransactionLogIdExists(long trans_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                return dbEnt.tbl_transactions.Any(log => log.trans_id == trans_id);
            }
        }

        private static void DeleteTransactionLog(long trans_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                //first delete the file
                var logid = (from log in dbEnt.tbl_transactions
                             where log.trans_id == trans_id
                             select log).SingleOrDefault();

                if (logid != null)
                {
                    dbEnt.tbl_transactions.Remove(logid);
                    dbEnt.SaveChanges();
                }
            }
        }

        private static bool TransactionPaymentLogLinkRecordExists(long pl_trans_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                return dbEnt.tbl_payments_log_transactions.Any(log => log.pl_trans_id == pl_trans_id);
            }
        }

        private static void DeletePaymentTransactionLogLinkRecord(long pl_trans_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                //first delete the file
                var logid = (from log in dbEnt.tbl_payments_log_transactions
                             where log.pl_trans_id == pl_trans_id
                             select log).SingleOrDefault();

                if (logid != null)
                {
                    dbEnt.tbl_payments_log_transactions.Remove(logid);
                    dbEnt.SaveChanges();
                }
            }
        }

        private static bool ItemDetailsLogIdExists(long detailsId)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                return dbEnt.tbl_payments_log_details.Any(log => log.paylog_details_id == detailsId);
            }
        }

        public static void DeleteItemDetailsLog(long detailsId)
        {
            if (ItemDetailsLogIdExists(detailsId))
            {
                using (var dbEnt = new PayDirectModelEntities())
                {
                    //first delete the file
                    var logid = (from log in dbEnt.tbl_payments_log_details
                                 where log.paylog_details_id == detailsId
                                 select log).SingleOrDefault();

                    if (logid != null)
                    {
                        dbEnt.tbl_payments_log_details.Remove(logid);
                        dbEnt.SaveChanges();
                    }
                }
            }
        }

        private static bool ItemAdjustmentLogIdExists(long adjustmentId)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                return dbEnt.tbl_payments_log_adjustments.Any(log => log.adjustment_id == adjustmentId);
            }
        }

        public static void DeleteLogAdjustmentRecord(long adjustmentId)
        {
            if (ItemAdjustmentLogIdExists(adjustmentId))
            {
                using (var dbEnt = new PayDirectModelEntities())
                {
                    //first delete the file
                    var logid = (from log in dbEnt.tbl_payments_log_adjustments
                                 where log.adjustment_id == adjustmentId
                                 select log).SingleOrDefault();

                    if (logid != null)
                    {
                        dbEnt.tbl_payments_log_adjustments.Remove(logid);
                        dbEnt.SaveChanges();
                    }
                }
            }
        }
    }
} 