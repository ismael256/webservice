﻿using System;
using System.Collections.Generic;
using System.Linq;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;
using System.Web;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models.API.PaymentNotification.dao
{
    public class RetrievePaymentItems
    {
        public long PaymentItemId { get; set; }
        public string PaymentItemCode { get; set; }
        public string PaymentItemName { get; set; }
        public int PaymentProductId { get; set; }
        public decimal PaymentItemAmount { get; set; }
        public bool PaymentItemIsFixedAmount { get; set; }
        public int LeadBankId { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<decimal> ItemFees { get; set; }
        public Nullable<bool> RequiresAuth { get; set; }
        public Nullable<int> ItemFeesId { get; set; }
        public Nullable<int> IsoBankId { get; set; }
        public bool IsBohItem { get; set; }
        public int EntFeeId { get; set; }
        public long CategoryId { get; set; } //todo for items that need categories implement this
        public int ValidationType { get; set; }
        public bool EnableNotification { get; set; }
        public string RouteId { get; set; }
        public bool ReqCustomerAuth { get; set; }
        public bool RespondWithMerchandize { get; set; }


        public static RetrievePaymentItems RetrievePaymentItem(int paymentItem_id, int productId, int collectingBank_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                string payItemId = paymentItem_id.ToString();

                var payItem = dbEnt.SP_psp_retrieve_payment_item(payItemId).ToList<Dao.FilteredPayDirectModelEntities.tbl_payment_items>();

                if (payItem.Count > 1)
                    throw new IncorrectApplicationConfigurationException("There appears to be more than one item configured with the same item Id, please contact the PayDirect administrator to rectify this issue : PaymentId " + paymentItem_id);

                if (PayDirectWebService.Helpers.TextHelper.IsNullOrEmpty(payItem))
                    throw new SecurityViolationException("There is no payment item with the specified Id , please contact the PayDirect administrator to rectify this issue : PaymentId " + paymentItem_id);


                var paymentItem_ = payItem[0];

                if (paymentItem_.lead_bank_id == -1)
                    paymentItem_.lead_bank_id = GetLeadBankId(productId, collectingBank_id);

                return PaymentItemAttributes(paymentItem_);
            }
        }

        private static RetrievePaymentItems PaymentItemAttributes(tbl_payment_items paymentItem_)
        {
                RetrievePaymentItems pytItems  = new RetrievePaymentItems();

                pytItems.PaymentItemCode = paymentItem_.item_code;
                pytItems.PaymentItemName = paymentItem_.item_name;
                pytItems.PaymentProductId = paymentItem_.item_college_id;
                pytItems.PaymentItemAmount = paymentItem_.item_amount;
                pytItems.PaymentItemIsFixedAmount = paymentItem_.item_is_fixed_amount;
                pytItems.LeadBankId = paymentItem_.lead_bank_id;
                pytItems.IsActive = paymentItem_.is_active;
                pytItems.ItemFees = paymentItem_.item_fees;
                pytItems.RequiresAuth = paymentItem_.requires_auth;
                pytItems.ItemFeesId = paymentItem_.item_fees_id;
                pytItems.IsoBankId = paymentItem_.iso_bank_id;
                pytItems.IsBohItem = paymentItem_.is_boh_item;
                pytItems.EntFeeId = paymentItem_.ent_fee_id;
                pytItems.CategoryId = paymentItem_.category_id;
                pytItems.ValidationType = paymentItem_.validation_type;
                pytItems.EnableNotification = paymentItem_.enable_notification;
                pytItems.RouteId = paymentItem_.route_id;
                pytItems.ReqCustomerAuth = paymentItem_.req_customer_auth;
                pytItems.RespondWithMerchandize = paymentItem_.respond_with_merchandize;

                return pytItems;
        }

        public static List<RetrievePaymentItems>  RetrieveProductPaymentItems(int productId,int collectingBank_id)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                List<RetrievePaymentItems>  paymentItems = new List<RetrievePaymentItems>();

                var payItems = dbEnt.SP_psp_retrieve_all_payment_item(productId).ToList<Dao.FilteredPayDirectModelEntities.tbl_payment_items>();
                if (payItems.Count == 0)
                    throw new IncorrectApplicationConfigurationException("There are no payment items set up for this product , please contact the PayDirect administrator to rectify this issue : Product-ID " + productId);

                foreach(var paymentItem in payItems)
                {
                    if (paymentItem.lead_bank_id == -1)
                        paymentItem.lead_bank_id = GetLeadBankId(productId, collectingBank_id);
                    paymentItems.Add(PaymentItemAttributes(paymentItem));
                }

                return paymentItems;
            }
        }

        public static int GetLeadBankId(int productId,int collectingBankId)
        {
            using (var dbCore = new PaydirectCoreEntities())
            {
                var lead_bank_id = (from log in dbCore.tbl_lead_banks
                                    where log.product_id == productId && log.coll_bank_id == collectingBankId
                             select log.lead_bank_id).SingleOrDefault();
                if (lead_bank_id != null)
                    return lead_bank_id;
                else
                    return -1;
            }
        }
    }
} 