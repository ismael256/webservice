﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.API.PaymentNotification.dto;
using System.Data.SqlClient;
using System.Data;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models.API.PaymentNotification.dao
{
    public class PaymentTransactionLog
    {
        public long payment_log_id { get; set; }
        public string pay_index { get; set; }
        public long chq_payment_id { get; set; }
        string reciept_reference { get; set; }
        public long trans_id { get; set; }
        public long pl_trans_id { get; set; }

        public void InsertTransactionLog(TransactionDetailsModel transaction)
        {
           // try
           // {
                SqlParameter payment_log_id = new SqlParameter
                {
                    ParameterName = "payment_log_id",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };

                SqlParameter chq_payment_id = new SqlParameter
                {
                    ParameterName = "chq_payment_id",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };

                SqlParameter pay_index = new SqlParameter
                {
                    ParameterName = "pay_index",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15
                };

                SqlParameter reciept_reference = new SqlParameter
                {
                    ParameterName = "reciept_reference",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };

                SqlParameter pl_trans_id = new SqlParameter
                {
                    ParameterName = "pl_trans_id",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };

                SqlParameter trans_id = new SqlParameter
                {
                    ParameterName = "trans_id",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };

                SqlParameter useReceiptGeneratorParameter = new SqlParameter
                {
                    ParameterName = "useReceiptGenerator",
                    Value = true,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter channel_idParameter = new SqlParameter
                {
                    ParameterName = "channel_id",
                    Value = transaction.channel_id,
                    Direction = System.Data.ParameterDirection.Input,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter channel_nameParameter = new SqlParameter
                {
                    ParameterName = "channel_name",
                    Value = transaction.channel_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter coll_acct_numParameter = new SqlParameter
                {
                    ParameterName = "coll_acct_num",
                    Value = transaction.coll_acct_num,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 30
                };
                SqlParameter payment_log_dateParameter = new SqlParameter
                {
                    ParameterName = "payment_log_date",
                    Value = transaction.payment_log_date,
                    SqlDbType = SqlDbType.DateTime
                };
                SqlParameter settlement_dateParameter = new SqlParameter
                {
                    ParameterName = "settlement_date",
                    SqlDbType = SqlDbType.DateTime,
                    Value = transaction.settlement_date,
                };
                SqlParameter remittance_dateParameter = new SqlParameter
                {
                    ParameterName = "remittance_date",
                    Value = transaction.remittance_date,
                    SqlDbType = SqlDbType.DateTime
                };
                SqlParameter payment_ref_numParameter = new SqlParameter
                {
                    ParameterName = "payment_ref_num",
                    Value = transaction.payment_ref_num,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter reciept_numberParameter = new SqlParameter
                {
                    ParameterName = "reciept_number",
                    Value = transaction.reciept_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cust_numberParameter = new SqlParameter
                {
                    ParameterName = "cust_number",
                    Value = transaction.cust_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter product_unique_fieldParameter = new SqlParameter
                {
                    ParameterName = "product_unique_field",
                    Value = transaction.product_unique_field,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter old_cust_numberParameter = new SqlParameter
                {
                    ParameterName = "old_cust_number",
                    Value = transaction.old_cust_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cust_nameParameter = new SqlParameter
                {
                    ParameterName = "cust_name",
                    Value = transaction.cust_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter cust_addressParameter = new SqlParameter
                {
                    ParameterName = "cust_address",
                    Value = transaction.cust_address,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 250
                };
                SqlParameter cust_phoneParameter = new SqlParameter
                {
                    ParameterName = "cust_phone",
                    Value = transaction.cust_phone,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cust_emailParameter = new SqlParameter
                {
                    ParameterName = "cust_email",
                    Value = transaction.cust_email,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cust_dobParameter = new SqlParameter
                {
                    ParameterName = "cust_dob",
                    Value = transaction.cust_dob,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cust_location_codeParameter = new SqlParameter
                {
                    ParameterName = "cust_location_code",
                    Value = transaction.cust_location_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter cust_location_nameParameter = new SqlParameter
                {
                    ParameterName = "cust_location_name",
                    Value = transaction.cust_location_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cust_sub_location_codeParameter = new SqlParameter
                {
                    ParameterName = "cust_sub_location_code",
                    Value = transaction.cust_sub_location_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter cust_sub_location_nameParameter = new SqlParameter
                {
                    ParameterName = "cust_sub_location_name",
                    Value = transaction.cust_sub_location_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter card_idParameter = new SqlParameter
                {
                    ParameterName = "card_id",
                    Value = transaction.card_id
                };
                SqlParameter card_panParameter = new SqlParameter
                {
                    ParameterName = "card_pan",
                    Value = transaction.card_pan,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 19
                };
                SqlParameter account_idParameter = new SqlParameter
                {
                    ParameterName = "account_id",
                    Value = transaction.account_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter account_numberParameter = new SqlParameter
                {
                    ParameterName = "account_number",
                    Value = transaction.account_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 25
                };
                SqlParameter user_db_idParameter = new SqlParameter
                {
                    ParameterName = "user_db_id",
                    Value = transaction.user_db_id,
                    SqlDbType = SqlDbType.BigInt
                };
                SqlParameter user_staff_codeParameter = new SqlParameter
                {
                    ParameterName = "user_staff_code",
                    Value = transaction.user_staff_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 15
                };
                SqlParameter user_login_nameParameter = new SqlParameter
                {
                    ParameterName = "user_login_name",
                    Value = transaction.user_login_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter user_last_nameParameter = new SqlParameter
                {
                    ParameterName = "user_last_name",
                    Value = transaction.user_last_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 25
                };
                SqlParameter user_other_namesParameter = new SqlParameter
                {
                    ParameterName = "user_other_names",
                    Value = transaction.user_other_names,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter role_nameParameter = new SqlParameter
                {
                    ParameterName = "role_name",
                    Value = transaction.role_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 30
                };
                SqlParameter branch_idParameter = new SqlParameter
                {
                    ParameterName = "branch_id",
                    Value = transaction.branch_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter branch_codeParameter = new SqlParameter
                {
                    ParameterName = "branch_code",
                    Value = transaction.branch_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter branch_nameParameter = new SqlParameter
                {
                    ParameterName = "branch_name",
                    Value = transaction.branch_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 30
                };
                SqlParameter misc_branch_codeParameter = new SqlParameter
                {
                    ParameterName = "misc_branch_code",
                    Value = transaction.misc_branch_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter bank_idParameter = new SqlParameter
                {
                    ParameterName = "bank_id",
                    Value = transaction.bank_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter cbn_bank_codeParameter = new SqlParameter
                {
                    ParameterName = "cbn_bank_code",
                    Value = transaction.cbn_bank_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter bank_codeParameter = new SqlParameter
                {
                    ParameterName = "bank_code",
                    Value = transaction.bank_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter bank_nameParameter = new SqlParameter
                {
                    ParameterName = "bank_name",
                    Value = transaction.bank_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter misc_bank_codeParameter = new SqlParameter
                {
                    ParameterName = "misc_bank_code",
                    Value = transaction.misc_bank_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter deposit_slip_noParameter = new SqlParameter
                {
                    ParameterName = "deposit_slip_no",
                    Value = transaction.deposit_slip_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter payment_method_idParameter = new SqlParameter
                {
                    ParameterName = "payment_method_id",
                    Value = transaction.payment_method_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter payment_method_nameParameter = new SqlParameter
                {
                    ParameterName = "payment_method_name",
                    Value = transaction.payment_method_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20

                };
                SqlParameter isw_trans_feeParameter = new SqlParameter
                {
                    ParameterName = "isw_trans_fee",
                    Value = transaction.isw_trans_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter isw_trans_fee_percentParameter = new SqlParameter
                {
                    ParameterName = "isw_trans_fee_percent",
                    Value = transaction.isw_trans_fee_percent,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter bank_coll_fee_percentParameter = new SqlParameter
                {
                    ParameterName = "bank_coll_fee_percent",
                    Value = transaction.bank_coll_fee_percent,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter lead_bank_fees_percentParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_fees_percent",
                    Value = transaction.lead_bank_fees_percent,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter bank_coll_feeParameter = new SqlParameter
                {
                    ParameterName = "bank_coll_fee",
                    Value = transaction.bank_coll_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter fees_amountParameter = new SqlParameter
                {
                    ParameterName = "fees_amount",
                    Value = transaction.fees_amount,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter iso_fees_percentParameter = new SqlParameter
                {
                    ParameterName = "iso_fees_percent",
                    Value = transaction.iso_fees_percent,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter uses_percentageParameter = new SqlParameter
                {
                    ParameterName = "uses_percentage",
                    Value = transaction.uses_percentage,
                    SqlDbType = SqlDbType.Bit

                };
                SqlParameter isw_trans_feesParameter = new SqlParameter
                {
                    ParameterName = "isw_trans_fees",
                    Value = transaction.isw_trans_fees,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter coll_bank_feesParameter = new SqlParameter
                {
                    ParameterName = "coll_bank_fees",
                    Value = transaction.coll_bank_fees,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter lead_bank_feesParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_fees",
                    Value = transaction.iso_fees,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter iso_feesParameter = new SqlParameter
                {
                    ParameterName = "iso_fees",
                    Value = transaction.iso_fees,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter payment_currencyParameter = new SqlParameter
                {
                    ParameterName = "payment_currency",
                    Value = transaction.payment_currency,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 6
                };
                SqlParameter commentsParameter = new SqlParameter
                {
                    ParameterName = "comments",
                    Value = transaction.comments,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 255
                };
                SqlParameter state_idParameter = new SqlParameter
                {
                    ParameterName = "state_id",
                    Value = transaction.state_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter state_nameParameter = new SqlParameter
                {
                    ParameterName = "state_name",
                    Value = transaction.state_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter is_chq_paymentParameter = new SqlParameter
                {
                    ParameterName = "is_chq_payment",
                    Value = transaction.is_chq_payment,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter chq_numberParameter = new SqlParameter
                {
                    ParameterName = "chq_number",
                    Value = transaction.chq_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter chq_value_dateParameter = new SqlParameter
                {
                    ParameterName = "chq_value_date",
                    Value = transaction.chq_value_date,
                    SqlDbType = SqlDbType.DateTime
                };
                SqlParameter chq_bank_idParameter = new SqlParameter
                {
                    ParameterName = "chq_bank_id",
                    Value = transaction.chq_bank_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter chq_bank_nameParameter = new SqlParameter
                {
                    ParameterName = "chq_bank_name",
                    Value = transaction.chq_bank_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter is_own_bank_chqParameter = new SqlParameter
                {
                    ParameterName = "is_own_bank_chq",
                    Value = transaction.is_own_bank_chq,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter response_codeParameter = new SqlParameter
                {
                    ParameterName = "response_code",
                    Value = transaction.response_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 2
                };
                SqlParameter trans_amountParameter = new SqlParameter
                {
                    ParameterName = "trans_amount",
                    Value = transaction.trans_amount,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter approved_amountParameter = new SqlParameter
                {
                    ParameterName = "approved_amount",
                    Value = transaction.approved_amount,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter currency_codeParameter = new SqlParameter
                {
                    ParameterName = "currency_code",
                    Value = transaction.payment_currency,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 3
                };
                SqlParameter surchargeParameter = new SqlParameter
                {
                    ParameterName = "surcharge",
                    Value = transaction.surcharge,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter surcharge_currencyParameter = new SqlParameter
                {
                    ParameterName = "surcharge_currency",
                    Value = transaction.payment_currency,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 3
                };
                SqlParameter trans_date_timeParameter = new SqlParameter
                {
                    ParameterName = "trans_date_time",
                    Value = transaction.payment_log_date,
                    SqlDbType = SqlDbType.DateTime
                };
                SqlParameter from_account_typeParameter = new SqlParameter
                {
                    ParameterName = "from_account_type",
                    Value = transaction.from_account_type,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 2
                };
                SqlParameter from_account_numberParameter = new SqlParameter
                {
                    ParameterName = "from_account_number",
                    Value = transaction.from_account_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 25
                };
                SqlParameter to_account_numberParameter = new SqlParameter
                {
                    ParameterName = "to_account_number",
                    Value = transaction.to_account_number,
                     SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter trans_typeParameter = new SqlParameter
                {
                    ParameterName = "trans_type",
                    Value = transaction.trans_type,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 25
                };
                SqlParameter terminal_idParameter = new SqlParameter
                {
                    ParameterName = "terminal_id",
                    Value = transaction.terminal_id,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 8
                };
                SqlParameter retrieval_ref_noParameter = new SqlParameter
                {
                    ParameterName = "retrieval_ref_no",
                    Value = transaction.retrieval_ref_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 12
                };
                SqlParameter card_seq_numberParameter = new SqlParameter
                {
                    ParameterName = "card_seq_number",
                    Value = transaction.card_seq_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 3
                };
                SqlParameter card_exp_dayParameter = new SqlParameter
                {
                    ParameterName = "card_exp_day",
                    Value = transaction.card_exp_day,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 2
                };
                SqlParameter card_exp_monthParameter = new SqlParameter
                {
                    ParameterName = "card_exp_month",
                    Value = transaction.card_exp_month,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 2
                };
                SqlParameter card_exp_yearParameter = new SqlParameter
                {
                    ParameterName = "card_exp_year",
                    Value = transaction.card_exp_year,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 4
                };
                SqlParameter to_account_typeParameter = new SqlParameter
                {
                    ParameterName = "to_account_type",
                    Value = transaction.to_account_type,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 2
                };
                SqlParameter student_numberParameter = new SqlParameter
                {
                    ParameterName = "student_number",
                    Value = transaction.student_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter student_nameParameter = new SqlParameter
                {
                    ParameterName = "student_name",
                    Value = transaction.student_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 200
                };
                SqlParameter student_college_codeParameter = new SqlParameter
                {
                    ParameterName = "student_college_code",
                    Value = transaction.student_college_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 10
                };
                SqlParameter student_college_nameParameter = new SqlParameter
                {
                    ParameterName = "student_college_name",
                    Value = transaction.student_college_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 200
                };
                SqlParameter student_college_idParameter = new SqlParameter
                {
                    ParameterName = "student_college_id",
                    Value = transaction.student_college_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter student_departmentParameter = new SqlParameter
                {
                    ParameterName = "student_department",
                    Value = transaction.student_department,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter student_facultyParameter = new SqlParameter
                {
                    ParameterName = "student_faculty",
                    Value = transaction.student_faculty,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter student_levelParameter = new SqlParameter
                {
                    ParameterName = "student_level",
                    Value = transaction.student_level,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter sessionParameter = new SqlParameter
                {
                    ParameterName = "session",
                    Value = transaction.college_session,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter semesterParameter = new SqlParameter
                {
                    ParameterName = "semester",
                    Value = transaction.semester,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 3
                };
                SqlParameter data_push_statusParameter = new SqlParameter
                {
                    ParameterName = "data_push_status",
                    Value = transaction.data_push_status,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter surcharge_deposit_slip_noParameter = new SqlParameter
                {
                    ParameterName = "surcharge_deposit_slip_no",
                    Value = transaction.surcharge_deposit_slip_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter alt_ref_flagParameter = new SqlParameter
                {
                    ParameterName = "alt_ref_flag",
                    Value = transaction.alt_ref_flag,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter alt_ref_lengthParameter = new SqlParameter
                {
                    ParameterName = "alt_ref_length",
                    Value = transaction.alt_ref_length,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter additional_infoParameter = new SqlParameter
                {
                    ParameterName = "additional_info",
                    Value = transaction.additional_info,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 512
                };
                SqlParameter is_feeParameter = new SqlParameter
                {
                    ParameterName = "is_fee",
                    Value = transaction.is_fee,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter coll_acct_nameParameter = new SqlParameter
                {
                    ParameterName = "coll_acct_name",
                    Value = transaction.coll_acct_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 256
                };
                SqlParameter use_ent_receipt_noParameter = new SqlParameter
                {
                    ParameterName = "use_ent_receipt_no",
                    Value = transaction.use_ent_receipt_no,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter narrationParameter = new SqlParameter
                {
                    ParameterName = "narration",
                    Value = transaction.narration,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 128
                };

                string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectEntEntities_ADO"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connstring))
                {
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "usp_payments_multiple_item_INSERT_2";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(channel_idParameter);
                    command.Parameters.Add(channel_nameParameter);
                    command.Parameters.Add(coll_acct_numParameter);
                    command.Parameters.Add(payment_log_dateParameter);
                    command.Parameters.Add(settlement_dateParameter);
                    command.Parameters.Add(remittance_dateParameter);
                    command.Parameters.Add(payment_ref_numParameter);
                    command.Parameters.Add(reciept_numberParameter);
                    command.Parameters.Add(cust_numberParameter);
                    command.Parameters.Add(cust_nameParameter);
                    command.Parameters.Add(cust_addressParameter);
                    command.Parameters.Add(cust_phoneParameter);
                    command.Parameters.Add(cust_emailParameter);
                    command.Parameters.Add(card_idParameter);
                    command.Parameters.Add(card_panParameter);
                    command.Parameters.Add(account_idParameter);
                    command.Parameters.Add(account_numberParameter);
                    command.Parameters.Add(user_db_idParameter);
                    command.Parameters.Add(user_staff_codeParameter);
                    command.Parameters.Add(user_login_nameParameter);
                    command.Parameters.Add(user_last_nameParameter);
                    command.Parameters.Add(user_other_namesParameter);
                    command.Parameters.Add(role_nameParameter);
                    command.Parameters.Add(branch_idParameter);
                    command.Parameters.Add(branch_codeParameter);
                    command.Parameters.Add(branch_nameParameter);
                    command.Parameters.Add(misc_branch_codeParameter);
                    command.Parameters.Add(bank_idParameter);
                    command.Parameters.Add(cbn_bank_codeParameter);
                    command.Parameters.Add(bank_codeParameter);
                    command.Parameters.Add(bank_nameParameter);
                    command.Parameters.Add(misc_bank_codeParameter);
                     command.Parameters.Add(deposit_slip_noParameter);
                    command.Parameters.Add(payment_method_idParameter);
                    command.Parameters.Add(payment_method_nameParameter);
                    command.Parameters.Add(isw_trans_feeParameter);
                    command.Parameters.Add(fees_amountParameter);
                    command.Parameters.Add(iso_fees_percentParameter);
                    command.Parameters.Add(uses_percentageParameter);
                    command.Parameters.Add(isw_trans_feesParameter);
                    command.Parameters.Add(coll_bank_feesParameter);
                    command.Parameters.Add(lead_bank_feesParameter);
                    command.Parameters.Add(iso_feesParameter);
                    command.Parameters.Add(payment_currencyParameter);
                    command.Parameters.Add(commentsParameter);
                    command.Parameters.Add(payment_log_id);
                    command.Parameters.Add(pay_index);
                    command.Parameters.Add(chq_payment_id);
                    command.Parameters.Add(reciept_reference);
                    command.Parameters.Add(response_codeParameter);
                    command.Parameters.Add(trans_amountParameter);
                    command.Parameters.Add(approved_amountParameter);
                    command.Parameters.Add(currency_codeParameter);
                    command.Parameters.Add(surchargeParameter);
                    command.Parameters.Add(surcharge_currencyParameter);
                    command.Parameters.Add(trans_date_timeParameter);
                    command.Parameters.Add(from_account_typeParameter);
                    command.Parameters.Add(from_account_numberParameter);
                    command.Parameters.Add(to_account_numberParameter);
                    command.Parameters.Add(trans_typeParameter);
                    command.Parameters.Add(terminal_idParameter);
                    command.Parameters.Add(retrieval_ref_noParameter);
                    command.Parameters.Add(card_seq_numberParameter);
                    command.Parameters.Add(card_exp_dayParameter);
                    command.Parameters.Add(card_exp_monthParameter);
                    command.Parameters.Add(card_exp_yearParameter);
                    command.Parameters.Add(to_account_typeParameter);
                    command.Parameters.Add(trans_id);
                    command.Parameters.Add(pl_trans_id);
                    command.Parameters.Add(student_numberParameter);
                    command.Parameters.Add(student_nameParameter);
                    command.Parameters.Add(student_college_codeParameter);
                    command.Parameters.Add(student_college_nameParameter);
                    command.Parameters.Add(student_college_idParameter);
                    command.Parameters.Add(student_departmentParameter);
                    command.Parameters.Add(student_facultyParameter);
                    command.Parameters.Add(student_levelParameter);
                    command.Parameters.Add(sessionParameter);
                    command.Parameters.Add(semesterParameter);

                    connection.Open();
                    command.ExecuteNonQuery();


                    connection.Close();

                    this.payment_log_id  = long.Parse(payment_log_id.Value.ToString());
                    this.pay_index =  pay_index.Value.ToString();
                    this.chq_payment_id  = long.Parse(chq_payment_id.Value.ToString());
                    this.reciept_reference  = reciept_reference.Value.ToString();
                    this.trans_id   = long.Parse(trans_id.Value.ToString());
                    this.pl_trans_id = long.Parse(pl_trans_id.Value.ToString());
                }
           /* }
            catch (Exception err)
            {
                throw new InternalServerException("A database Exception was thrown when logging payment details");
            }*/
        }
    }
} 