﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotification.dto;

namespace PayDirectWebService.Models.API.PaymentNotification.dao
{
    public class PaymentItemLogDetails
    {
        public long AdjustmentId { get; set; }

        public void InsertPaymentItemLogDetails(PaymentItemDetailsModel paymentItemDetails)
        {
           // try
           // {
                SqlParameter paylog_details_paylog_idParameter = new SqlParameter
                {
                    ParameterName = "paylog_details_paylog_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.paylog_details_paylog_id,
                    SqlDbType = SqlDbType.BigInt
                };
                SqlParameter paylog_details_amountParameter = new SqlParameter
                {
                    ParameterName = "paylog_details_amount",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.paylog_details_amount,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter paylog_details_commentsParameter = new SqlParameter
                {
                    ParameterName = "paylog_details_comments",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.paylog_details_comments,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 255
                };
                SqlParameter payment_type_idParameter = new SqlParameter
                {
                    ParameterName = "payment_type_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.payment_type_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter payment_type_codeParameter = new SqlParameter
                {
                    ParameterName = "payment_type_code",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.payment_type_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter payment_type_nameParameter = new SqlParameter
                {
                    ParameterName = "payment_type_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.payment_type_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter lead_bank_idParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.lead_bank_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter lead_bank_codeParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_code",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.lead_bank_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter lead_bank_nameParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.lead_bank_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter lead_bank_cbn_codeParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_cbn_code",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.lead_bank_cbn_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter cbn_account_nameParameter = new SqlParameter
                {
                    ParameterName = "cbn_account_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.cbn_account_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter cbn_account_numberParameter = new SqlParameter
                {
                    ParameterName = "cbn_account_number",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.cbn_account_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter ref_payment_type_idParameter = new SqlParameter
                {
                    ParameterName = "ref_payment_type_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.ref_payment_type_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter ref_payment_type_codeParameter = new SqlParameter
                {
                    ParameterName = "ref_payment_type_code",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.ref_payment_type_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter ref_payment_type_nameParameter = new SqlParameter
                {
                    ParameterName = "ref_payment_type_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.ref_payment_type_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter iso_bank_idParameter = new SqlParameter
                {
                    ParameterName = "iso_bank_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.iso_bank_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter iso_bank_nameParameter = new SqlParameter
                {
                    ParameterName = "iso_bank_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.iso_bank_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter iso_bank_codeParameter = new SqlParameter
                {
                    ParameterName = "iso_bank_code",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.iso_bank_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter iso_bank_cbn_codeParameter = new SqlParameter
                {
                    ParameterName = "iso_bank_cbn_code",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.iso_bank_cbn_code,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 20
                };
                SqlParameter total_feeParameter = new SqlParameter
                {
                    ParameterName = "total_fee",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.paylog_details_paylog_id,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter isw_trans_fee_pldParameter = new SqlParameter
                {
                    ParameterName = "isw_trans_fee_pld",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.isw_trans_fee_pld,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter bank_coll_fee_pldParameter = new SqlParameter
                {
                    ParameterName = "bank_coll_fee_pld",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.bank_coll_fee_pld,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter lead_bank_fee_pldParameter = new SqlParameter
                {
                    ParameterName = "lead_bank_fee_pld",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.lead_bank_fee_pld,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter iso_fee_pldParameter = new SqlParameter
                {
                    ParameterName = "iso_fee_pld",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.iso_fee_pld,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter customer_borne_feeParameter = new SqlParameter
                {
                    ParameterName = "customer_borne_fee",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.customer_borne_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter export_statusParameter = new SqlParameter
                {
                    ParameterName = "export_status",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.export_status,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter item_typeParameter = new SqlParameter
                {
                    ParameterName = "item_type",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.item_type,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter reversal_statusParameter = new SqlParameter
                {
                    ParameterName = "reversal_status",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.reversal_status,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter payment_item_category_idParameter = new SqlParameter
                {
                    ParameterName = "payment_item_category_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.payment_item_category_id,
                    SqlDbType = SqlDbType.BigInt
                };
                SqlParameter payment_item_category_nameParameter = new SqlParameter
                {
                    ParameterName = "payment_item_category_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.payment_item_category_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter has_additional_dataParameter = new SqlParameter
                {
                    ParameterName = "has_additional_data",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.has_additional_data,
                    SqlDbType = SqlDbType.Bit
                };
                SqlParameter merchandize_pinParameter = new SqlParameter
                {
                    ParameterName = "merchandize_pin",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.merchandize_pin,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter merchandize_serial_noParameter = new SqlParameter
                {
                    ParameterName = "merchandize_serial_no",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.merchandize_serial_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter merchandize_customer_msgParameter = new SqlParameter
                {
                    ParameterName = "merchandize_customer_msg",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.merchandize_customer_msg,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 255
                };
                SqlParameter third_party_account_numberParameter = new SqlParameter
                {
                    ParameterName = "third_party_account_number",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.third_party_account_number,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 100
                };
                SqlParameter vas_provider_feeParameter = new SqlParameter
                {
                    ParameterName = "vas_provider_fee",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.vas_provider_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter vas_provider_bank_idParameter = new SqlParameter
                {
                    ParameterName = "vas_provider_bank_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.vas_provider_bank_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter vas_provider_bank_nameParameter = new SqlParameter
                {
                    ParameterName = "vas_provider_bank_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.vas_provider_bank_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter vas_provider_account_noParameter = new SqlParameter
                {
                    ParameterName = "vas_provider_account_no",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.vas_provider_account_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter terminal_owner_feeParameter = new SqlParameter
                {
                    ParameterName = "terminal_owner_fee",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.terminal_owner_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter terminal_owner_bank_idParameter = new SqlParameter
                {
                    ParameterName = "terminal_owner_bank_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.terminal_owner_bank_id,
                    SqlDbType = SqlDbType.Int
                };
                SqlParameter terminal_owner_bank_nameParameter = new SqlParameter
                {
                    ParameterName = "terminal_owner_bank_name",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.terminal_owner_bank_name,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter terminal_owner_account_noParameter = new SqlParameter
                {
                    ParameterName = "terminal_owner_account_no",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.terminal_owner_account_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter iso_bank_account_noParameter = new SqlParameter
                {
                    ParameterName = "iso_bank_account_no",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = paymentItemDetails.iso_bank_account_no,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter paylog_details_id = new SqlParameter
                {
                    ParameterName = "paylog_details_id",
                    Direction = System.Data.ParameterDirection.Output,
                    SqlDbType = SqlDbType.BigInt
                };

                  string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectEntEntities_ADO"].ConnectionString;
                  using (SqlConnection connection = new SqlConnection(connstring))
                  {
                      SqlCommand command = new SqlCommand();
                      command.Connection = connection;
                      command.CommandText = "usp_payments_log_detail_INSERT";
                      command.CommandType = CommandType.StoredProcedure;

                      command.Parameters.Add(paylog_details_paylog_idParameter);
                      command.Parameters.Add(paylog_details_id);
                      command.Parameters.Add(paylog_details_amountParameter);
                      command.Parameters.Add(paylog_details_commentsParameter);
                      command.Parameters.Add(payment_type_idParameter);
                      command.Parameters.Add(payment_type_codeParameter);
                      command.Parameters.Add(payment_type_nameParameter);
                      command.Parameters.Add(lead_bank_idParameter);
                      command.Parameters.Add(lead_bank_codeParameter);
                      command.Parameters.Add(lead_bank_nameParameter);
                      command.Parameters.Add(lead_bank_cbn_codeParameter);
                      command.Parameters.Add(cbn_account_nameParameter);
                      command.Parameters.Add(cbn_account_numberParameter);
                      command.Parameters.Add(ref_payment_type_idParameter);
                      command.Parameters.Add(ref_payment_type_codeParameter);
                      command.Parameters.Add(ref_payment_type_nameParameter);
                      command.Parameters.Add(iso_bank_idParameter);
                      command.Parameters.Add(iso_bank_nameParameter);
                      command.Parameters.Add(iso_bank_cbn_codeParameter);
                      command.Parameters.Add(total_feeParameter);
                      command.Parameters.Add(isw_trans_fee_pldParameter);
                      command.Parameters.Add(bank_coll_fee_pldParameter);
                      command.Parameters.Add(lead_bank_fee_pldParameter);
                      command.Parameters.Add(iso_fee_pldParameter);
                      command.Parameters.Add(customer_borne_feeParameter);
                      command.Parameters.Add(export_statusParameter);
                      command.Parameters.Add(item_typeParameter);
                      command.Parameters.Add(reversal_statusParameter);
                      command.Parameters.Add(payment_item_category_idParameter);
                      command.Parameters.Add(payment_item_category_nameParameter);
                      command.Parameters.Add(has_additional_dataParameter);
                      command.Parameters.Add(merchandize_pinParameter);
                      command.Parameters.Add(merchandize_serial_noParameter);
                      command.Parameters.Add(merchandize_customer_msgParameter);
                      command.Parameters.Add(third_party_account_numberParameter);
                      command.Parameters.Add(vas_provider_feeParameter);
                      command.Parameters.Add(vas_provider_bank_idParameter);
                      command.Parameters.Add(vas_provider_bank_nameParameter);
                      command.Parameters.Add(vas_provider_account_noParameter);
                      command.Parameters.Add(terminal_owner_feeParameter);
                      command.Parameters.Add(terminal_owner_bank_idParameter);
                      command.Parameters.Add(terminal_owner_bank_nameParameter);
                      command.Parameters.Add(terminal_owner_account_noParameter);
                      command.Parameters.Add(iso_bank_account_noParameter);

                      connection.Open();
                      command.ExecuteNonQuery();

                      AdjustmentId = long.Parse(paylog_details_id.Value.ToString());

                      connection.Close();
                  }
             /*   }
                catch (Exception e)
                {
                    throw new InternalServerException("A database Exception was thrown when logging paymentItem details");
                } */
        
        }

        
    }
}