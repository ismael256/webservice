﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotification.dto;

namespace PayDirectWebService.Models.API.PaymentNotification.dao
{
    public class PaymentItemAmountAdjustment
    {
        public static void InsertItemAmountAdjustment(ItemAmountAdjustmentModel adjustmentDetails)
        {
            try
            { 
                SqlParameter adjustment_idParameter = new SqlParameter
                {
                    ParameterName = "adjustment_id",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.adjustment_id,
                    SqlDbType = SqlDbType.BigInt
                };
                SqlParameter adjustment_amountParameter = new SqlParameter
                {
                    ParameterName = "adjustment_amount",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.adjustment_amount,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter adjustment_commentsParameter = new SqlParameter
                {
                    ParameterName = "adjustment_comments",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.adjustment_comments,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter adjustment_reasonParameter = new SqlParameter
                {
                    ParameterName = "adjustment_reason",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.adjustment_reason,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter tellers_adjustment_amountParameter = new SqlParameter
                {
                    ParameterName = "tellers_adjustment_amount",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.tellers_adjustment_amount,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter adjustment_typeParameter = new SqlParameter
                {
                    ParameterName = "adjustment_type",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.adjustment_type,
                    SqlDbType = SqlDbType.VarChar,
                    Size = 50
                };
                SqlParameter isw_coll_feeParameter = new SqlParameter
                {
                    ParameterName = "isw_coll_fee",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.isw_coll_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter bank_coll_feeParameter = new SqlParameter
                {
                    ParameterName = "bank_coll_fee",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.bank_coll_fee,
                    SqlDbType = SqlDbType.Money
                };
                SqlParameter is_pendingParameter = new SqlParameter
                {
                    ParameterName = "is_pending",
                    Direction = System.Data.ParameterDirection.Input,
                    Value = adjustmentDetails.is_pending,
                    SqlDbType = SqlDbType.Bit
                };

                 string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PayDirectEntEntities_ADO"].ConnectionString;
                 using (SqlConnection connection = new SqlConnection(connstring))
                 {
                     SqlCommand command = new SqlCommand();
                     command.Connection = connection;
                     command.CommandText = "usp_payments_log_adjustment_INSERT";
                     command.CommandType = CommandType.StoredProcedure;

                     command.Parameters.Add(adjustment_idParameter);
                     command.Parameters.Add(adjustment_amountParameter);
                     command.Parameters.Add(adjustment_commentsParameter);
                     command.Parameters.Add(adjustment_reasonParameter);
                     command.Parameters.Add(tellers_adjustment_amountParameter);
                     command.Parameters.Add(adjustment_typeParameter);
                     command.Parameters.Add(isw_coll_feeParameter);
                     command.Parameters.Add(bank_coll_feeParameter);
                     command.Parameters.Add(is_pendingParameter);

                     connection.Open();
                     command.ExecuteNonQuery();

                     connection.Close();
                 }
            }
            catch (Exception e)
            {
                throw new InternalServerException("A database Exception was thrown when logging paymentItem adjustment details");
            } 
        }
    }
}