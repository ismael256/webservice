﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.PaymentNotification.util
{
    public class Fee
    {
        public bool IsBorneByCustomer { get; set; }
        public bool IsBorneByBiller { get; set; }
        public bool UsesPercentage { get; set; }
        public decimal FeeCap { get; set; }

        public decimal TransactionFees { get; set; }
        public decimal TransactionAmount { get; set; }
        public decimal ISOFeeAmount { get; set; }
        public decimal LeadBankFeeAmount { get; set; }
        public decimal CollectingBankFeeAmount { get; set; }
        public decimal MerchantFeeAmount { get; set; }
        public decimal SwitchFeeAmount { get; set; }
        public decimal CustomerBorneFee { get; set; }
    }


}