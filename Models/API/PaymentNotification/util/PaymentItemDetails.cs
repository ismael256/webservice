﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.Utils;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.API.PaymentNotification.dto;
using PayDirectWebService.Models.API.PaymentNotification.dao;
using PayDirectWebService.Models.API.PaymentNotification.util;

namespace PayDirectWebService.Models.API.PaymentNotification.util
{
    public class PaymentItemDetails
    {
        
        public long PaymentLogId;
        private List<PaymentItem> PaymentItem_;
        bool UsesClassicFee;
        decimal PaymentAmount;
        int ProductId;
        int CollectingBankId;

        public List<PaymentItemDetailsModel> PaymentItemModelDetails;

        public enum FeeTypes
        {
            ClassicFee,
            EnterpriseFee
        };

        public PaymentItemDetails(long paymentLogId, bool usesClassicFeeRegime, List<PaymentItem> paymentItem, decimal paymentAmount,int productId,int collectingBankId)
        {
            PaymentLogId = paymentLogId;
            UsesClassicFee = usesClassicFeeRegime;
            PaymentAmount = paymentAmount;
            this.ProductId = productId;
            this.CollectingBankId = collectingBankId;

            PaymentItem_ = new List<PaymentItem>();
            PaymentItem_ = paymentItem;

            this.ExtractPaymentItemLogDetails();
        }


        private void ExtractPaymentItemLogDetails()
        {
            if (PaymentItem_.Count == 0)
                throw new DataFormatException("There are no payment items in the request");

            List<PaymentItemDetailsModel> payItemDetails = new List<PaymentItemDetailsModel>();
            decimal totalAmt = 0;

            if (PaymentItem_.Count == 1)
            {
                PaymentItem pytItm = PaymentItem_[0];
                RetrievePaymentItems paymentItem = RetrievePaymentItems.RetrievePaymentItem(pytItm.ItemCode, this.ProductId, this.CollectingBankId);
                this.CheckItemBelongsToProduct(paymentItem.PaymentProductId);
                totalAmt = pytItm.ItemAmount;
                pytItm.ItemAmount = pytItm.ItemAmount * 100;//todo convert amount into small denominsation
                payItemDetails.Add(ExtractItemdetails(paymentItem, pytItm));
            }
            else
            {
                for (var c = 0; c < PaymentItem_.Count; c++)
                {
                    PaymentItem pytItm = PaymentItem_[c];
                    totalAmt += pytItm.ItemAmount;
                    RetrievePaymentItems paymentItem = RetrievePaymentItems.RetrievePaymentItem(pytItm.ItemCode, this.ProductId, this.CollectingBankId);
                    this.CheckItemBelongsToProduct(paymentItem.PaymentProductId);
                    pytItm.ItemAmount = pytItm.ItemAmount * 100;//todo convert amount into small denominsation
                    payItemDetails.Add(ExtractItemdetails(paymentItem, pytItm));

                   
                }
            }

            if (totalAmt != PaymentAmount)
                throw new FormatException("The value in the amount field should be the summation of individual item amounts");

            PaymentItemModelDetails =  payItemDetails;
        }

        private void CheckItemBelongsToProduct(int itemProductId)
        {
            if (itemProductId != this.ProductId)
                throw new FormatException("One of the payment-Items used, doesn't belong to the selected product");
        }
       /* private void checkItemSignature(string signature,decimal code,int itemcode,)
        {

        }*/

        private BankDetails SetBankDetails(int bankId)
        {
            BankDetails bankDetails = new BankDetails();
            IList<Bank> paydBanks = ApplicationDataHelper.GetPayDirectConfigApplicationData().PayDirectBanks;

            for (int c = 0; c < paydBanks.Count(); c++)
            {
                if (paydBanks[c].bankID == bankId)
                {
                    bankDetails.BankId = paydBanks[c].bankID;
                    bankDetails.BankName = paydBanks[c].bankName;
                    bankDetails.CBNBankCode  = paydBanks[c].cbnBankCode;
                    bankDetails.BankCode = paydBanks[c].bankCode;

                    return bankDetails;
                }
            }
            throw new IncorrectApplicationConfigurationException("payment-Item leadbank/ISO bank doesn't exist. please contact the PayDirect administrator to rectify this issue BankId:" + bankId);
           
        }

        private PaymentItemDetailsModel ExtractItemdetails(RetrievePaymentItems paymentItem, PaymentItem payItem)
        {
            PaymentItemDetailsModel payLogDet = new PaymentItemDetailsModel();
            BankDetails leadBank = SetBankDetails(paymentItem.LeadBankId);

            BankDetails ISOBank;
            int isoBank = (int)paymentItem.IsoBankId;//TODO find out what the ISO bank is 
            if (isoBank == 0)
                ISOBank = leadBank;
            else
                ISOBank = SetBankDetails(isoBank);
            //fee calculation
            ClassicFeeCalculation fee;
            int fee_id;
            try
            {
                fee_id = (int)paymentItem.ItemFeesId;
                if (fee_id == 0)
                   throw new IncorrectApplicationConfigurationException("Payment Item has no configured fees :ItemCode : " + payItem.ItemCode);
            }
            catch (Exception e) { throw new IncorrectApplicationConfigurationException("Payment Item has no configured fees :ItemCode : " + payItem.ItemCode); }

            if (UsesClassicFee)
            {
                fee = new ClassicFeeCalculation(fee_id, payItem.ItemAmount);
            }
            else
                throw new IncorrectApplicationConfigurationException("This product uses a feature that hasn't been implemented yet.The Enterprise fee feature hasn't yet been implemented");

            payLogDet.paylog_details_paylog_id = PaymentLogId;
            payLogDet.paylog_details_amount = payItem.ItemAmount;
            payLogDet.paylog_details_comments = "";
            payLogDet.payment_type_id = payItem.ItemCode;
            payLogDet.payment_type_code = paymentItem.PaymentItemCode;
            payLogDet.payment_type_name = paymentItem.PaymentItemName;
            payLogDet.lead_bank_id = paymentItem.LeadBankId;
            payLogDet.lead_bank_code = leadBank.BankCode;
            payLogDet.lead_bank_name = leadBank.BankName;
            payLogDet.lead_bank_cbn_code = leadBank.CBNBankCode;
            payLogDet.cbn_account_name = "";
            payLogDet.cbn_account_number = "";
            payLogDet.ref_payment_type_code = "";
            payLogDet.ref_payment_type_name = "";
            payLogDet.iso_bank_id = paymentItem.IsoBankId;
            payLogDet.iso_bank_name = ISOBank.BankName;
            payLogDet.iso_bank_code = ISOBank.BankCode;
            payLogDet.iso_bank_cbn_code = ISOBank.CBNBankCode;
            payLogDet.total_fee = fee.TransactionFees;
            payLogDet.isw_trans_fee_pld = fee.SwitchFeeAmount;
            payLogDet.bank_coll_fee_pld = fee.CollectingBankFeeAmount;
            payLogDet.lead_bank_fee_pld = fee.LeadBankFeeAmount;
            payLogDet.iso_fee_pld = fee.ISOFeeAmount;
            payLogDet.customer_borne_fee = fee.CustomerBorneFee; //for customer borne fee, fill this value
            payLogDet.reversal_status = 0;
            payLogDet.payment_item_category_id = paymentItem.CategoryId;
            payLogDet.payment_item_category_name = "";//todo get category name;
            payLogDet.has_additional_data = false;
            payLogDet.merchandize_pin = "";
            payLogDet.merchandize_serial_no  = "";
            payLogDet.merchandize_customer_msg  = "";
            payLogDet.third_party_account_number  = "";
            payLogDet.vas_provider_bank_name  = "";
            payLogDet.vas_provider_account_no  = "";
            payLogDet.terminal_owner_bank_name  = "";
            payLogDet.terminal_owner_account_no  = "";
            payLogDet.iso_bank_account_no = "";
            payLogDet.export_status = 0;
            payLogDet.item_type = 0;

            if (fee.UsesPercentage)
                payLogDet.fee_uses_percentage = true;
            else
                payLogDet.fee_uses_percentage = true;

            return payLogDet;
        }
    }
}