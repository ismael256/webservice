﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;
using PayDirectWebService.Models.Exceptions;

namespace PayDirectWebService.Models.API.PaymentNotification.util
{
    public class ClassicFeeCalculation : Fee
    {
        private int FeesId { get; set; }
        private decimal FixedTransFees { get; set; }
        private decimal TransFeesPercent { get; set; }
        private decimal IssuerFees { get; set; }
        private decimal Merchant_fees { get; set; }
        private decimal SwitchFees { get; set; }
        private decimal AcquirerFees { get; set; }
        private decimal ProviderFees { get; set; }
        private decimal IssuerFeesPercent { get; set; }
        private decimal MerchantFees_percent { get; set; }
        private decimal SwitchFeesPercent { get; set; }
        private decimal AcquirerFeesPercent { get; set; }
        private decimal ProviderFeesPercent { get; set; }
        private string FeeName { get; set; }
        private bool UseFeeEntityCap { get; set; }


        public ClassicFeeCalculation(int feesid,decimal paymentAmount) : base () //todo convert all money to decimal
        {
            FeesId = feesid;
            TransactionAmount = paymentAmount;

            this.RetrieveItemFee(feesid);

            if (IsBorneByCustomer)
            {
                IsBorneByBiller = false;
                CustomerBorneFee = TransactionFees;
            }
        }

        private void RetrieveItemFee(int feesid)
        {
            using (var dbCore = new PaydirectCoreEntities())
            {
                var itemFees = dbCore.SP_psp_retrieve_fee(feesid).ToList<Dao.FilteredPaydirectCoreModelEntities.tbl_fees>();

                //if (itemFees.Count() < 1)
                //    throw new IncorrectApplicationConfigurationException("The fee has wrongly been configured, please contact the PayDirect administrator to rectify this issue : FeeId " + feesid);
             
                var itemFee = itemFees[0];// we only expect 1 fee

                FeesId = (int)itemFee.fees_id;
                IsBorneByCustomer = itemFee.is_surcharge;
                UsesPercentage = itemFee.uses_percentage;
                FixedTransFees = itemFee.fixed_trans_fees;
                TransFeesPercent = itemFee.trans_fees_percent;
                FeeCap = itemFee.fees_max_amt_value;
                IssuerFees = itemFee.issuer_fees;
                Merchant_fees = itemFee.merchant_fees;
                SwitchFees = itemFee.switch_fees;
                AcquirerFees = itemFee.acquirer_fees;
                ProviderFees = itemFee.provider_fees;
                IssuerFeesPercent = itemFee.issuer_fees_percent;
                MerchantFees_percent = itemFee.merchant_fees_percent;
                SwitchFeesPercent = itemFee.switch_fees_percent;
                AcquirerFeesPercent = itemFee.acquirer_fees_percent;
                ProviderFeesPercent = itemFee.provider_fees_percent;
                FeeName = itemFee.fee_name;
                UseFeeEntityCap = itemFee.use_fee_entity_cap;
            }
        }

        /// <summary>
        /// calculates the transaction fees, they can be a flat fee or a %
        /// </summary>
        private void CalculateTransactionFees()
        {
            if (UsesPercentage && FixedTransFees !=0)
                throw new IncorrectApplicationConfigurationException("The fee has wrongly been configured, please contact the PayDirect administrator to rectify this issue : FeeId " + FeesId);

            if (TransFeesPercent == 0)
                TransactionFees = 0;

            if (UsesPercentage)
            {
                if (TransFeesPercent != 0)
                    TransactionFees = (TransFeesPercent / 100) * TransactionAmount;
                else
                    TransactionFees = 0;
            }
            else
                TransactionFees = FixedTransFees;
        }

        private void CalculateIsoFees()
        {
            ISOFeeAmount = CalculateFees(ProviderFeesPercent, ProviderFees);
        }

        private void CalculateLeadBankFees()
        {
            LeadBankFeeAmount = CalculateFees(IssuerFeesPercent, IssuerFees);
        }

        private void CalculateSwitchFees()
        {
            SwitchFeeAmount = CalculateFees(SwitchFeesPercent, SwitchFees);
        }

        private void CalculateMerchantFees()
        {
            MerchantFeeAmount = CalculateFees(MerchantFees_percent, Merchant_fees);
        }

        private void CalculateCollectingBankFees()
        {
            CollectingBankFeeAmount =  CalculateFees(AcquirerFeesPercent, AcquirerFees);
        }

        private decimal ImplementFeeCap(decimal fee)
        {
            if (fee > FeeCap)
                return FeeCap;
            else
                return fee;
        }

        private decimal CalculateFees(decimal feePercent, decimal flat_fee)
        {
            if (UsesPercentage)
                return  ImplementFeeCap((feePercent / 100) * TransactionFees);
            else
                return ImplementFeeCap(flat_fee);
        }

    }
} 