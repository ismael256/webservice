﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.API.PaymentNotification.dto;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.API.PaymentNotification.dao;
using PayDirectWebService.Models.API.Security.dto;

namespace PayDirectWebService.Models.API.PaymentNotification.util
{
    public class MakePayment
    {
        public static PaymentNotificationResponse doPayment(PaymentNotificationRequest paymentNotificationRequest, MerchantApiServiceprovider acquirerAttributes)
        {
                PaymentNotificationResponse notificatnResponse = new PaymentNotificationResponse();
                notificatnResponse.Payments = new List<PaymentResponse>();
                DateTime time = PayDirectWebService.Helpers.TimeHelper.FromUnixTime(paymentNotificationRequest.TransmissionDate);

                foreach (var paymt in paymentNotificationRequest.Payments)
                {

                    TransactionDetails trxnDetail = new TransactionDetails(paymentNotificationRequest.ProductId, acquirerAttributes.TellerLogin, time, paymt, acquirerAttributes);
                    TransactionDetailsModel trxndetail = trxnDetail.TransactiondetailsModel;
                    PaymentTransactionLog trxnlog = new PaymentTransactionLog();
                    trxnlog.InsertTransactionLog(trxndetail);

                    //insert payment item details tbl_adjustment table, log_details

                    PaymentItemDetails payItmDetails = new PaymentItemDetails(trxnlog.payment_log_id, trxnDetail.ProductDetails.UsesClassicFeeRegime, paymt.PaymentItems, paymt.Amount, paymentNotificationRequest.ProductId, GetCollectingBankId(acquirerAttributes.BankCode));
                    PaymentItemLogDetails payDet = new PaymentItemLogDetails();

                    for (int c = 0; c < payItmDetails.PaymentItemModelDetails.Count; c++)
                    {
                        try
                        {
                            PaymentItemDetailsModel itemDetModel = payItmDetails.PaymentItemModelDetails[c];
                            payDet.InsertPaymentItemLogDetails(itemDetModel);

                            decimal isw_trxnFee = (decimal)itemDetModel.isw_trans_fee_pld;
                            decimal bankCollction_trxnFee = (decimal)itemDetModel.bank_coll_fee_pld;

                            ItemAmountAdjustment adjustmentAmt = new ItemAmountAdjustment(payDet.AdjustmentId, itemDetModel.paylog_details_amount, isw_trxnFee, bankCollction_trxnFee);
                            ItemAmountAdjustmentModel adjustmtAmt = adjustmentAmt.FillItemAmountAdjustmentModel();

                            PaymentItemAmountAdjustment.InsertItemAmountAdjustment(adjustmtAmt);
                        }
                        catch (PaymentException e)
                        {
                            //log back trxn insert and maybe paymentItemdetails and adjustment
                            RollBackTransaction.DeleteTransactionDetails(trxnlog.payment_log_id, trxnlog.trans_id, trxnlog.pl_trans_id);
                            RollBackTransaction.DeleteLogAdjustmentRecord(payDet.AdjustmentId);
                            RollBackTransaction.DeleteItemDetailsLog(payDet.AdjustmentId);

                            throw e.CaughtException;
                        }
                    }
                    PaymentResponse pytResponse = new PaymentResponse();
                    pytResponse.Amount = trxndetail.approved_amount;
                    pytResponse.PaymentLogId = paymt.PaymentLogId;
                    pytResponse.PaymentReferenceNumber = trxndetail.payment_ref_num + trxnlog.pay_index;
                    pytResponse.ResponseCode = Convert.ToInt32(trxndetail.response_code);


                    notificatnResponse.Payments.Add(pytResponse);
                }
                return notificatnResponse;
        }

        private static int GetCollectingBankId(String bankCode)
        {
            IList<Bank> paydBanks = ApplicationDataHelper.GetPayDirectConfigApplicationData().PayDirectBanks;

            for (int c = 0; c < paydBanks.Count(); c++)
            {
                if (paydBanks[c].bankCode == bankCode)
                {
                    return paydBanks[c].bankID;
                }
            }
            throw new IncorrectApplicationConfigurationException("Missing collections account");
        }
    }
}