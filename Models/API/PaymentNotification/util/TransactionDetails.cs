﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models;
using PayDirectWebService.Models.Dto;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Dao.FilteredPaydirectCoreModelEntities;
using PayDirectWebService.Models.API.PaymentNotification.dto;
using PayDirectWebService.Models.API.PaymentNotification.dao;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.Utils;
using PayDirectWebService.Models.API.Security.dto;


namespace PayDirectWebService.Models.API.PaymentNotification.util
{
    public class TransactionDetails
    {
        PaymentOptionConfigs paymentoptionsConfig;

        public int ProductId {get;set;}
        public Products ProductDetails {get;set;}
        public MerchantApiServiceprovider WebServiceAcquirer { get; set; }
       // public PaymentNotificationRequest PaymentNotificationRequest { get; set; }
        public string TellerLogin { get; set; }
        public DateTime PaymentDate { get; set; }
        Payment PaymentVariables { get; set; }
        public TransactionDetailsModel TransactiondetailsModel { get; set; }

        public TransactionDetails(int productId,string tellerLogin,DateTime paymentDate,Payment paymt, MerchantApiServiceprovider acquirerAttributes)
        {
            TransactiondetailsModel = new TransactionDetailsModel();
            this.WebServiceAcquirer = new MerchantApiServiceprovider();
            this.WebServiceAcquirer = acquirerAttributes;
            this.PaymentDate = paymentDate;
            this.ProductId = productId;
            this.PaymentVariables = paymt;
            this.TellerLogin = tellerLogin;
            this.GetProductDetails(); 

            paymentoptionsConfig = new PaymentOptionConfigs();

           // PaymentNotificationRequest = paymentNotificationRequest;
            this.RetrievePaymentAttributes();

            
            this.CalculateSettlementDays();
            this.SetChannelDetails();
            this.SetPaymentRefNumber();
            this.SetTillAccountDetails();
            this.SetBankDetails();
            this.SetMiscelenious();
        }

        /// <summary>
        /// Gets product details from the database  //TODO develop cache system to store previously fetched recorddds
        /// </summary>
        private void GetProductDetails()
        {
            Products prd = PaymentOptionConfigs.GetProductDetails(this.ProductId);
            ProductDetails = prd;

            TransactiondetailsModel.product_id = ProductDetails.ProductId;
            TransactiondetailsModel.product_code = ProductDetails.ProductCode;
            TransactiondetailsModel.product_name = ProductDetails.ProductName;
            TransactiondetailsModel.student_college_code = ProductDetails.ProductCode;
            TransactiondetailsModel.student_college_name = ProductDetails.ProductName;
            TransactiondetailsModel.student_college_id = ProductDetails.ProductId;

            TransactiondetailsModel.uses_percentage = false; //default value coz fees are caluclated at item level
        }

        private void RetrievePaymentAttributes()
        {

            TransactiondetailsModel.user_login_name = this.TellerLogin;
            TransactiondetailsModel.payment_log_date = this.PaymentDate;
            TransactiondetailsModel.bank_code = WebServiceAcquirer.BankCode;
            TransactiondetailsModel.cust_number = PaymentVariables.CustReference;
            TransactiondetailsModel.coll_acct_num = this.WebServiceAcquirer.CollectionsAccount;
            TransactiondetailsModel.old_cust_number = PaymentVariables.AltCustReference;
            TransactiondetailsModel.product_unique_field = "";
            TransactiondetailsModel.cust_name = PaymentVariables.CustomerName;
            TransactiondetailsModel.cust_address = "";
            TransactiondetailsModel.cust_phone = PaymentVariables.CustomerPhoneNumber.ToString();
            TransactiondetailsModel.cust_email = ""; //TODO add email amongst the customer data fields
            TransactiondetailsModel.cust_dob = "";
            TransactiondetailsModel.cust_location_code = "";
            TransactiondetailsModel.cust_location_name = "";
            TransactiondetailsModel.cust_sub_location_code = "";
            TransactiondetailsModel.cust_sub_location_name = "";

            TransactiondetailsModel.user_last_name = PaymentVariables.CustomerName;
            TransactiondetailsModel.user_other_names = "";
            TransactiondetailsModel.role_name = "Bank Teller";
            TransactiondetailsModel.user_staff_code = "";
            TransactiondetailsModel.branch_code = WebServiceAcquirer.BankBranchCode;

            TransactiondetailsModel.deposit_slip_no = PaymentVariables.DepositSlipNumber;
            TransactiondetailsModel.state_name = "";
            TransactiondetailsModel.comments = "";
            TransactiondetailsModel.misc_bank_code = "";
            TransactiondetailsModel.misc_branch_code = "";
            TransactiondetailsModel.payment_currency = WebServiceAcquirer.PaymentCurrencyCode;
            TransactiondetailsModel.student_number = PaymentVariables.CustReference;
            TransactiondetailsModel.student_name = PaymentVariables.CustomerName;
            TransactiondetailsModel.student_department = "";
            TransactiondetailsModel.student_faculty = "";
            TransactiondetailsModel.student_level = "";
            TransactiondetailsModel.college_session = "";
            TransactiondetailsModel.semester = "";
            TransactiondetailsModel.notification_status = false; //TODO UNRESOLVED find a way to change tis back to true when notification takes place
            TransactiondetailsModel.transaction_ref_num = "";
            TransactiondetailsModel.transaction_status = "";
            TransactiondetailsModel.transaction_code = "";
            TransactiondetailsModel.payment_notification_status = 0;
            TransactiondetailsModel.remittance_status = 0;
            TransactiondetailsModel.surcharge_deposit_slip_no = "";
            TransactiondetailsModel.trans_amount = PaymentVariables.Amount;
            TransactiondetailsModel.approved_amount = this.PaymentVariables.Amount;
            TransactiondetailsModel.surcharge = 0;
            TransactiondetailsModel.from_account_type = "00";
            TransactiondetailsModel.to_account_type = "00";
            TransactiondetailsModel.trans_type = "50";
            TransactiondetailsModel.to_account_number = this.WebServiceAcquirer.CollectionsAccount;
            TransactiondetailsModel.from_account_number = "";
            TransactiondetailsModel.additional_info = "";
            TransactiondetailsModel.terminal_id = WebServiceAcquirer.TerminalId;
            TransactiondetailsModel.retrieval_ref_no = "";
            TransactiondetailsModel.use_ent_receipt_no = true;
            TransactiondetailsModel.reversal_lock_start_date = this.PaymentDate;
            TransactiondetailsModel.email_notification_status = 0;
            TransactiondetailsModel.sms_notification_status = 0;
            TransactiondetailsModel.ftp_notification_status = 0;
            TransactiondetailsModel.payment_notification_retries = 0;
            TransactiondetailsModel.reversal_status = 0;
            TransactiondetailsModel.service_notification_status = 0;
            TransactiondetailsModel.loadcard_notification_status = 0;

            this.SetPaymentTypes(MerchantApiServiceprovider.PAYMENT_METHOD);
        }

        private void CalculateSettlementDays()
        {
            SettlementCalculation settle = new SettlementCalculation(ProductDetails.SettlementFrequency, ProductDetails.CashSettlementAddDays, TransactiondetailsModel.payment_log_date, ProductDetails.RemittanceAddDays);

            TransactiondetailsModel.remittance_date = settle.RemittanceDate;
            TransactiondetailsModel.settlement_date = settle.SettlementDate;
        }

        private void SetChannelDetails()
        {
            paymentoptionsConfig.RetrievePaymentChannel();

            TransactiondetailsModel.channel_id = paymentoptionsConfig.ChannelId;
            TransactiondetailsModel.channel_name = paymentoptionsConfig.ChannelName;
        }

        private void SetPaymentRefNumber()
        {
            paymentoptionsConfig.GeneratePaymentRefNumber(WebServiceAcquirer.BankCode, ProductDetails.ProductCode, TransactiondetailsModel.payment_log_date);

            TransactiondetailsModel.payment_ref_num = paymentoptionsConfig.PaymentRefNumber;
            TransactiondetailsModel.reciept_number = paymentoptionsConfig.ReceiptNumber;
        }

        private void GetUserId()
        {

        }

        private void SetTillAccountDetails()
        {
            TransactiondetailsModel.card_id = WebServiceAcquirer.TillCardId;
            TransactiondetailsModel.account_id = WebServiceAcquirer.TillAccountId;
            TransactiondetailsModel.account_number = WebServiceAcquirer.TillAccountNumber;
            TransactiondetailsModel.card_pan = WebServiceAcquirer.TillCardPAN;

            TransactiondetailsModel.card_seq_number = "000";
            TransactiondetailsModel.card_exp_day = "12";
            TransactiondetailsModel.card_exp_month = "12";
            TransactiondetailsModel.card_exp_year = "2022";
        }

        private void SetBankDetails()
        {
            IList<Bank> paydBanks = ApplicationDataHelper.GetPayDirectConfigApplicationData().PayDirectBanks;
            

            for (int c = 0; c < paydBanks.Count(); c++)
            {
                if (paydBanks[c].bankCode.ToLower().Trim() == WebServiceAcquirer.BankCode.ToLower().Trim())
                {
                    TransactiondetailsModel.bank_id = paydBanks[c].bankID;
                    TransactiondetailsModel.bank_name = paydBanks[c].bankName;
                    TransactiondetailsModel.cbn_bank_code = paydBanks[c].cbnBankCode;

                    BankBranchDetails branchDetail = PaymentOptionConfigs.RetrieveBankBranchDetail(WebServiceAcquirer.BankBranchCode, paydBanks[c].bankID);
                    TransactiondetailsModel.branch_id = branchDetail.BranchID;
                    TransactiondetailsModel.branch_name = branchDetail.BranchName;
                    TransactiondetailsModel.branch_code = branchDetail.BranchCode;

                    return;
                }
            }
            throw new IncorrectApplicationConfigurationException("Specified Bank Code doesn't exist " + paydBanks.Count());
        }

        private void SetPaymentTypes(string paymentMethod)
        {
            IList<PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities.tbl_payment_type> paymentTypes = ApplicationDataHelper.GetPayDirectConfigApplicationData().PaymentTypes;

            foreach(var paymethod in paymentTypes)
            {
                if (paymethod.payment_type_name == paymentMethod)//Cash, Cheque, Internal Transfer are xpected
                {
                    TransactiondetailsModel.payment_method_id = paymethod.payment_type_id;
                    TransactiondetailsModel.payment_method_name = paymethod.payment_type_name;
                    return;
                }
                if(paymentMethod == "Card") //todo hard coded because card is not in db table but resolves to belw details
                {
                    TransactiondetailsModel.payment_method_id = 4;     //todo Hard Coded
                    TransactiondetailsModel.payment_method_name = "Debit Card"; //todo Hard Coded
                    return;
                }
            }

            throw new DataFormatException("Invalid PaymentMethod :" + paymentMethod);
        }

        private void SetMiscelenious()
        {
            TransactiondetailsModel.data_push_status = false;
            TransactiondetailsModel.alt_ref_length = 0;
            TransactiondetailsModel.is_fee = false;
            TransactiondetailsModel.coll_acct_name = "";
            TransactiondetailsModel.narration = "";
            TransactiondetailsModel.response_code = "00";
            TransactiondetailsModel.is_chq_payment = false;
            TransactiondetailsModel.chq_number = "";
            TransactiondetailsModel.chq_bank_id = 0;
            TransactiondetailsModel.chq_bank_name = "";
            TransactiondetailsModel.is_own_bank_chq = false;
            TransactiondetailsModel.isw_trans_fees = 0;
            TransactiondetailsModel.isw_trans_fee = 0;
            TransactiondetailsModel.fees_amount = 0;
            TransactiondetailsModel.coll_bank_fees = 0;
            TransactiondetailsModel.lead_bank_fees = 0;
            TransactiondetailsModel.iso_fees = 0;
            TransactiondetailsModel.isw_trans_fee_percent = 0;
            TransactiondetailsModel.bank_coll_fee_percent = 0;
            TransactiondetailsModel.lead_bank_fees_percent = 0;
            TransactiondetailsModel.bank_coll_fee = 0;
            TransactiondetailsModel.state_id = 0;
            TransactiondetailsModel.payment_currency = this.WebServiceAcquirer.PaymentCurrencyCode;
            TransactiondetailsModel.user_db_id = PaymentOptionConfigs.GetTellerUserId(this.WebServiceAcquirer.BankCode,this.TellerLogin);
        }
    }
} 