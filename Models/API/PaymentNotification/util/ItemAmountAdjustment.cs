﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.API.PaymentNotification.dto;

namespace PayDirectWebService.Models.API.PaymentNotification.util
{
    public class ItemAmountAdjustment
    {
        public long AdjustmentId { get; set; }
        public decimal AdjustmentAmount { get; set; }
        public string AdjustmentComments { get; set; }
        public string AdjustmentReason { get; set; }
        public decimal TellersAdjustmentAmount { get; set; }
        public string AdjustmentType { get; set; }
        public decimal IswCollFee { get; set; }
        public decimal BankCollFee { get; set; }
        public bool IsPending { get; set; }

        public ItemAmountAdjustment(long adjustmentId, decimal adjustmentAmount, decimal iswCollFee, decimal bankCollFee)
        {
            AdjustmentId = adjustmentId;
            AdjustmentAmount = adjustmentAmount;
            IswCollFee = iswCollFee;
            BankCollFee = bankCollFee;
        }

        public ItemAmountAdjustmentModel FillItemAmountAdjustmentModel()
        {
            ItemAmountAdjustmentModel obj = new ItemAmountAdjustmentModel();
            
            obj.adjustment_id  = AdjustmentId;
            obj.adjustment_amount  = AdjustmentAmount;
            obj.adjustment_comments = AdjustmentComments;
            obj.adjustment_reason  = AdjustmentReason;
            obj.tellers_adjustment_amount = TellersAdjustmentAmount;
            obj.adjustment_type = AdjustmentType;
            obj.isw_coll_fee  = IswCollFee;
            obj.bank_coll_fee  = BankCollFee;
            obj.is_pending = IsPending;

            return obj;
        }

    }
}