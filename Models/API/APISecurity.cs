﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.API.CustomerValidation;
using PayDirectWebService.Models.API.PaymentNotification;
using PayDirectWebService.Models.Dto;
using PayDirectWebService.Helpers;
/*
namespace PayDirectWebService.Models.API
{
   
    public class APISecurity
    {
        public ServiceAcquirerDetails WebServiceAcquirerDetails { get; set; }
        public SimpleAES AESEncrypt { get; set; }
        public AES4all AESEcryption {get;set;}
        public string DecryptedCustomerRef { get; set; }

        int _productCode;
        decimal _amount;
        long _paymentDate;
        long _paymentId;
        string _custRef;
        string _encryptedCustRef;
        long _paymentLogId;

        /// <summary>
        /// Used by payment Notification Request
        /// </summary>
        /// <param name="acquirerPublicKey"></param>
        /// <param name="productCode"></param>
        /// <param name="amount"></param>
        /// <param name="paymentDate"></param>
        /// <param name="paymentId"></param>
        /// <param name="customerRef"></param>
        public APISecurity(string acquirerPublicKey, int productCode, decimal amount, long paymentDate, long paymentId, string customerRef)
        {
            this._productCode = productCode;
            this._amount = amount;
            this._paymentDate = paymentDate;
            this._paymentId = paymentId;
            this._encryptedCustRef = customerRef;

            WebServiceAcquirerDetails = ServiceAcquirerDetails.RetrieveServiceAcquire(acquirerPublicKey);
            DecryptCustomerReference();
        }

        /// <summary>
        /// used by customerNumber verification
        /// </summary>
        /// <param name="acquirerPublicKey"></param>
        /// <param name="productCode"></param>
        /// <param name="custRef"></param>
        public APISecurity(string acquirerPublicKey, int productCode, string custRef)
        {
            this._productCode = productCode;
            this._custRef = custRef;

            WebServiceAcquirerDetails = ServiceAcquirerDetails.RetrieveServiceAcquire(acquirerPublicKey);
        }

        /// <summary>
        /// used by payment Reversal
        /// </summary>
        /// <param name="acquirerPublicKey"></param>
        /// <param name="productCode"></param>
        /// <param name="amount"></param>
        /// <param name="paymentId">payment-Id or reversalId</param>
        /// <param name="paymentLogId"></param>
        public APISecurity(string acquirerPublicKey, int productCode, decimal amount, long paymentId, long paymentLogId)
        {
            this._productCode = productCode;
            this._amount = amount;
            this._paymentLogId = paymentLogId;
            this._paymentId = paymentId;

            WebServiceAcquirerDetails = ServiceAcquirerDetails.RetrieveServiceAcquire(acquirerPublicKey);
        }

        /// <summary>
        /// used by the payment status request
        /// </summary>
        /// <param name="acquirerPublicKey"></param>
        /// <param name="productCode"></param>
        /// <param name="paymentId"></param>
        /// <param name="paymentLogId"></param>
        public APISecurity(string acquirerPublicKey, int productCode, long paymentId, long paymentLogId)
        {
            this._productCode = productCode;
            this._paymentLogId = paymentLogId;
            this._paymentId = paymentId;

            WebServiceAcquirerDetails = ServiceAcquirerDetails.RetrieveServiceAcquire(acquirerPublicKey);
        }

        public bool CheckPaymentRequestHashValue(string hashValue)//private key + productcode + amount + paymentdate + payment id + customer_ref
        {
            string msg = WebServiceAcquirerDetails.PrivateKey + this._productCode + this._amount + this._paymentDate + _paymentId + this.DecryptedCustomerRef;

            string compiledVal = SHA512Helper.Signature(msg);
            if (compiledVal.ToLower() == hashValue.ToLower())
                return true;
            else
                return false;
        }

        public bool CheckCustomerRequestHashValue(string hashValue)//private key + productcode + custRef 
        {
            string msg = WebServiceAcquirerDetails.PrivateKey + this._productCode + this._custRef;

            string compiledVal = SHA512Helper.Signature(msg);
            if (compiledVal.ToLower() == hashValue.ToLower())
                return true;
            else
                return false;
        }

        public bool CheckPaymentReversalHashValue(string hashValue)//private key + productcode + amount +  paymentId + paymentLogId
        {
            string msg = WebServiceAcquirerDetails.PrivateKey + this._productCode + this._amount + this._paymentId + this._paymentLogId;

            string compiledVal = SHA512Helper.Signature(msg);
            if (compiledVal.ToLower() == hashValue.ToLower())
                return true;
            else
                return false;
        }

        public bool CheckPaymentStatusHashValue(string hashValue)//private key + productcode +  paymentId + paymentLogId
        {
            string msg = WebServiceAcquirerDetails.PrivateKey + this._productCode + this._paymentId + this._paymentLogId;

            string compiledVal = SHA512Helper.Signature(msg);
            if (compiledVal.ToLower() == hashValue.ToLower())
                return true;
            else
                return false;
        }

        private void DecryptCustomerReference()
        {
            this.DecryptedCustomerRef =  Helpers.TrippleDES.Decrypt(this._encryptedCustRef,this.WebServiceAcquirerDetails.PrivateKey); //decrypt customer number
            this._custRef = this.DecryptedCustomerRef;
        }
    }
} */