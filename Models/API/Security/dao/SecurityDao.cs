﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web; 
using PayDirectWebService.Models.Dao.Service;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.Security.dto;


namespace PayDirectWebService.Models.API.Security.dao
{
    public class SecurityDao
    {
        public static bool TransactionSignatureExists(string signature)
        {
            using (var db = new PayDirectWebServiceEntities())
            {
                if (db.tbl_accessLogs.Any(trxn => trxn.signature == signature))
                    return true;
                else
                    return false;
            }
        }

        public static bool TransactionNonceExists(string nonce)
        {
            using (var db = new PayDirectWebServiceEntities())
            {
                if (db.tbl_accessLogs.Any(trxn => trxn.nonce == nonce))
                    return true;
                else
                    return false;
            }
        }

        public static bool ClientIdExists(string clientId)
        {
            using (var db = new PayDirectWebServiceEntities())
            {
                if (db.tbl_serviceProviders.Any(trxn => trxn.publicKey == clientId))
                    return true;
                else
                    return false;
            }
        }
        
        public static bool IfClientIdMatchesMerchantApiProviderClientId(string clientId,int merchantid)
        {
            using (var db = new PayDirectWebServiceEntities())
            {
                 var exists = (from mer in db.tbl_merchantProviders
                             join serv in db.tbl_serviceProviders on mer.serviceProviderId equals serv.serviceProviderId
                              where mer.merchantId == merchantid && serv.publicKey == clientId
                              select mer.merchantId).Any();
                 if (exists)
                    return true;
                else
                    return false;
            }
        }

        public static MerchantApiServiceprovider GetClientAuthAttributes(string clientId)
        {
            using (var db = new PayDirectWebServiceEntities())
            {
                var auth = (from nde in db.tbl_serviceProviders
                            where nde.publicKey == clientId
                            select nde).SingleOrDefault();

                if(auth == null)
                    throw new SecurityViolationException("Incorrect Authorization Header");
                
                MerchantApiServiceprovider clientAuth = new MerchantApiServiceprovider();
                clientAuth.IpAddress = auth.ipAddress;
                clientAuth.Name = auth.name;
                clientAuth.PrivateKey = auth.privateKey;
                clientAuth.PublicKey = auth.publicKey;
                clientAuth.ServiceProviderId = auth.serviceProviderId;
                clientAuth.AcquirerId = auth.acquirerId;
                clientAuth.BankCode = auth.tbl_acquirerDetails.bankCode;
                clientAuth.BankName = auth.tbl_acquirerDetails.bankName;
                clientAuth.TillAccountId = auth.tbl_acquirerDetails.tillAccountId;
                clientAuth.TillAccountNumber = auth.tbl_acquirerDetails.tillAccountNumber;
                clientAuth.TillCardId = auth.tbl_acquirerDetails.tillCardId;
                clientAuth.TillCardPAN = auth.tbl_acquirerDetails.tillCardPAN;

                clientAuth.TerminalId = auth.terminalId;
                clientAuth.CollectionsAccount = auth.tbl_acquirerDetails.collectionsAccount;
                clientAuth.BankBranchCode = auth.tbl_acquirerDetails.bankBranchCode;
                clientAuth.PaymentCurrencyCode = auth.tbl_acquirerDetails.currency;
                clientAuth.TellerLogin = auth.tbl_acquirerDetails.tellerLogin;
                clientAuth.TellerNames = auth.tbl_acquirerDetails.tellerNames;

               
               

                return clientAuth;
            }
        } 
    }
} 