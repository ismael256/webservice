﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayDirectWebService.Models.API.Security.dto
{
    public class MerchantApiServiceprovider
    {
        public int ServiceProviderId { get; set; }
        public string Name { get; set; }
        public string IpAddress { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
        public int AcquirerId { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public int TillAccountId { get; set; }
        public int TillCardId { get; set; }
        public string TillCardPAN { get; set; }
        public string TillAccountNumber { get; set; }
        public string TerminalId { get; set; }
        public string CollectionsAccount {get;set;}
        public string BankBranchCode {get;set;}
        public string PaymentCurrencyCode { get; set; }
        public static  string PAYMENT_METHOD = "Internal Transfer";
        public string TellerLogin { get; set; }
        public string TellerNames { get; set; }
    }
}