﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;/*
using IIFTS_WebApi.Areas.processor.Models.Utils.api.dto;
using IIFTS_WebApi.Areas.processor.Models.BeneficiaryValidation.dto;
using IIFTS_WebApi.Helpers;
using IIFTS_WebApi.Models.Exceptions;
using IIFTS_WebApi.Areas.processor.Models.Routing.dto;
using IIFTS_WebApi.Areas.processor.Models.Exceptions;

namespace PayDirectWebService.Models.Security.dao
{
    public class ToDownstreamResponseMessageSecure
    {
        string sharedSecretKey;
        string processorClientId;

        public ToDownstreamResponseMessageSecure(string _sharedSecretKey, string _processorClientId)
        {
            this.sharedSecretKey = _sharedSecretKey;
            this.processorClientId = _processorClientId;
        }

        private string OutFundsTransferSignature(TransferResponseMessage response)
        {
            string benfVars = response.BeneficiaryData.AccountNo.Trim();
            string amounts = response.Amounts.Currency.ToString().Trim() + response.Amounts.FinalAmount.ToString().Trim() + response.Amounts.Surcharge.ToString().Trim();
            string time = response.ResponseTime.ToString().Trim();
            string combine = benfVars + amounts + time + response.BeneficiaryData.BenefBankId.ToString().Trim() + response.MessageType.Trim() + response.ResponseCode.ToString().Trim() + response.RetrievalRefNo.ToString().Trim() + this.sharedSecretKey.Trim();
            return HashHelper.Signature(combine);
        }

        private string OutBeneficiarySignature(BeneficiaryInformationResponse response)
        {
            string benfVars = response.BeneficiaryData.AccountNo.Trim();
            string time = response.ResponseTime.ToString().Trim();
            string combine = benfVars + time + response.BeneficiaryData.BenefBankId.ToString().Trim() + response.MessageType.Trim() + response.ResponseCode.ToString().Trim() + response.RetrievalRefNo.ToString().Trim() + this.sharedSecretKey.Trim();
            return HashHelper.Signature(combine);
        }

        private string OutTransferExceptionSignature(TransferExceptionResponse response)
        {
            string time = response.ResponseTime.ToString().Trim();
            string combine = time + response.DownStreamBankId.ToString().Trim() + response.MessageType.Trim() + response.ResponseCode.ToString().Trim() + response.RetrievalRefNo.ToString().Trim() + this.sharedSecretKey.Trim();
            return HashHelper.Signature(combine);
        }

        public Dictionary<string, string> GetOutGoingBeneficiaryResponseHeaders(BeneficiaryInformationResponse response)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Authorization", this.processorClientId);
            headers.Add("signature", this.OutBeneficiarySignature(response));
            return headers;
        }

        public Dictionary<string, string> GetOutFundsTransferResponseHeaders(TransferResponseMessage response)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Authorization", this.processorClientId);
            headers.Add("signature", this.OutFundsTransferSignature(response));
            return headers;
        }

        public Dictionary<string, string> GetOutTransferExceptionResponseResponseHeaders(TransferExceptionResponse response)
        {
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Authorization", this.processorClientId);
            headers.Add("signature", this.OutTransferExceptionSignature(response));
            return headers;
        }
    }
} */