﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using PayDirectWebService.Models.API.PaymentNotification.dto;

namespace PayDirectWebService.Models.API.Security.util
{
    public class FromDownstreamRequestMessageSecure
    {
        string signature;
        string nonce;
        bool showErrorDescription;
        string sharedSecretKey;
        long timestamp;
        public FromDownstreamRequestMessageSecure(string headerSecretKey, string headerSignature, string headerNonce, long headerTimestamp)
        {
            this.signature = headerSignature;
            this.nonce = headerNonce;
            timestamp = headerTimestamp;
            this.sharedSecretKey = headerSecretKey;
            this.showErrorDescription =  Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SecurityErrorHelperMessages"]);
        }

        private void ValidatePaymentNotificationEnquirySignature(PaymentNotificationEnquiryRequest request)
        {
            string merchantRef = request.MerchantReference.ToString();

            string combine = merchantRef + this.nonce.Trim() + this.sharedSecretKey.Trim();

            if (!HashHelper.HashValue(combine).Equals(this.signature))
                throw new SecurityViolationException("Incorrect Security parameters " + ((this.showErrorDescription) ? ":: Incorrect Signature" : ""));
        }

        private void ValidateCustomerNumberRequestSignature(CustomerInformationRequest request)
        {
            string merchantRef = request.ProductId.ToString();
            string combine = merchantRef + this.nonce.Trim() + this.sharedSecretKey.Trim();

            if (!HashHelper.HashValue(combine).Equals(this.signature))
                throw new SecurityViolationException("Incorrect Security parameters " + ((this.showErrorDescription) ? ":: Incorrect Signature" : ""));
        }

        private void ValidateTimestamp(long _timestamp)
        {
            if (_timestamp != this.timestamp)
                throw new SecurityViolationException("Incorrect Security parameters " + ((this.showErrorDescription) ? ":: Incorrect Timestamp" : ""));
        }

        public void ValidatePaymentNotificationEnquiryRequest(PaymentNotificationEnquiryRequest request)
        {
            this.ValidatePaymentNotificationEnquirySignature(request);
        }

        public void ValidateCustomerNumberRequest(CustomerInformationRequest request)
        {
            this.ValidateCustomerNumberRequestSignature(request);
            this.ValidateTimestamp( Convert.ToInt64(request.TransmissionDate));
        }

        public void ValidatePaymentNotificationSignature(PaymentNotificationRequest request)
        {
            string productCode = request.RouteId.ToString();
            string postdate = request.TransmissionDate.ToString();
            string combine = productCode + postdate + this.nonce.Trim() + this.sharedSecretKey.Trim();

            if (!HashHelper.HashValue(combine).Equals(this.signature))
                throw new SecurityViolationException("Incorrect Security parameters " + ((this.showErrorDescription) ? ":: Incorrect Signature" : ""));

            this.ValidateTimestamp(Convert.ToInt64(request.TransmissionDate));
        }

        public void ValidatepaymentSignature(PaymentNotification.dto.Payment request)
        {
            List<PaymentItem> paymentItems = request.PaymentItems;
            PaymentItem item = paymentItems[0];
            string combine = item.ItemCode.ToString() + item.ItemCode + this.sharedSecretKey.Trim();

            if (!HashHelper.HashValue(combine).Equals(this.signature))
                throw new SecurityViolationException("Incorrect Security parameters " + ((this.showErrorDescription) ? ":: Incorrect Signature" : ""));

        }
    }
} 