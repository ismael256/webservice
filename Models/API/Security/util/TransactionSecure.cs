﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.Utils.dto;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.API.Security.dto;
using PayDirectWebService.Models.API.Security.dao;

namespace PayDirectWebService.Models.API.Security.util
{
    public class TransactionSecure
    {
        public MerchantApiServiceprovider ClientEntities;
        PayDirectWebService.Models.API.Utils.dto.ApiUsage apiRequest;
        bool showErrorDescription;
        public TransactionSecure(PayDirectWebService.Models.API.Utils.dto.ApiUsage request)
        {
            showErrorDescription = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SecurityErrorHelperMessages"]);
            apiRequest = request;
            ClientEntities =  SecurityDao.GetClientAuthAttributes(request.ClientId);
            this.IsNonceExists();
            this.IsSignatureExists();
            this.IsSourceIpCorrect();
            this.IsTimestampInRange(); 
        }

        public void IsSignatureExists()
        {
            if (SecurityDao.TransactionSignatureExists(apiRequest.Signature))
                throw new SecurityViolationException("Incorrect Security parameters " + ((showErrorDescription)?":: Signature has already been used": ""));
        }

        public void IsTimestampInRange()
        {
            long currentTime = TimeHelper.ToUnixTime(DateTime.Now);
            int acceptableTimeRange = 180;
            long timestamp = 0;
            try {
                timestamp = Convert.ToInt64(apiRequest.Timestamp);
                acceptableTimeRange = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["APIRequestTimeRange"]);
            }
            catch (Exception ex) { throw new SecurityViolationException("Incorrect Security parameters " + ((showErrorDescription) ? ":: Invalid Timestamp" : "")); }

            if ((currentTime - timestamp) > acceptableTimeRange)
                throw new SecurityViolationException("Incorrect Security parameters " + ((showErrorDescription) ? ":: Timestamp used is out of range" : ""));
        }

     
        public void IsSourceIpCorrect()
        {
            if(ClientEntities.IpAddress != apiRequest.SourceIpaddress)
                throw new SecurityViolationException("Incorrect Security parameters " + ((showErrorDescription) ? ":: SourceIpAddress used is incorrect" : ""));
        }

        public void IsNonceExists()
        {
            if(SecurityDao.TransactionNonceExists(apiRequest.Nonce))
                throw new SecurityViolationException("Incorrect Security parameters " + ((showErrorDescription) ? ":: Nonce has already been used" : ""));
        }

        /// <summary>
        /// ensures that the client Id specified in the header belongs to said merchant
        /// </summary>
        /// <param name="bankId"></param>
        /// <param name="clientId"></param>
        public static void IsbankIdMatchClientId(int merchantId, string clientId)
        {
            if (SecurityDao.IfClientIdMatchesMerchantApiProviderClientId(clientId, merchantId))
                throw new SecurityViolationException("Incorrect Security parameters :: wrong client Id used ");
        }
    }
} 