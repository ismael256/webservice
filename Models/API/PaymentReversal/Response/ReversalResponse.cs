﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentReversal.Response
{
    [DataContract(Namespace = "")]
    public class ReversalResponse
    {
        [DataMember]
        public long PaymentLogId { get; set; }

        [DataMember]
        public int ResponseCode { get; set; }
    }
}