﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentReversal.Response
{
     [DataContract(Namespace = "")]
    public class PaymentReversalResponse
    {
        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public int ProductCode { get; set; }

        [DataMember(IsRequired = true)]
        public List<ReversalResponse> Reversals { get; set; }
    }
}