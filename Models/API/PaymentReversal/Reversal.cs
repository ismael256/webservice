﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentReversal
{
    [DataContract(Namespace = "")]
    public class Reversal
    {
        [DataMember(IsRequired = true)]
        public long PaymentLogId { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Numeric]
        public long ReversalId { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string TerminalId { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Numeric]
        public decimal Amount { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(101, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Reason { get; set; }
    }
}