﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dto;

/*
namespace PayDirectWebService.Models.API.PaymentReversal
{
    public class ReversalDetails
    {
        long _paymentLogId;
        long _reversalId;
        string _terminalId;
        decimal _amount;
        string _reason;
        public Products ProductDetails { get; set; }
        string _paymentRefNumber;
        public int ReversalState { get; set; }

        public ReversalDetails(int productId,long paymentLogId, long reversalId, string terminalId, decimal amount, string reason)
        {
            _paymentLogId = paymentLogId;
            _reversalId = reversalId;
            _terminalId = terminalId;
            _amount = amount;
            _reason = reason;

            ProductDetails = PaymentOptionConfigs.GetProductDetails(productId);

            Dictionary<string, string> dic = PaymentOptionConfigs.RetrievePaymentLogAttributes(_paymentLogId);

            _paymentRefNumber = dic["PaymentRefNumber"];
            ReversalState = Convert.ToInt32(dic["ReversalStatus"]);
        }

        public void Zeroize()
        {
            ReversalDetailsModel reversalModel = new ReversalDetailsModel();
            reversalModel.adjustment_amount = 0.00m;
            reversalModel.adjustment_comments = _reason;
            reversalModel.adjustment_id = PaymentOptionConfigs.RetrieveAdjustmentId(_paymentLogId);
            reversalModel.adjustment_reason = "Wrong Posting";//hardcodded todo What are the other options
            reversalModel.adjustment_type = "ADJUSTMENT"; //hardcodded todo What are the other options
            reversalModel.bank_coll_fee = 0.00m;
            reversalModel.is_pending = false;
            reversalModel.isw_coll_fee = 0.00m;
            reversalModel.tellers_adjustment_amount = 0.00m;

            PaymentReversals.Zeroroize(reversalModel);
        }

        public void ReverseAsNewTransaction()
        {
            ReversalDetailsModel reversalModel = new ReversalDetailsModel();
            reversalModel.adjustment_amount = _amount;
            reversalModel.adjustment_comments = _reason;
            reversalModel.adjustment_id = PaymentOptionConfigs.RetrieveAdjustmentId(_paymentLogId);
            reversalModel.adjustment_reason = "Wrong Posting";//hardcodded todo What are the other options
            reversalModel.adjustment_type = ""; 
            reversalModel.bank_coll_fee = 0.00m;
            reversalModel.is_pending = false;
            reversalModel.isw_coll_fee = 0.00m;
            reversalModel.tellers_adjustment_amount = 0.00m;
            reversalModel.payment_log_id = _paymentLogId;
            reversalModel.payment_ref_number = _paymentRefNumber;
            reversalModel.is_cheque_payment = false;

            PaymentReversals.ReverseAsNewTransaction(reversalModel);
        }
    }
} */