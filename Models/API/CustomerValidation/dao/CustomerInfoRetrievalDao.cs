﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using PayDirectWebService.Helpers;

namespace PayDirectWebService.Models.API.CustomerValidation.dao
{
    public class CustomerInfoRetrievalDao
    {
        public static Customer RetrieveCustomerDetails(string custNum, int productId, decimal amount, int paymentId, String paymentreason)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                IList<usp_customer_details_SELECT_Result> cust_record = dbEnt.SP_usp_customer_details_SELECT(custNum, productId).ToList<usp_customer_details_SELECT_Result>();

                if (cust_record.Count == 0)
                    throw new InvalidCustomerReferenceException("Supplied Customer Number is wrong! " + custNum);

                if (cust_record.Count > 1)
                    throw new IncorrectApplicationConfigurationException("Supplied customer number appears more than once in customer's database, please contact merchant to correct this error " + custNum);


                return ExtractCustomerInfo(cust_record[0], Exceptions.SuccessfullResponse.RESPONSE_CODE, amount, paymentId, paymentreason);
               
            }
        }

        private static Customer ExtractCustomerInfo(usp_customer_details_SELECT_Result cust, int responseCode, Nullable<decimal> amount, Nullable<long> paymentId, string paymentreason)
        {
            Customer customer = new Customer();
            customer.CustomerReferenceAlternate = (cust.student_number_alternate == null) ? "" : cust.student_number_alternate;
            customer.CustReference = (cust.student_number == null) ? "" : cust.student_number;
            customer.FirstName = (cust.student_first_name == null) ? "" : cust.student_first_name;
            customer.LastName = (cust.student_last_name == null) ? "" : cust.student_last_name;
            customer.OtherName = (cust.student_other_names == null) ? "" : cust.student_other_names;
            customer.Amount = amount;
            customer.CustPaymentId = paymentId;
            customer.ResponseCode = responseCode;
            customer.PaymentReason = paymentreason;


            string phone = cust.student_phone;
            try
            {
                customer.Phone = TextHelper.ConvertStringToLong(phone.ToString(), "Phone Number");
            }
            catch (Exception e) { }

            customer.Email = (cust.student_email == null) ? "" : cust.student_email;

            return customer;
        }

        private static Customer ExtractCustomerInfo(paydirectservice_usp_customers_details_SELECT_Result cust, int responseCode, Nullable<decimal> amount, Nullable<long> paymentId, string paymentreason)
        {
            Customer customer = new Customer();
            customer.CustomerReferenceAlternate = (cust.student_number_alternate == null) ? "" : cust.student_number_alternate;
            customer.CustReference = (cust.student_number == null) ? "" : cust.student_number;
            customer.FirstName = (cust.student_first_name == null) ? "" : cust.student_first_name;
            customer.LastName = (cust.student_last_name == null) ? "" : cust.student_last_name;
            customer.OtherName = (cust.student_other_names == null) ? "" : cust.student_other_names;
            customer.ResponseCode = responseCode;
            customer.Amount = amount;
            customer.CustPaymentId = paymentId;
            customer.PaymentReason = paymentreason;
            string phone = cust.student_phone;

            try
            {
                customer.Phone = TextHelper.ConvertStringToLong(phone.ToString(), "Phone Number");
            }
            catch (Exception e) { }

            customer.Email = (cust.student_email == null) ? "" : cust.student_email;

            return customer;
        }


        public static List<Customer> BatchRetrieveCustomerDetails(List<CustNumber> customerDetails, int productId)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                String customerNos = "";
                List<Customer> customers = new List<Customer>();
                for (int c = 0; c < customerDetails.Count; c++ )
                {
                    if (c == 0)
                        customerNos = customerDetails[c].CustReference.Trim();
                    else
                        customerNos = customerNos + "," + customerDetails[c].CustReference.Trim();
                }

                IList<paydirectservice_usp_customers_details_SELECT_Result> customerResponseDetails = dbEnt.SP_retrieve_customers_Details(customerNos, productId).ToList<paydirectservice_usp_customers_details_SELECT_Result>();

               

                foreach (var customer in customerResponseDetails)
                {
                    var cust = customerDetails.Find(e => e.CustReference == customer.student_number);
                    if(cust != null)
                        customers.Add(ExtractCustomerInfo(customer, Exceptions.SuccessfullResponse.RESPONSE_CODE, cust.Amount, cust.CustPaymentId, cust.PaymentReason));
                }

                if (customers.Count != customerDetails.Count)
                {
                    foreach (var custNo in customerDetails)
                    {
                        paydirectservice_usp_customers_details_SELECT_Result emptyCust = new paydirectservice_usp_customers_details_SELECT_Result();
                        emptyCust.student_number = custNo.CustReference;
                        var cust = customers.Find(e => e.CustReference == custNo.CustReference);
                        if (cust == null)
                            customers.Add(ExtractCustomerInfo(emptyCust, Exceptions.InvalidCustomerReferenceException.RESPONSE_CODE, custNo.Amount, custNo.CustPaymentId, custNo.PaymentReason));
                    }
                }
                

                return customers;
            }

        }
    }
} 