﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using System.Data.SqlClient;
using System.Data;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.API.Utils;

namespace PayDirectWebService.Models.API.CustomerValidation.dao
{
    public class CardRetrievalDao
    {
        public static List<Customer> BatchRetrieveCardDetails(List<CustNumber> customerDetails)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                String customerNos = "";
                List<Customer> cards = new List<Customer>();
                for (int c = 0; c < customerDetails.Count; c++)
                {
                    customerNos = (c == 0) ? customerDetails[c].CustReference.Trim() : customerNos + "," + customerDetails[c].CustReference.Trim();
                }
                string connstring = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Paypoint_ADO"].ConnectionString;
                using (var conn = new SqlConnection(connstring))
                using (var CommandTypecmd = conn.CreateCommand())
                {
                    string buildquery = "validate_cards";
                    CommandTypecmd.CommandText = buildquery;
                    CommandTypecmd.CommandType = CommandType.StoredProcedure;
                    CommandTypecmd.Connection = conn;
                    CommandTypecmd.CommandTimeout = 300;
                    SqlDataReader reader;
                    conn.Open();

                    SqlParameter pans = new SqlParameter
                    {
                        ParameterName = "pans",
                        Direction = System.Data.ParameterDirection.Input,
                        Value = customerNos,
                        SqlDbType = SqlDbType.Text
                    };
                    CommandTypecmd.Parameters.Add(pans);
                    reader = CommandTypecmd.ExecuteReader();
                    while (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Customer customer = new Customer();
                            customer.CustomerReferenceAlternate = "";
                            customer.CustReference = DataRecordExtensions.SafeDataReaderGetString(reader, "cardNumber");
                           customer.FirstName =   DataRecordExtensions.SafeDataReaderGetString(reader, "name");
                         //  if (customer.FirstName.Length < 5)
                            //   customer.FirstName = " -- ";
                           customer.LastName = " -- ";
		                   /* if(name.Length > 2)
		                    {
                                char[] separ = {' '};
			                    String []tokens = name.Split(separ);
			                    customer.FirstName = tokens[0];
			                    if(tokens.Count() > 1)
				                    customer.LastName = name.Substring(tokens[0].Length + 1, name.Length);
			                    else
				                    customer.LastName = name.Substring(tokens[0].Length, name.Length);
		                    }
		                    else
		                    {
			                   customer.FirstName = "-----";
			                   customer.LastName = "------";
		                    }*/
                            customer.OtherName = "";
                            customer.Amount = customerDetails.Find(x => x.CustReference == customer.CustReference).Amount ;
                            customer.CustPaymentId =  customerDetails.Find(x => x.CustReference ==  customer.CustReference).CustPaymentId;
                            customer.ResponseCode = Exceptions.SuccessfullResponse.RESPONSE_CODE;
                            customer.PaymentReason = customerDetails.Find(x => x.CustReference ==  customer.CustReference).PaymentReason;
                            customer.Phone = TextHelper.ConvertStringToLong(DataRecordExtensions.SafeDataReaderGetString(reader, "phoneNumber"), "Phone Number");
                            customer.Email = DataRecordExtensions.SafeDataReaderGetString(reader, "email");
                            cards.Add(customer);
	                    }
                        reader.NextResult();
                    }
                    CommandTypecmd.Parameters.Clear();
                    reader.Close();
                    conn.Close();
                }
                if (cards.Count != customerDetails.Count)
                {
                    foreach (var custNo in customerDetails)
                    {
                        paydirectservice_usp_customers_details_SELECT_Result emptyCust = new paydirectservice_usp_customers_details_SELECT_Result();
                        emptyCust.student_number = custNo.CustReference;
                        var cust = cards.Find(e => e.CustReference == custNo.CustReference);
                        if (cust == null)
                            cards.Add(ExtractCustomerInfo(Exceptions.SuccessfullResponse.RESPONSE_CODE, custNo.CustPaymentId, custNo.PaymentReason));
                    }
                }
                return cards;
            }
        }

        private static Customer ExtractCustomerInfo(int responseCode, Nullable<long> paymentId, string paymentreason)
        {
            Customer customer = new Customer();
            customer.CustomerReferenceAlternate = "";
            customer.CustReference = "";
            customer.FirstName = "";
            customer.LastName = "";
            customer.OtherName = "";
            customer.Amount = 0;
            customer.CustPaymentId = paymentId;
            customer.ResponseCode = responseCode;
            customer.PaymentReason = paymentreason;
            customer.Phone = 0;
            customer.Email = "";
            return customer;
        }

        private static Customer ExtractCustomerInfo(usp_customer_details_SELECT_Result cust, int responseCode, Nullable<decimal> amount, Nullable<long> paymentId, string paymentreason)
        {
            Customer customer = new Customer();
            customer.CustomerReferenceAlternate = (cust.student_number_alternate == null) ? "" : cust.student_number_alternate;
            customer.CustReference = (cust.student_number == null) ? "" : cust.student_number;
            customer.FirstName = (cust.student_first_name == null) ? "" : cust.student_first_name;
            customer.LastName = (cust.student_last_name == null) ? "" : cust.student_last_name;
            customer.OtherName = (cust.student_other_names == null) ? "" : cust.student_other_names;
            customer.Amount = amount;
            customer.CustPaymentId = paymentId;
            customer.ResponseCode = responseCode;
            customer.PaymentReason = paymentreason;


            string phone = cust.student_phone;
            try
            {
                customer.Phone = TextHelper.ConvertStringToLong(phone.ToString(), "Phone Number");
            }
            catch (Exception e) { }

            customer.Email = (cust.student_email == null) ? "" : cust.student_email;

            return customer;
        }
    }
}