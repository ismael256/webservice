﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using PayDirectWebService.Helpers;

namespace PayDirectWebService.Models.API.CustomerValidation.dao
{
    public class AgentsInfoRetrievalDao
    {
        public static Customer RetrieveCustomerDetails(string custNum, int productId, decimal amount, int paymentId, String paymentreason)
        {
            using (var dbEnt = new PayDirectModelEntities())
            {
                IList<usp_customer_details_SELECT_Result> cust_record = dbEnt.SP_usp_customer_details_SELECT(custNum, productId).ToList<usp_customer_details_SELECT_Result>();

                if (cust_record.Count == 0)
                    throw new InvalidCustomerReferenceException("Supplied Customer Number is wrong! " + custNum);

                if (cust_record.Count > 1)
                    throw new IncorrectApplicationConfigurationException("Supplied customer number appears more than once in customer's database, please contact merchant to correct this error " + custNum);


                return ExtractCustomerInfo(cust_record[0], Exceptions.SuccessfullResponse.RESPONSE_CODE, amount, paymentId, paymentreason);

            }
        }

        private static Customer ExtractCustomerInfo(usp_customer_details_SELECT_Result cust, int responseCode, Nullable<decimal> amount, Nullable<long> paymentId, string paymentreason)
        {
            Customer customer = new Customer();
            customer.CustomerReferenceAlternate = (cust.student_number_alternate == null) ? "" : cust.student_number_alternate;
            customer.CustReference = (cust.student_number == null) ? "" : cust.student_number;
            customer.FirstName = (cust.student_first_name == null) ? "" : cust.student_first_name;
            customer.LastName = (cust.student_last_name == null) ? "" : cust.student_last_name;
            customer.OtherName = (cust.student_other_names == null) ? "" : cust.student_other_names;
            customer.Amount = amount;
            customer.CustPaymentId = paymentId;
            customer.ResponseCode = responseCode;
            customer.PaymentReason = paymentreason;


            string phone = cust.student_phone;
            try
            {
                customer.Phone = TextHelper.ConvertStringToLong(phone.ToString(), "Phone Number");
            }
            catch (Exception e) { }

            customer.Email = (cust.student_email == null) ? "" : cust.student_email;

            return customer;
        }
    }
}