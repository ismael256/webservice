﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.CustomerValidation.dto
{
      [DataContract(Namespace = "")]
    public class CustNumber
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string CustReference { get; set; }

        [DataMember(IsRequired = false)]
        [DataAnnotationsExtensions.Numeric]
        public Nullable<decimal> Amount { get; set; }

        [DataMember(IsRequired = false)]
        [DataAnnotationsExtensions.Integer]
        public Nullable<long> CustPaymentId { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string PaymentReason { get; set; }
    }
}