﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.CustomerValidation.dto
{
     [DataContract(Namespace = "")]
    public class CustomerInformationResponse
    {
        [DataMember(IsRequired = true)]
        public List<Customer> Customers { get; set; }
    }
}