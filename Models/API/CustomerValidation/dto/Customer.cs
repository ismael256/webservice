﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.CustomerValidation.dto
{
     [DataContract(Namespace = "")]
    public class Customer
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(51, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string CustReference { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public Nullable<long> CustPaymentId { get; set; }

        [DataType(DataType.Text)]
        [DataMember]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string CustomerReferenceAlternate { get; set; }


        [DataType(DataType.Text)]
        [DataMember]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string PaymentReason { get; set; }

        [DataMember]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string FirstName { get; set; }

        [DataMember]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string LastName { get; set; }

        [DataMember]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string OtherName { get; set; }

        [DataMember]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 0)]
        public string Email { get; set; }

        [DataMember]
        [DataAnnotationsExtensions.Integer]
        public Nullable<long> Phone { get; set; }

        [DataMember]
        [DataAnnotationsExtensions.Numeric]
        public Nullable<decimal> Amount { get; set; }

        [DataMember]
        public int ResponseCode { get; set; }
    }
}