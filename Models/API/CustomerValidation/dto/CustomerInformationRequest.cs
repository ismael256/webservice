﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.CustomerValidation.dto
{
    [DataContract(Namespace = "")]
    public class CustomerInformationRequest
    {
         [DataMember(IsRequired = true)]
         [DataAnnotationsExtensions.Integer]
         public int ProductId { get; set; }

         [DataMember(IsRequired = true)]
         [DataType(DataType.Text)]
         public string Signature { get; set; }

         [DataMember(IsRequired = false)]
         [DataType(DataType.Text)]
         public string RouteId { get; set; }

         [DataMember(IsRequired = true)]
         [DataAnnotationsExtensions.Numeric]
         [Required(ErrorMessage = "The {0} field is required")]
         [Range(1000000000, 9999999999)]
         public long? TransmissionDate { get; set; }

         [DataMember(IsRequired = true)]
         public List<CustNumber> Customers { get; set; }

         [DataMember(IsRequired = false)]
         [DataType(DataType.Text)]
         public string PaymentItemCode { get; set; }

    }
}