﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using PayDirectWebService.Models.API.CustomerValidation.dao;

namespace PayDirectWebService.Models.API.CustomerValidation.util
{
    public class RetrieveCustomer
    {
        public static List<Customer> BatchUploadCustomerRetrieval(List<CustNumber> customerDetails,int productId)
        {
            return CustomerInfoRetrievalDao.BatchRetrieveCustomerDetails(customerDetails, productId);
        }

       
    }
} 