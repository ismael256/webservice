﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.Exceptions;
using PayDirectWebService.Models.API.CustomerValidation.dto;
using PayDirectWebService.Models.API.CustomerValidation.dao;
namespace PayDirectWebService.Models.API.CustomerValidation.utils
{
    public class RetrieveCards
    {
        public static List<Customer> BatchCardsRetrieval(List<CustNumber> customerDetails)
        {
            return CardRetrievalDao.BatchRetrieveCardDetails(customerDetails);
        }
    }
}