﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using PayDirectWebService.Models.Dto; /*

namespace PayDirectWebService.Models.API
{
    public class ApiMessageHandler : DelegatingHandler
    {
         protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
         {
             string apiRequestString = "";
             string apiResponseString = "";
             string erroressage = "";
             int errorCount = 0;
             try
             {
                 var apiRequest = new ApiRequest(request);
                 apiRequest.Content = request.Content.ReadAsStringAsync().Result;
                 apiRequestString = apiRequest.Content;
             }
             catch (Exception e) { errorCount++; erroressage = e.Message; }
            
            return base.SendAsync(request, cancellationToken).ContinueWith(
                task =>
                {
                    try
                     {
                          var apiResponse = new ApiResponse(task.Result);
                          apiResponse.Content = task.Result.Content.ReadAsStringAsync().Result;
                          apiResponseString = apiResponse.Content;
                     }
                    catch (Exception e) { errorCount++; erroressage = "   "+e.Message; }

                    LogAPIRequests.LogApiUsage(apiRequestString, DateTime.Now, apiResponseString + "  " + erroressage);

                    if (errorCount > 0)
                    {
                        task.Result.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                        task.Result.ReasonPhrase = "The service encountered an error while processing your request, please contact the PayDirect administrator to rectify this issue";
                    }
                    
                   return task.Result;
                }
            );
         }

         private Task<HttpResponseMessage> SendError(string error, System.Net.HttpStatusCode code)
         {
             var response = new HttpResponseMessage();
             response.Content = new StringContent(error);
             response.StatusCode = code;

             return Task<HttpResponseMessage>.Factory.StartNew(() => response);
         }
    }
} */