﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto
{
     [Serializable()]
    [DataContract(Namespace = "")]
    public class paymentNotificationEnquiryRequest
    {
        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        [Required(ErrorMessage = "The {0} field is required")]
        public int MerchantReference{get;set;}

        [DataMember(IsRequired = true)]
        [Required(ErrorMessage = "The {0} field is required")]
        public DateTime StartDate  { get; set; }

        [DataMember(IsRequired = true)]
        [Required(ErrorMessage = "The {0} field is required")]
        public DateTime EndDate { get; set; }

        [DataMember(IsRequired = false)]
        public string ThirdPartyCode { get; set; }
    }
}