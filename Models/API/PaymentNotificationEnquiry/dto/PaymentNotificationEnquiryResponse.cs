﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto
{
    [Serializable()]
    [DataContract(Namespace = "")]
    public class PaymentNotificationEnquiryResponse
    {
        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public short ResponseCode { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string ResponseMessage { get; set; }

        [DataMember(IsRequired = true)]
        public Payment[] Payments { get; set; }
    }
}