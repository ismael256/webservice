﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;

namespace PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto
{
    [Serializable()]
    [DataContract(Namespace = "")]
    public class PaymentNotificationEnquiryRequest
    {
        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public int MerchantReference { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime StartDate { get; set; }

        [DataMember(IsRequired = true)]
        public DateTime EndDate { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public String ThirdPartyCode { get; set; }
    }
}