﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DataAnnotationsExtensions;
using System.Runtime.Serialization;
using PayDirectWebService.Models.API.PaymentNotification.dto;

namespace PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto
{
    [Serializable()]
    [DataContract(Namespace = "")]
    public class Payment
    {
        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public long PaymentLogId { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(51, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string CustReference { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(51, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string AlternateCustReference { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(101, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string CustomerName { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(251, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string CustomerAddress { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(251, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string OtherCustomerInfo { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public String CustomerPhoneNumber { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Numeric]
        public long Amount { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(21, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string PaymentMethod { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string PaymentReference { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string TerminalId { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string ChannelName { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string Location { get; set; }

        [DataMember(IsRequired = true)]
        public bool IsReversal { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        public string PaymentDate { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string SettlementDate { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public int MerchantReference { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(25, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 2)]
        public string MerchantName { get; set; }

        [DataMember(IsRequired = true)]
        [DataAnnotationsExtensions.Integer]
        public string ReceiptNo { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        public string DepositSlipNumber { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string BankName { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(250, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string DepositorName { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string BankCode { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string BranchName { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string CollectionsAccount { get; set; }

        [DataMember(IsRequired = false)]
        [DataType(DataType.Text)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string ThirdPartyCode { get; set; }

        [DataMember(IsRequired = true)]
        [DataType(DataType.Text)]
        [StringLength(3, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
        public string PaymentCurrency { get; set; }

        [DataMember(IsRequired = true)]
        public PaymentItem[] PaymentItems { get; set; }
    }
}