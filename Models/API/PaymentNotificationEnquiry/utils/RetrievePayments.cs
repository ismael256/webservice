﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto;
namespace PayDirectWebService.Models.API.PaymentNotificationEnquiry.utils
{
    public class RetrievePayments
    {
        PaymentNotificationEnquiryRequest paymentsEnquiry;
        public RetrievePayments(PaymentNotificationEnquiryRequest request)
        {
            paymentsEnquiry = request;
        }

        public Payment[] GetAllPaymentsInRange()
        {
            List<Payment> payments = PayDirectWebService.Models.API.PaymentNotificationEnquiry.dao.GetPayments.GetCollegePayments(paymentsEnquiry.MerchantReference, paymentsEnquiry.StartDate, paymentsEnquiry.EndDate);
            Payment[] paymentCollectn = new Payment[payments.Count];
            for (int c = 0; c < payments.Count; c++)
            {
                paymentCollectn[c] = payments[c];
            }
            return  paymentCollectn;
        }
    }
}