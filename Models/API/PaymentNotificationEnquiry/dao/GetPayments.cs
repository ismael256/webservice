﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PayDirectWebService.Helpers;
using PayDirectWebService.Models.Dao.FilteredPayDirectModelEntities;
using PayDirectWebService.Models.API.PaymentNotificationEnquiry.dto;

namespace PayDirectWebService.Models.API.PaymentNotificationEnquiry.dao
{
    public class GetPayments
    {
        public static List<Payment> GetCollegePayments(int productId,DateTime startDate,DateTime endDate)
        {
            using (var dbContext = new PayDirectModelEntities())
            {
                List<Payment> paymentlist = new List<Payment>();
               /* IList<psp_retrieve_payments_by_college_Result> payments = dbContext.psp_retrieve_payments_by_college(productId, startDate, endDate).ToList<psp_retrieve_payments_by_college_Result>();
                foreach (var payment in payments)
                {
                    Payment fetchedPayment = new Payment();
                    fetchedPayment.AlternateCustReference = "";
                    fetchedPayment.Amount = Convert.ToInt64(payment.adjustment_amount);
                    fetchedPayment.BankCode = payment.bank_code;
                    fetchedPayment.BankName = payment.bank_name;
                    fetchedPayment.BranchName = payment.branch_name;
                    fetchedPayment.ChannelName = payment.channel_name;
                    fetchedPayment.CollectionsAccount = payment.coll_acct_num;
                    fetchedPayment.CustomerAddress = payment.cust_address;
                    fetchedPayment.CustomerName = payment.cust_name;
                    fetchedPayment.CustomerPhoneNumber =  payment.cust_phone;
                    fetchedPayment.CustReference = payment.cust_ref;
                    fetchedPayment.DepositorName = "";
                    fetchedPayment.DepositSlipNumber = payment.deposit_slip_no;
                    fetchedPayment.IsReversal = (payment.reversal_status == 1) ? true : false;
                    fetchedPayment.Location = payment.location;
                    fetchedPayment.MerchantName = payment.institution_name;
                    fetchedPayment.ProductId = productId;
                    fetchedPayment.OtherCustomerInfo = payment.other_customer_info;
                    fetchedPayment.PaymentCurrency = payment.payment_currency;
                    fetchedPayment.PaymentDate = TimeHelper.FormatTime(payment.payment_log_date.ToString());
                    fetchedPayment.PaymentLogId = Convert.ToInt64(payment.payment_log_id);
                    fetchedPayment.PaymentMethod = payment.payment_method_name;
                    fetchedPayment.PaymentReference = payment.payment_ref_num;
                    fetchedPayment.ReceiptNo =  payment.reciept_number;
                    fetchedPayment.SettlementDate = TimeHelper.FormatTime(payment.settlement_date.ToString());
                    fetchedPayment.TerminalId =  payment.terminal_id.ToString();
                    fetchedPayment.ThirdPartyCode = "";

                    paymentlist.Add(fetchedPayment);
                }
                */
                return paymentlist;
            }
        }
    }
}