﻿USE [uat_paydirect_core]
GO
/****** Object:  StoredProcedure [dbo].[usp_PaymentChannels_SELECT_ALL]    Script Date: 10/17/2013 09:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.usp_PaymentChannels_SELECT_ALL    Script Date: 9/25/2004 4:38:44 PM ******/




ALTER PROCEDURE [dbo].[usp_PaymentChannels_SELECT_ALL]
AS
BEGIN
	BEGIN
		SELECT * FROM PaymentChannels ORDER BY channelName
	END
END
