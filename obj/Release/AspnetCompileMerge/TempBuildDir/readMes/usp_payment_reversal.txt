﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[usp_payment_reversal]    Script Date: 12/11/2013 19:28:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_payment_reversal]
(
      @payment_log_id bigint,
      @reversal_date datetime,
      @retVal int output,
      @payment_ref_num VARCHAR(50),
      @is_chq_payment bit,
      @reversed_payment_log_id bigint OUTPUT
)
AS

DECLARE @ref varchar(50)
DECLARE @reversal_status int
--DECLARE @previous_payment_log_id bigint
DECLARE @product_id bigint 
DECLARE @settlement_date DATETIME
DECLARE @remmittance_date DATETIME


SET @reversed_payment_log_id = 0

select @reversal_status = l.reversal_status, @product_id = l.student_college_id
from tbl_payments_log l
where l.payment_log_id = @payment_log_id

if (@reversal_status = 1) -- reversed
BEGIN
      SET @retVal = -2000
      Return
END
ELSE IF (@reversal_status = 2) -- Repost 
BEGIN
      SET @retVal = -3000
      Return
END
ELSE
BEGIN
      BEGIN TRAN
      
      UPDATE tbl_payments_log SET reversal_status = 1, push_status=0, payment_notification_status = 5,
	  service_notification_status = 0, ftp_notification_status = 0, email_notification_status = 0,
	  sms_notification_status = 0, loadcard_notification_status = 0
      WHERE payment_log_id = @payment_log_id
      
      
      DECLARE @payment_ref_index bigint
      DECLARE @pay_index varchar(50)
      DECLARE @narration VARCHAR(50)      
      
      INSERT INTO tbl_payment_ref_generator 
      (payment_ref)
      VALUES
      (@payment_ref_num)
      if @@error != 0
      begin
        rollback tran
        return @@error
      end
      SET @payment_ref_index = @@Identity
      SELECT @pay_index = RIGHT('000000' + CAST(@payment_ref_index AS varchar), 6)
      SET @pay_index = @payment_ref_num + @pay_index
      SET @narration = right(@ref + '-Reversal',100)
      SET @settlement_date = dbo.GetNextWorkDate(@reversal_date)
      SET @remmittance_date = dbo.GetNextWorkDate(@settlement_date)
      
      INSERT INTO [tbl_payments_log]
      ([channel_id],[channel_name],[coll_acct_num],[payment_log_date]
      ,[payment_ref_num],[cust_number],[old_cust_number],[product_unique_field]
      ,[cust_name],[cust_address],[cust_phone],[cust_email],[cust_dob],[cust_location_code]
      ,[cust_location_name],[cust_sub_location_code],[cust_sub_location_name],[card_id]
      ,[card_pan],[account_id],[account_number],[user_login_name],[user_last_name]
      ,[user_other_names],[role_name],[user_db_id],[user_staff_code]
      ,[bank_id],[bank_name],[cbn_bank_code],[bank_code],[branch_id]
      ,[branch_name],[branch_code],[deposit_slip_no],[payment_method_id]
      ,[payment_method_name],[state_id]
      ,[state_name],[isw_trans_fee_percent],[bank_coll_fee_percent]
      ,[reciept_number],[comments],[misc_bank_code],[misc_branch_code]
      ,[payment_currency],[isw_trans_fee],[lead_bank_fees_percent],[is_double_posting]
      ,[settlement_date],[remittance_date],[student_number],[student_name]
      ,[student_college_code],[student_college_name],[student_college_id]
      ,[student_department],[student_faculty],[student_level],[college_session]
      ,[semester],[product_id],[product_code],[product_name],[fees_amount],[iso_fees_percent]
      ,[notification_status],[notification_retries],[isw_trans_fees],[coll_bank_fees]
      ,[lead_bank_fees],[iso_fees],[transaction_ref_num],[transaction_status]
      ,[transaction_code],[payment_notification_status],[remittance_status]
      ,[push_status],[surcharge_deposit_slip_no],[email_notification_status]
      ,[sms_notification_status],[ftp_notification_status],[additional_info],
      [use_ent_receipt_no],[reversal_lock_start_date], [reversal_status])
      SELECT 
      [channel_id],[channel_name],[coll_acct_num],@reversal_date
      ,@pay_index,[cust_number],[old_cust_number],[product_unique_field]
      ,right([cust_name] + '(' + [payment_ref_num] + ' - Reversal)',100),[cust_address],[cust_phone],[cust_email],[cust_dob],[cust_location_code]
      ,[cust_location_name],[cust_sub_location_code],[cust_sub_location_name],[card_id]
      ,[card_pan],[account_id],[account_number],[user_login_name],[user_last_name]
      ,[user_other_names],[role_name],[user_db_id],[user_staff_code]
      ,[bank_id],[bank_name],[cbn_bank_code],[bank_code],[branch_id]
      ,[branch_name],[branch_code],[deposit_slip_no],[payment_method_id]
      ,[payment_method_name],[state_id]
      ,[state_name],-1 * [isw_trans_fee_percent],-1 * [bank_coll_fee_percent]
      ,[reciept_number],[comments],[misc_bank_code],[misc_branch_code]
      ,[payment_currency],-1 * [isw_trans_fee],-1 * [lead_bank_fees_percent],[is_double_posting]
      ,@settlement_date,@remmittance_date,[student_number],[student_name]
      ,[student_college_code],[student_college_name],[student_college_id]
      ,[student_department],[student_faculty],[student_level],[college_session]
      ,[semester],[product_id],[product_code],[product_name],-1 * [fees_amount],-1 * [iso_fees_percent]
      ,[notification_status],[notification_retries],-1 * [isw_trans_fees],-1 * [coll_bank_fees]
      ,-1 * [lead_bank_fees],-1 * [iso_fees],[transaction_ref_num],[transaction_status]
      ,[transaction_code],2,[remittance_status] -- mark the notification as sent
      ,[push_status],[surcharge_deposit_slip_no],[email_notification_status]
      ,[sms_notification_status],[ftp_notification_status],[additional_info],
      [use_ent_receipt_no],[reversal_lock_start_date], 2
      FROM [tbl_payments_log] p 
      where p.payment_log_id = @payment_log_id
      
      IF (@@Error <> 0 )
      BEGIN
            Set @retVal = -4000
            ROLLBACK TRANSACTION
            RETURN
      END
      
      SET @reversed_payment_log_id = @@Identity
      
      if (@is_chq_payment = 1)
      BEGIN
            INSERT INTO [tbl_cheque_payments]
        ([chq_payment_log_id],[chq_number],[chq_value_date],[chq_bank_id]
         ,[chq_bank_name],[is_own_bank_chq],[chq_date_returned])
        SELECT
        @reversed_payment_log_id,[chq_number],@settlement_date,[chq_bank_id]
         ,[chq_bank_name],[is_own_bank_chq],[chq_date_returned]
         FROM [tbl_cheque_payments] c inner join tbl_payments_log l
         on c.chq_payment_log_id = l.payment_log_id
         WHERE c.chq_payment_log_id = @payment_log_id
      END
      
      
      IF (@@Error <> 0 )
      BEGIN
            Set @retVal = -4001
            ROLLBACK TRANSACTION
            RETURN
      END
      
      update tbl_service_notification_status
      set update_flag = 0,service_update_status = 0
      where service_paylog_id = @payment_log_id 


      IF (@@Error <> 0 )
      BEGIN
            Set @retVal = -4002
            ROLLBACK TRANSACTION
            RETURN
      END
        
      
            
      COMMIT
      SET @retVal = 0
END
