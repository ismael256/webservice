﻿USE [uat_paydirect_core]
GO
/****** Object:  StoredProcedure [dbo].[usp_banksproducts_banks_SELECT]    Script Date: 12/17/2013 19:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER    PROCEDURE [dbo].[usp_banksproducts_banks_SELECT]
	@productID 		int
AS
BEGIN
	BEGIN
		SELECT  
			bankID,
			bankCode,
			isBankOnSwitch,
			bankName,
			isBankSuspended,
			cbnBankCode, 
                      	
			bankProductID,
			bankProductStatus,
			goLiveDate,
			
			productID,
			productName
		
		FROM
			Products INNER JOIN BanksProducts
				ON productID = bpProductID 
			INNER JOIN Banks 
				ON bpBankID = bankID
		
		WHERE bpProductID = @productID

		ORDER BY bankName

	END
END





