﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[usp_generate_payment_reference]    Script Date: 10/22/2013 16:37:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[usp_generate_payment_reference]
	@payment_ref_num varchar(50),
	@reciept_number varchar(50),
	@out_payment_ref varchar(50) output,
	@out_reciept_number varchar(50) output
as
declare @payment_ref_index bigint
declare @pay_index varchar(100)

	begin tran
	INSERT INTO tbl_payment_ref_generator 
		(payment_ref)
	VALUES
		(@payment_ref_num)
		
	IF (@@Error <> 0 )
	BEGIN
	
		ROLLBACK TRANSACTION
		RETURN
	END

	SET @payment_ref_index = SCOPE_IDENTITY()
	COMMIT TRANSACTION
			
	SELECT @pay_index = RIGHT('000000' + CAST(@payment_ref_index AS varchar), 6)
	SET @payment_ref_num = @payment_ref_num + @pay_index
	-- receipt number is now generated internally
	-- SET @reciept_number = @reciept_number + @pay_index
	
	EXEC [usp_generate_receipt_number] @out_reciept_number OUTPUT

	SET @out_payment_ref = @payment_ref_num
	