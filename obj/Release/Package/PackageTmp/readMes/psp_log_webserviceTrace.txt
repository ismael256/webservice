﻿USE [PayDirectWebService]
GO
/****** Object:  StoredProcedure [dbo].[psp_log_webserviceTrace]    Script Date: 12/13/2013 15:54:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[psp_log_webserviceTrace]
	@institution varchar(50),
    @service_method varchar(50),
    @request_time datetime,
    @request_message varchar(MAX),
    @hash_value varchar(256),
    @request_id int
AS
BEGIN
	INSERT INTO WebServiceTrace
           (Institution
           ,ServiceMethod
           ,RequestTime
           ,RequestMessage
           ,HashValue
           ,RequestId )
     VALUES
           (@institution
           ,@service_method
           ,@request_time
           ,@request_message
           ,@hash_value
           ,@request_id)

END
