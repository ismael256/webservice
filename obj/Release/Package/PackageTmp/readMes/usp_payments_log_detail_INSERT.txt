﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[usp_payments_log_detail_INSERT]    Script Date: 11/07/2013 16:05:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_payments_log_detail_INSERT]
	@paylog_details_paylog_id bigint,
	@paylog_details_amount money,
	@paylog_details_comments varchar(255),
	@payment_type_id int,
	@payment_type_code varchar(50),
	@payment_type_name varchar(100),
	@lead_bank_id int,
	@lead_bank_code varchar(20),
	@lead_bank_name varchar(50),
	@lead_bank_cbn_code varchar(20),
	@cbn_account_name varchar(50) = NULL,
	@cbn_account_number varchar(20) = NULL,
	@ref_payment_type_id int = NULL,
	@ref_payment_type_code varchar(10) = NULL,
	@ref_payment_type_name varchar(100) = NULL,
	@iso_bank_id int = NULL,
	@iso_bank_name varchar(50) = NULL,
	@iso_bank_code varchar(20) = NULL,
	@iso_bank_cbn_code varchar(20) = NULL,
	@total_fee money = NULL,
	@isw_trans_fee_pld money = NULL,
	@bank_coll_fee_pld money = NULL,
	@lead_bank_fee_pld money = NULL,
	@iso_fee_pld money = 0,
	@customer_borne_fee money = NULL,
	@export_status int = NULL,
	@item_type int = NULL,
	@reversal_status int = 0,
	@payment_item_category_id bigint = 0,
	@payment_item_category_name varchar(50) = NULL,
	@has_additional_data bit = 0,
	@merchandize_pin varchar(50) = NULL,
	@merchandize_serial_no varchar(50) = NULL,
	@merchandize_customer_msg varchar(255) = NULL,
	@third_party_account_number varchar(100) = NULL,
	@vas_provider_fee money = NULL,
	@vas_provider_bank_id int = NULL,
	@vas_provider_bank_name varchar(50) = NULL,
	@vas_provider_account_no varchar(50) = NULL,
	@terminal_owner_fee money = NULL,
	@terminal_owner_bank_id int = NULL,
	@terminal_owner_bank_name varchar(50) = NULL,
	@terminal_owner_account_no varchar(50) = NULL,
	@iso_bank_account_no varchar(50) = NULL,
	@paylog_details_id bigint OUTPUT
AS


INSERT INTO [dbo].[tbl_payments_log_details] (
	[paylog_details_paylog_id],
	[paylog_details_amount],
	[paylog_details_comments],
	[payment_type_id],
	[payment_type_code],
	[payment_type_name],
	[lead_bank_id],
	[lead_bank_code],
	[lead_bank_name],
	[lead_bank_cbn_code],
	[cbn_account_name],
	[cbn_account_number],
	[ref_payment_type_id],
	[ref_payment_type_code],
	[ref_payment_type_name],
	[iso_bank_id],
	[iso_bank_name],
	[iso_bank_code],
	[iso_bank_cbn_code],
	[total_fee],
	[isw_trans_fee_pld],
	[bank_coll_fee_pld],
	[lead_bank_fee_pld],
	[iso_fee_pld],
	[customer_borne_fee],
	[export_status],
	[item_type],
	[reversal_status],
	[payment_item_category_id],
	[payment_item_category_name],
	[has_additional_data],
	[merchandize_pin],
	[merchandize_serial_no],
	[merchandize_customer_msg],
	[third_party_account_number],
	[vas_provider_fee],
	[vas_provider_bank_id],
	[vas_provider_bank_name],
	[vas_provider_account_no],
	[terminal_owner_fee],
	[terminal_owner_bank_id],
	[terminal_owner_bank_name],
	[terminal_owner_account_no],
	[iso_bank_account_no]
) VALUES (
	@paylog_details_paylog_id,
	@paylog_details_amount,
	@paylog_details_comments,
	@payment_type_id,
	@payment_type_code,
	@payment_type_name,
	@lead_bank_id,
	@lead_bank_code,
	@lead_bank_name,
	@lead_bank_cbn_code,
	@cbn_account_name,
	@cbn_account_number,
	@ref_payment_type_id,
	@ref_payment_type_code,
	@ref_payment_type_name,
	@iso_bank_id,
	@iso_bank_name,
	@iso_bank_code,
	@iso_bank_cbn_code,
	@total_fee,
	@isw_trans_fee_pld,
	@bank_coll_fee_pld,
	@lead_bank_fee_pld,
	@iso_fee_pld,
	@customer_borne_fee,
	@export_status,
	@item_type,
	@reversal_status,
	@payment_item_category_id,
	@payment_item_category_name,
	@has_additional_data,
	@merchandize_pin,
	@merchandize_serial_no,
	@merchandize_customer_msg,
	@third_party_account_number,
	@vas_provider_fee,
	@vas_provider_bank_id,
	@vas_provider_bank_name,
	@vas_provider_account_no,
	@terminal_owner_fee,
	@terminal_owner_bank_id,
	@terminal_owner_bank_name,
	@terminal_owner_account_no,
	@iso_bank_account_no
)

SET @paylog_details_id = SCOPE_IDENTITY()
SELECT @paylog_details_id

--endregion

