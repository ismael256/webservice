﻿USE [uat_paydirect_ent]
GO
/****** Object:  StoredProcedure [dbo].[psp_retrieve_payments_by_college]    Script Date: 08/08/2014 11:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[psp_retrieve_payments_by_college](
   @college_id INT
)
AS
BEGIN
SET NOCOUNT ON
SELECT top 10
       l.payment_log_id,
	   c.route_id,
       l.student_number as txn_ref,
       SUM(a.adjustment_amount/100) AS adjustment_amount,
       l.remittance_status AS payment_status,
       l.payment_method_name,
       l.payment_ref_num,
       null as terminal_id, --- posibly extend -- look at the branch column cos theres a branch column to evry payment
       l.channel_name,
       l.branch_name as location, --clarify -- note that it relates to terminal_id above
       l.payment_log_date,
       c.college_code AS institution_id,
       c.college_name as institution_name,
       l.branch_name,
       l.bank_name,
       l.cust_name,
       l.cust_address + '|' + l.cust_phone AS other_customer_info,
       l.reciept_number,
	   l.coll_acct_num,
	   l.bank_code,
	   l.cust_address,
	   l.cust_phone,
	   l.deposit_slip_no,
	   l.product_unique_field,
	   l.payment_currency
FROM tbl_payments_log l 
INNER JOIN tbl_colleges c
ON l.student_college_id = c.college_id
INNER JOIN tbl_payments_log_details d
ON l.payment_log_id = d.paylog_details_paylog_id
LEFT JOIN tbl_payments_log_adjustments a
ON d.paylog_details_id= a.adjustment_id
WHERE (l.payment_notification_status = 0) 
AND (l.payment_log_date IS NOT NULL) 
AND (c.route_id IS NOT NULL AND c.route_id <> '')
AND (((c.notification_type = 1) AND (DATEDIFF(mi,l.payment_log_date,getdate())> c.necessary_delay) ))
AND (DATEDIFF(hh,l.payment_log_date,getdate())< 48)
AND (c.college_id = @college_id)
GROUP BY l.payment_log_id,
	   c.route_id,
       l.student_number,
       l.remittance_status,
       l.payment_method_name,
       l.payment_ref_num,
       l.channel_name,
       l.branch_name,
       l.payment_log_date,
       c.college_code,
       c.college_name,
       l.branch_name,
       l.bank_name,
       l.cust_name,
       l.cust_address + '|' + l.cust_phone,
       l.reciept_number,
       l.coll_acct_num,
	   l.bank_code,
	   l.cust_address,
	   l.cust_phone,
	   l.deposit_slip_no,
	   l.product_unique_field,
	   l.payment_currency

RETURN @@Error
END
